﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text/$Main$58/$
struct U24_t860252705;
// text
struct text_t3556653;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_text3556653.h"

// System.Void text/$Main$58/$::.ctor(text)
extern "C"  void U24__ctor_m3519126728 (U24_t860252705 * __this, text_t3556653 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean text/$Main$58/$::MoveNext()
extern "C"  bool U24_MoveNext_m365369077 (U24_t860252705 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
