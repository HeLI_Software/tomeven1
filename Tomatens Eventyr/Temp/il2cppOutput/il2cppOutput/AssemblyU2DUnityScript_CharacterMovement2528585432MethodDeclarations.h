﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CharacterMovement
struct CharacterMovement_t2528585432;
// UnityEngine.Collider
struct Collider_t2939674232;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"

// System.Void CharacterMovement::.ctor()
extern "C"  void CharacterMovement__ctor_m788163262 (CharacterMovement_t2528585432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::Start()
extern "C"  void CharacterMovement_Start_m4030268350 (CharacterMovement_t2528585432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::Update()
extern "C"  void CharacterMovement_Update_m390119439 (CharacterMovement_t2528585432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void CharacterMovement_OnTriggerEnter_m1343824250 (CharacterMovement_t2528585432 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::OnTriggerExit(UnityEngine.Collider)
extern "C"  void CharacterMovement_OnTriggerExit_m2048576744 (CharacterMovement_t2528585432 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void CharacterMovement_OnControllerColliderHit_m3640053894 (CharacterMovement_t2528585432 * __this, ControllerColliderHit_t2416790841 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CharacterMovement::Main()
extern "C"  void CharacterMovement_Main_m4097338943 (CharacterMovement_t2528585432 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
