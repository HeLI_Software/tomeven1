﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SUN
struct SUN_t82476;

#include "codegen/il2cpp-codegen.h"

// System.Void SUN::.ctor()
extern "C"  void SUN__ctor_m1493783231 (SUN_t82476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SUN::Start()
extern "C"  void SUN_Start_m440921023 (SUN_t82476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SUN::Update()
extern "C"  void SUN_Update_m789501998 (SUN_t82476 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
