﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToPoteland/$Main$44
struct U24MainU2444_t3891303456;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"

// System.Void ToPoteland/$Main$44::.ctor()
extern "C"  void U24MainU2444__ctor_m1372992950 (U24MainU2444_t3891303456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> ToPoteland/$Main$44::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2444_GetEnumerator_m2259384390 (U24MainU2444_t3891303456 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
