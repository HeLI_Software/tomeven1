﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bartalk
struct bartalk_t3961876287;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void bartalk::.ctor()
extern "C"  void bartalk__ctor_m367497719 (bartalk_t3961876287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator bartalk::Main()
extern "C"  Il2CppObject * bartalk_Main_m373104404 (bartalk_t3961876287 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
