﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Jaagub
struct Jaagub_t2211131146;

#include "codegen/il2cpp-codegen.h"

// System.Void Jaagub::.ctor()
extern "C"  void Jaagub__ctor_m2085018758 (Jaagub_t2211131146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jaagub::DeductPoints(System.Int32)
extern "C"  void Jaagub_DeductPoints_m3855673429 (Jaagub_t2211131146 * __this, int32_t ___hitpoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jaagub::Update()
extern "C"  void Jaagub_Update_m1937934151 (Jaagub_t2211131146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jaagub::Main()
extern "C"  void Jaagub_Main_m3862078327 (Jaagub_t2211131146 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
