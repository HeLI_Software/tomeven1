﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToBar002
struct GoToBar002_t2246850754;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void GoToBar002::.ctor()
extern "C"  void GoToBar002__ctor_m425628494 (GoToBar002_t2246850754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToBar002::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void GoToBar002_OnTriggerEnter_m203850474 (GoToBar002_t2246850754 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToBar002::Main()
extern "C"  void GoToBar002_Main_m1868886959 (GoToBar002_t2246850754 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
