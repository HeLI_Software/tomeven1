﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadNewScene
struct  LoadNewScene_t2813835762  : public MonoBehaviour_t667441552
{
public:
	// System.String LoadNewScene::newScene
	String_t* ___newScene_2;

public:
	inline static int32_t get_offset_of_newScene_2() { return static_cast<int32_t>(offsetof(LoadNewScene_t2813835762, ___newScene_2)); }
	inline String_t* get_newScene_2() const { return ___newScene_2; }
	inline String_t** get_address_of_newScene_2() { return &___newScene_2; }
	inline void set_newScene_2(String_t* value)
	{
		___newScene_2 = value;
		Il2CppCodeGenWriteBarrier(&___newScene_2, value);
	}
};

struct LoadNewScene_t2813835762_StaticFields
{
public:
	// UnityEngine.GameObject LoadNewScene::title_Background
	GameObject_t3674682005 * ___title_Background_3;
	// UnityEngine.GameObject LoadNewScene::title
	GameObject_t3674682005 * ___title_4;

public:
	inline static int32_t get_offset_of_title_Background_3() { return static_cast<int32_t>(offsetof(LoadNewScene_t2813835762_StaticFields, ___title_Background_3)); }
	inline GameObject_t3674682005 * get_title_Background_3() const { return ___title_Background_3; }
	inline GameObject_t3674682005 ** get_address_of_title_Background_3() { return &___title_Background_3; }
	inline void set_title_Background_3(GameObject_t3674682005 * value)
	{
		___title_Background_3 = value;
		Il2CppCodeGenWriteBarrier(&___title_Background_3, value);
	}

	inline static int32_t get_offset_of_title_4() { return static_cast<int32_t>(offsetof(LoadNewScene_t2813835762_StaticFields, ___title_4)); }
	inline GameObject_t3674682005 * get_title_4() const { return ___title_4; }
	inline GameObject_t3674682005 ** get_address_of_title_4() { return &___title_4; }
	inline void set_title_4(GameObject_t3674682005 * value)
	{
		___title_4 = value;
		Il2CppCodeGenWriteBarrier(&___title_4, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
