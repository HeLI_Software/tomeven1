﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// Dimensions
struct Dimensions_t2407799789;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t215400611;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t4024180168;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// PixelMapTerrain
struct  PixelMapTerrain_t2288975167  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.Texture2D PixelMapTerrain::heightmap
	Texture2D_t3884108195 * ___heightmap_2;
	// Dimensions PixelMapTerrain::dimensions
	Dimensions_t2407799789 * ___dimensions_3;
	// System.Boolean PixelMapTerrain::slopeEdges
	bool ___slopeEdges_4;
	// System.Int32 PixelMapTerrain::slopeTolerance
	int32_t ___slopeTolerance_5;
	// UnityEngine.Vector3[] PixelMapTerrain::vertices
	Vector3U5BU5D_t215400611* ___vertices_6;
	// System.Int32[] PixelMapTerrain::triangles
	Int32U5BU5D_t3230847821* ___triangles_7;
	// UnityEngine.Vector2[] PixelMapTerrain::uvs
	Vector2U5BU5D_t4024180168* ___uvs_8;
	// UnityEngine.Material[] PixelMapTerrain::mat
	MaterialU5BU5D_t170856778* ___mat_9;

public:
	inline static int32_t get_offset_of_heightmap_2() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___heightmap_2)); }
	inline Texture2D_t3884108195 * get_heightmap_2() const { return ___heightmap_2; }
	inline Texture2D_t3884108195 ** get_address_of_heightmap_2() { return &___heightmap_2; }
	inline void set_heightmap_2(Texture2D_t3884108195 * value)
	{
		___heightmap_2 = value;
		Il2CppCodeGenWriteBarrier(&___heightmap_2, value);
	}

	inline static int32_t get_offset_of_dimensions_3() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___dimensions_3)); }
	inline Dimensions_t2407799789 * get_dimensions_3() const { return ___dimensions_3; }
	inline Dimensions_t2407799789 ** get_address_of_dimensions_3() { return &___dimensions_3; }
	inline void set_dimensions_3(Dimensions_t2407799789 * value)
	{
		___dimensions_3 = value;
		Il2CppCodeGenWriteBarrier(&___dimensions_3, value);
	}

	inline static int32_t get_offset_of_slopeEdges_4() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___slopeEdges_4)); }
	inline bool get_slopeEdges_4() const { return ___slopeEdges_4; }
	inline bool* get_address_of_slopeEdges_4() { return &___slopeEdges_4; }
	inline void set_slopeEdges_4(bool value)
	{
		___slopeEdges_4 = value;
	}

	inline static int32_t get_offset_of_slopeTolerance_5() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___slopeTolerance_5)); }
	inline int32_t get_slopeTolerance_5() const { return ___slopeTolerance_5; }
	inline int32_t* get_address_of_slopeTolerance_5() { return &___slopeTolerance_5; }
	inline void set_slopeTolerance_5(int32_t value)
	{
		___slopeTolerance_5 = value;
	}

	inline static int32_t get_offset_of_vertices_6() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___vertices_6)); }
	inline Vector3U5BU5D_t215400611* get_vertices_6() const { return ___vertices_6; }
	inline Vector3U5BU5D_t215400611** get_address_of_vertices_6() { return &___vertices_6; }
	inline void set_vertices_6(Vector3U5BU5D_t215400611* value)
	{
		___vertices_6 = value;
		Il2CppCodeGenWriteBarrier(&___vertices_6, value);
	}

	inline static int32_t get_offset_of_triangles_7() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___triangles_7)); }
	inline Int32U5BU5D_t3230847821* get_triangles_7() const { return ___triangles_7; }
	inline Int32U5BU5D_t3230847821** get_address_of_triangles_7() { return &___triangles_7; }
	inline void set_triangles_7(Int32U5BU5D_t3230847821* value)
	{
		___triangles_7 = value;
		Il2CppCodeGenWriteBarrier(&___triangles_7, value);
	}

	inline static int32_t get_offset_of_uvs_8() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___uvs_8)); }
	inline Vector2U5BU5D_t4024180168* get_uvs_8() const { return ___uvs_8; }
	inline Vector2U5BU5D_t4024180168** get_address_of_uvs_8() { return &___uvs_8; }
	inline void set_uvs_8(Vector2U5BU5D_t4024180168* value)
	{
		___uvs_8 = value;
		Il2CppCodeGenWriteBarrier(&___uvs_8, value);
	}

	inline static int32_t get_offset_of_mat_9() { return static_cast<int32_t>(offsetof(PixelMapTerrain_t2288975167, ___mat_9)); }
	inline MaterialU5BU5D_t170856778* get_mat_9() const { return ___mat_9; }
	inline MaterialU5BU5D_t170856778** get_address_of_mat_9() { return &___mat_9; }
	inline void set_mat_9(MaterialU5BU5D_t170856778* value)
	{
		___mat_9 = value;
		Il2CppCodeGenWriteBarrier(&___mat_9, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
