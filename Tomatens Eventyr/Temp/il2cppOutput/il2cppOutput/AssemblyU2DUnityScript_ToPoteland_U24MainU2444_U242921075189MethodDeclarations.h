﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToPoteland/$Main$44/$
struct U24_t2921075189;

#include "codegen/il2cpp-codegen.h"

// System.Void ToPoteland/$Main$44/$::.ctor()
extern "C"  void U24__ctor_m2515562177 (U24_t2921075189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ToPoteland/$Main$44/$::MoveNext()
extern "C"  bool U24_MoveNext_m615627145 (U24_t2921075189 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
