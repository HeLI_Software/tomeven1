﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadNewScene/$HideScene$28/$
struct U24_t1905319356;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadNewScene/$HideScene$28/$::.ctor()
extern "C"  void U24__ctor_m12193364 (U24_t1905319356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadNewScene/$HideScene$28/$::MoveNext()
extern "C"  bool U24_MoveNext_m4017414482 (U24_t1905319356 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
