﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkyboxRotator
struct SkyboxRotator_t551786275;

#include "codegen/il2cpp-codegen.h"

// System.Void SkyboxRotator::.ctor()
extern "C"  void SkyboxRotator__ctor_m3439010216 (SkyboxRotator_t551786275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxRotator::Update()
extern "C"  void SkyboxRotator_Update_m961996389 (SkyboxRotator_t551786275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxRotator::ToggleSkyboxRotation()
extern "C"  void SkyboxRotator_ToggleSkyboxRotation_m1119967416 (SkyboxRotator_t551786275 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
