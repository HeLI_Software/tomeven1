﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// enemy
struct  enemy_t96653192  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 enemy::enemyhealth
	int32_t ___enemyhealth_2;

public:
	inline static int32_t get_offset_of_enemyhealth_2() { return static_cast<int32_t>(offsetof(enemy_t96653192, ___enemyhealth_2)); }
	inline int32_t get_enemyhealth_2() const { return ___enemyhealth_2; }
	inline int32_t* get_address_of_enemyhealth_2() { return &___enemyhealth_2; }
	inline void set_enemyhealth_2(int32_t value)
	{
		___enemyhealth_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
