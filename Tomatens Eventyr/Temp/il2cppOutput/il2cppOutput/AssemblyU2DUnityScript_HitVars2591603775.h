﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// HitVars
struct  HitVars_t2591603775  : public MonoBehaviour_t667441552
{
public:

public:
};

struct HitVars_t2591603775_StaticFields
{
public:
	// System.Int32 HitVars::hitpoints
	int32_t ___hitpoints_2;
	// System.Single HitVars::totarget
	float ___totarget_3;
	// System.Single HitVars::range
	float ___range_4;

public:
	inline static int32_t get_offset_of_hitpoints_2() { return static_cast<int32_t>(offsetof(HitVars_t2591603775_StaticFields, ___hitpoints_2)); }
	inline int32_t get_hitpoints_2() const { return ___hitpoints_2; }
	inline int32_t* get_address_of_hitpoints_2() { return &___hitpoints_2; }
	inline void set_hitpoints_2(int32_t value)
	{
		___hitpoints_2 = value;
	}

	inline static int32_t get_offset_of_totarget_3() { return static_cast<int32_t>(offsetof(HitVars_t2591603775_StaticFields, ___totarget_3)); }
	inline float get_totarget_3() const { return ___totarget_3; }
	inline float* get_address_of_totarget_3() { return &___totarget_3; }
	inline void set_totarget_3(float value)
	{
		___totarget_3 = value;
	}

	inline static int32_t get_offset_of_range_4() { return static_cast<int32_t>(offsetof(HitVars_t2591603775_StaticFields, ___range_4)); }
	inline float get_range_4() const { return ___range_4; }
	inline float* get_address_of_range_4() { return &___range_4; }
	inline void set_range_4(float value)
	{
		___range_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
