﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ToPoteland
struct ToPoteland_t2745890390;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void ToPoteland::.ctor()
extern "C"  void ToPoteland__ctor_m72997946 (ToPoteland_t2745890390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ToPoteland::Main()
extern "C"  Il2CppObject * ToPoteland_Main_m205833045 (ToPoteland_t2745890390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
