﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(1)/$Main$52
struct U24MainU2452_t3645169472;
// text(1)
struct textU281U29_t2877074419;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_textU281U292877074419.h"

// System.Void text(1)/$Main$52::.ctor(text(1))
extern "C"  void U24MainU2452__ctor_m1775250447 (U24MainU2452_t3645169472 * __this, textU281U29_t2877074419 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text(1)/$Main$52::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2452_GetEnumerator_m522146288 (U24MainU2452_t3645169472 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
