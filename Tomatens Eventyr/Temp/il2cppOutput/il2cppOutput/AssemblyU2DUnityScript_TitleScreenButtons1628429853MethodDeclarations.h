﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// TitleScreenButtons
struct TitleScreenButtons_t1628429853;

#include "codegen/il2cpp-codegen.h"

// System.Void TitleScreenButtons::.ctor()
extern "C"  void TitleScreenButtons__ctor_m1889281875 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleScreenButtons::OnGUI()
extern "C"  void TitleScreenButtons_OnGUI_m1384680525 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void TitleScreenButtons::Main()
extern "C"  void TitleScreenButtons_Main_m3855764234 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
