﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk002
struct Peptalk002_t3966958763;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Peptalk002::.ctor()
extern "C"  void Peptalk002__ctor_m4053476741 (Peptalk002_t3966958763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Peptalk002::Main()
extern "C"  Il2CppObject * Peptalk002_Main_m3797918890 (Peptalk002_t3966958763 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
