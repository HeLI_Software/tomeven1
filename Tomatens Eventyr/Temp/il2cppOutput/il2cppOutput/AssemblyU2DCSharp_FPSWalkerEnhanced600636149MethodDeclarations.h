﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSWalkerEnhanced
struct FPSWalkerEnhanced_t600636149;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"

// System.Void FPSWalkerEnhanced::.ctor()
extern "C"  void FPSWalkerEnhanced__ctor_m2085172758 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::Start()
extern "C"  void FPSWalkerEnhanced_Start_m1032310550 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::FixedUpdate()
extern "C"  void FPSWalkerEnhanced_FixedUpdate_m1406905105 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::Update()
extern "C"  void FPSWalkerEnhanced_Update_m1942708151 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void FPSWalkerEnhanced_OnControllerColliderHit_m4190582318 (FPSWalkerEnhanced_t600636149 * __this, ControllerColliderHit_t2416790841 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSWalkerEnhanced::FallingDamageAlert(System.Single)
extern "C"  void FPSWalkerEnhanced_FallingDamageAlert_m4129903575 (FPSWalkerEnhanced_t600636149 * __this, float ___fallDistance0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
