﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// text(2)
struct textU282U29_t2877074450;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// text(2)/$Main$55/$
struct  U24_t2316117721  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// text(2) text(2)/$Main$55/$::$self_$56
	textU282U29_t2877074450 * ___U24self_U2456_2;

public:
	inline static int32_t get_offset_of_U24self_U2456_2() { return static_cast<int32_t>(offsetof(U24_t2316117721, ___U24self_U2456_2)); }
	inline textU282U29_t2877074450 * get_U24self_U2456_2() const { return ___U24self_U2456_2; }
	inline textU282U29_t2877074450 ** get_address_of_U24self_U2456_2() { return &___U24self_U2456_2; }
	inline void set_U24self_U2456_2(textU282U29_t2877074450 * value)
	{
		___U24self_U2456_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2456_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
