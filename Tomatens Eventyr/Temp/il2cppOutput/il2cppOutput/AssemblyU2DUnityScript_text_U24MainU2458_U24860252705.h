﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// text
struct text_t3556653;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// text/$Main$58/$
struct  U24_t860252705  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// text text/$Main$58/$::$self_$59
	text_t3556653 * ___U24self_U2459_2;

public:
	inline static int32_t get_offset_of_U24self_U2459_2() { return static_cast<int32_t>(offsetof(U24_t860252705, ___U24self_U2459_2)); }
	inline text_t3556653 * get_U24self_U2459_2() const { return ___U24self_U2459_2; }
	inline text_t3556653 ** get_address_of_U24self_U2459_2() { return &___U24self_U2459_2; }
	inline void set_U24self_U2459_2(text_t3556653 * value)
	{
		___U24self_U2459_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2459_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
