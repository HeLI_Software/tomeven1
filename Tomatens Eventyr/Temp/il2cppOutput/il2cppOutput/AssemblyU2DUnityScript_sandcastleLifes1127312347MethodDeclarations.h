﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// sandcastleLifes
struct sandcastleLifes_t1127312347;

#include "codegen/il2cpp-codegen.h"

// System.Void sandcastleLifes::.ctor()
extern "C"  void sandcastleLifes__ctor_m1835958747 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sandcastleLifes::.cctor()
extern "C"  void sandcastleLifes__cctor_m598050098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sandcastleLifes::DeductPoints(System.Int32)
extern "C"  void sandcastleLifes_DeductPoints_m892424416 (sandcastleLifes_t1127312347 * __this, int32_t ___hitpoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sandcastleLifes::Update()
extern "C"  void sandcastleLifes_Update_m2807008402 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void sandcastleLifes::Main()
extern "C"  void sandcastleLifes_Main_m528908162 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
