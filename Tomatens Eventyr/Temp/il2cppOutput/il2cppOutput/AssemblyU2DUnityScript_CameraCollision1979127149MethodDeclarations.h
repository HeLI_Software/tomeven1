﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CameraCollision
struct CameraCollision_t1979127149;

#include "codegen/il2cpp-codegen.h"

// System.Void CameraCollision::.ctor()
extern "C"  void CameraCollision__ctor_m1440316553 (CameraCollision_t1979127149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraCollision::Start()
extern "C"  void CameraCollision_Start_m387454345 (CameraCollision_t1979127149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraCollision::Update()
extern "C"  void CameraCollision_Update_m3427002276 (CameraCollision_t1979127149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CameraCollision::Main()
extern "C"  void CameraCollision_Main_m2040166164 (CameraCollision_t1979127149 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
