﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AdvancedAIV2
struct AdvancedAIV2_t2935784454;

#include "codegen/il2cpp-codegen.h"

// System.Void AdvancedAIV2::.ctor()
extern "C"  void AdvancedAIV2__ctor_m1907856714 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::Start()
extern "C"  void AdvancedAIV2_Start_m854994506 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::Update()
extern "C"  void AdvancedAIV2_Update_m740878083 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::lookAt()
extern "C"  void AdvancedAIV2_lookAt_m1605957388 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::chase()
extern "C"  void AdvancedAIV2_chase_m1826617078 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::attack()
extern "C"  void AdvancedAIV2_attack_m4187346082 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::ApplyDammage()
extern "C"  void AdvancedAIV2_ApplyDammage_m2140479086 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AdvancedAIV2::Main()
extern "C"  void AdvancedAIV2_Main_m254132787 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
