﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// text(2)
struct textU282U29_t2877074450;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// text(2)/$Main$55
struct  U24MainU2455_t1848218116  : public GenericGenerator_1_t207610107
{
public:
	// text(2) text(2)/$Main$55::$self_$57
	textU282U29_t2877074450 * ___U24self_U2457_0;

public:
	inline static int32_t get_offset_of_U24self_U2457_0() { return static_cast<int32_t>(offsetof(U24MainU2455_t1848218116, ___U24self_U2457_0)); }
	inline textU282U29_t2877074450 * get_U24self_U2457_0() const { return ___U24self_U2457_0; }
	inline textU282U29_t2877074450 ** get_address_of_U24self_U2457_0() { return &___U24self_U2457_0; }
	inline void set_U24self_U2457_0(textU282U29_t2877074450 * value)
	{
		___U24self_U2457_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2457_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
