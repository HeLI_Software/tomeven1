﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AItest
struct AItest_t1931868346;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;
// System.Object
struct Il2CppObject;
// UnityEngine.Collider
struct Collider_t2939674232;
// coinsound
struct coinsound_t3586493726;
// Dimensions
struct Dimensions_t2407799789;
// System.String
struct String_t;
// Example
struct Example_t341682506;
// FPSWalkerEnhanced
struct FPSWalkerEnhanced_t600636149;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// levelManager
struct levelManager_t2114375561;
// PixelMapTerrain
struct PixelMapTerrain_t2288975167;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// UnityEngine.MeshFilter
struct MeshFilter_t3839065225;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;
// SkyboxChanger
struct SkyboxChanger_t4201523768;
// UnityEngine.UI.Dropdown
struct Dropdown_t4201779933;
// SkyboxRotator
struct SkyboxRotator_t551786275;
// SmoothMouseLook
struct SmoothMouseLook_t1581351606;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// SUN
struct SUN_t82476;
// testai
struct testai_t3417797690;
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip
struct ProtectCameraFromWallClip_t1836063158;
// UnityEngine.Camera
struct Camera_t2727095145;
// UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer
struct RayHitComparer_t1977523238;
// WaterFlowFREEDemo
struct WaterFlowFREEDemo_t2180500532;
// UnityEngine.Renderer
struct Renderer_t3076687687;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790.h"
#include "AssemblyU2DCSharp_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DCSharp_AItest1931868346.h"
#include "AssemblyU2DCSharp_AItest1931868346MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "UnityEngine_UnityEngine_NavMeshAgent588466745.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "AssemblyU2DCSharp_coinsound3586493726.h"
#include "AssemblyU2DCSharp_coinsound3586493726MethodDeclarations.h"
#include "AssemblyU2DCSharp_Dimensions2407799789.h"
#include "AssemblyU2DCSharp_Dimensions2407799789MethodDeclarations.h"
#include "mscorlib_System_Object4170816371MethodDeclarations.h"
#include "mscorlib_System_Int321153838500.h"
#include "mscorlib_System_String7231557.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Object4170816371.h"
#include "AssemblyU2DCSharp_Example341682506.h"
#include "AssemblyU2DCSharp_Example341682506MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "AssemblyU2DCSharp_FPSWalkerEnhanced600636149.h"
#include "AssemblyU2DCSharp_FPSWalkerEnhanced600636149MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841MethodDeclarations.h"
#include "AssemblyU2DCSharp_levelManager2114375561.h"
#include "AssemblyU2DCSharp_levelManager2114375561MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SceneManagement_SceneManag2940962239MethodDeclarations.h"
#include "AssemblyU2DCSharp_PixelMapTerrain2288975167.h"
#include "AssemblyU2DCSharp_PixelMapTerrain2288975167MethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Mesh4241756145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen2522024052.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"
#include "UnityEngine_UnityEngine_Mathf4203372500MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_MeshFilter3839065225.h"
#include "UnityEngine_UnityEngine_MeshRenderer2804666580.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture2526458961.h"
#include "UnityEngine_UnityEngine_Mathf4203372500.h"
#include "AssemblyU2DCSharp_SkyboxChanger4201523768.h"
#include "AssemblyU2DCSharp_SkyboxChanger4201523768MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933.h"
#include "UnityEngine_UI_UnityEngine_UI_Dropdown4201779933MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderSettings425127197MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Material3870600107MethodDeclarations.h"
#include "AssemblyU2DCSharp_SkyboxRotator551786275.h"
#include "AssemblyU2DCSharp_SkyboxRotator551786275MethodDeclarations.h"
#include "AssemblyU2DCSharp_SmoothMouseLook1581351606.h"
#include "AssemblyU2DCSharp_SmoothMouseLook1581351606MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen1365137228.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes4275115676.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DCSharp_SmoothMouseLook_RotationAxes4275115676MethodDeclarations.h"
#include "AssemblyU2DCSharp_SUN82476.h"
#include "AssemblyU2DCSharp_SUN82476MethodDeclarations.h"
#include "AssemblyU2DCSharp_testai3417797690.h"
#include "AssemblyU2DCSharp_testai3417797690MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Prot1836063158.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Prot1836063158MethodDeclarations.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Prot1977523238MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "AssemblyU2DCSharp_UnityStandardAssets_Cameras_Prot1977523238.h"
#include "UnityEngine_UnityEngine_Ray3134616544MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "mscorlib_System_Array1146569071MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Ray3134616544.h"
#include "mscorlib_System_Single4291918972MethodDeclarations.h"
#include "AssemblyU2DCSharp_WaterFlowFREEDemo2180500532.h"
#include "AssemblyU2DCSharp_WaterFlowFREEDemo2180500532MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Events_UnityEvent_1_gen183549189MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer3076687687.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider79469677.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_SliderEvent2627072750.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen4186098975.h"
#include "mscorlib_System_IntPtr4010401971.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.NavMeshAgent>()
#define Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649(__this, method) ((  NavMeshAgent_t588466745 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.CharacterController>()
#define Component_GetComponent_TisCharacterController_t1618060635_m2645817963(__this, method) ((  CharacterController_t1618060635 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshFilter>()
#define GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(__this, method) ((  MeshFilter_t3839065225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.MeshRenderer>()
#define GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(__this, method) ((  MeshRenderer_t2804666580 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_AddComponent_TisIl2CppObject_m337943659_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisIl2CppObject_m337943659(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshFilter>()
#define GameObject_AddComponent_TisMeshFilter_t3839065225_m638563702(__this, method) ((  MeshFilter_t3839065225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.MeshRenderer>()
#define GameObject_AddComponent_TisMeshRenderer_t2804666580_m3074975883(__this, method) ((  MeshRenderer_t2804666580 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_AddComponent_TisIl2CppObject_m337943659_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Dropdown>()
#define Component_GetComponent_TisDropdown_t4201779933_m1526532837(__this, method) ((  Dropdown_t4201779933 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, method) ((  Rigidbody_t3346577219 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C"  Il2CppObject * Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisIl2CppObject_m900797242(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
#define Component_GetComponentInChildren_TisCamera_t2727095145_m140979021(__this, method) ((  Camera_t2727095145 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponentInChildren_TisIl2CppObject_m900797242_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.Renderer>()
#define GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(__this, method) ((  Renderer_t3076687687 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AItest::.ctor()
extern "C"  void AItest__ctor_m901029153 (AItest_t1931868346 * __this, const MethodInfo* method)
{
	{
		__this->set_searchingTurnSpeed_2((120.0f));
		__this->set_searchingDuration_3((4.0f));
		__this->set_sightRange_4((20.0f));
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		__this->set_offset_7(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AItest::Awake()
extern const MethodInfo* Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649_MethodInfo_var;
extern const uint32_t AItest_Awake_m1138634372_MetadataUsageId;
extern "C"  void AItest_Awake_m1138634372 (AItest_t1931868346 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AItest_Awake_m1138634372_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NavMeshAgent_t588466745 * L_0 = Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649_MethodInfo_var);
		__this->set_navMeshAgent_10(L_0);
		return;
	}
}
// System.Void AItest::Start()
extern "C"  void AItest_Start_m4143134241 (AItest_t1931868346 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AItest::Update()
extern "C"  void AItest_Update_m3888962060 (AItest_t1931868346 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AItest::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void AItest_OnTriggerEnter_m3085602295 (AItest_t1931868346 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void coinsound::.ctor()
extern "C"  void coinsound__ctor_m1759472141 (coinsound_t3586493726 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void coinsound::Start()
extern "C"  void coinsound_Start_m706609933 (coinsound_t3586493726 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void coinsound::Update()
extern "C"  void coinsound_Update_m435923616 (coinsound_t3586493726 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Dimensions::.ctor()
extern "C"  void Dimensions__ctor_m666081102 (Dimensions_t2407799789 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		__this->set_x_0(((int32_t)128));
		__this->set_y_1(((int32_t)128));
		__this->set_blockSize_2(1);
		__this->set_height_3(2);
		return;
	}
}
// System.String Dimensions::ToString()
extern Il2CppClass* ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3770157569;
extern Il2CppCodeGenString* _stringLiteral1396;
extern Il2CppCodeGenString* _stringLiteral667062683;
extern Il2CppCodeGenString* _stringLiteral2581570743;
extern const uint32_t Dimensions_ToString_m481552133_MetadataUsageId;
extern "C"  String_t* Dimensions_ToString_m481552133 (Dimensions_t2407799789 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Dimensions_ToString_m481552133_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t1108656482* L_0 = ((ObjectU5BU5D_t1108656482*)SZArrayNew(ObjectU5BU5D_t1108656482_il2cpp_TypeInfo_var, (uint32_t)8));
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, _stringLiteral3770157569);
		(L_0)->SetAt(static_cast<il2cpp_array_size_t>(0), (Il2CppObject *)_stringLiteral3770157569);
		ObjectU5BU5D_t1108656482* L_1 = L_0;
		int32_t L_2 = __this->get_x_0();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 1);
		ArrayElementTypeCheck (L_1, L_4);
		(L_1)->SetAt(static_cast<il2cpp_array_size_t>(1), (Il2CppObject *)L_4);
		ObjectU5BU5D_t1108656482* L_5 = L_1;
		NullCheck(L_5);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_5, 2);
		ArrayElementTypeCheck (L_5, _stringLiteral1396);
		(L_5)->SetAt(static_cast<il2cpp_array_size_t>(2), (Il2CppObject *)_stringLiteral1396);
		ObjectU5BU5D_t1108656482* L_6 = L_5;
		int32_t L_7 = __this->get_y_1();
		int32_t L_8 = L_7;
		Il2CppObject * L_9 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 3);
		ArrayElementTypeCheck (L_6, L_9);
		(L_6)->SetAt(static_cast<il2cpp_array_size_t>(3), (Il2CppObject *)L_9);
		ObjectU5BU5D_t1108656482* L_10 = L_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 4);
		ArrayElementTypeCheck (L_10, _stringLiteral667062683);
		(L_10)->SetAt(static_cast<il2cpp_array_size_t>(4), (Il2CppObject *)_stringLiteral667062683);
		ObjectU5BU5D_t1108656482* L_11 = L_10;
		int32_t L_12 = __this->get_blockSize_2();
		int32_t L_13 = L_12;
		Il2CppObject * L_14 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_13);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 5);
		ArrayElementTypeCheck (L_11, L_14);
		(L_11)->SetAt(static_cast<il2cpp_array_size_t>(5), (Il2CppObject *)L_14);
		ObjectU5BU5D_t1108656482* L_15 = L_11;
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 6);
		ArrayElementTypeCheck (L_15, _stringLiteral2581570743);
		(L_15)->SetAt(static_cast<il2cpp_array_size_t>(6), (Il2CppObject *)_stringLiteral2581570743);
		ObjectU5BU5D_t1108656482* L_16 = L_15;
		int32_t L_17 = __this->get_height_3();
		int32_t L_18 = L_17;
		Il2CppObject * L_19 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_18);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 7);
		ArrayElementTypeCheck (L_16, L_19);
		(L_16)->SetAt(static_cast<il2cpp_array_size_t>(7), (Il2CppObject *)L_19);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_20 = String_Concat_m3016520001(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Void Example::.ctor()
extern "C"  void Example__ctor_m1330927969 (Example_t341682506 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Example::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Example_OnTriggerEnter_m3487513015 (Example_t341682506 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	{
		Collider_t2939674232 * L_0 = ___other0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSWalkerEnhanced::.ctor()
extern "C"  void FPSWalkerEnhanced__ctor_m2085172758 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method)
{
	{
		__this->set_walkSpeed_2((6.0f));
		__this->set_runSpeed_3((11.0f));
		__this->set_limitDiagonalSpeed_4((bool)1);
		__this->set_jumpSpeed_6((8.0f));
		__this->set_gravity_7((20.0f));
		__this->set_fallingDamageThreshold_8((10.0f));
		__this->set_slideSpeed_11((12.0f));
		__this->set_antiBumpFactor_13((0.75f));
		__this->set_antiBunnyHopFactor_14(1);
		Vector3_t4282066566  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void FPSWalkerEnhanced::Start()
extern const MethodInfo* Component_GetComponent_TisCharacterController_t1618060635_m2645817963_MethodInfo_var;
extern const uint32_t FPSWalkerEnhanced_Start_m1032310550_MetadataUsageId;
extern "C"  void FPSWalkerEnhanced_Start_m1032310550 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSWalkerEnhanced_Start_m1032310550_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		CharacterController_t1618060635 * L_0 = Component_GetComponent_TisCharacterController_t1618060635_m2645817963(__this, /*hidden argument*/Component_GetComponent_TisCharacterController_t1618060635_m2645817963_MethodInfo_var);
		__this->set_controller_17(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		__this->set_myTransform_18(L_1);
		float L_2 = __this->get_walkSpeed_2();
		__this->set_speed_19(L_2);
		CharacterController_t1618060635 * L_3 = __this->get_controller_17();
		NullCheck(L_3);
		float L_4 = CharacterController_get_height_m2077757108(L_3, /*hidden argument*/NULL);
		CharacterController_t1618060635 * L_5 = __this->get_controller_17();
		NullCheck(L_5);
		float L_6 = CharacterController_get_radius_m2930031455(L_5, /*hidden argument*/NULL);
		__this->set_rayDistance_24(((float)((float)((float)((float)L_4*(float)(0.5f)))+(float)L_6)));
		CharacterController_t1618060635 * L_7 = __this->get_controller_17();
		NullCheck(L_7);
		float L_8 = CharacterController_get_slopeLimit_m3981117981(L_7, /*hidden argument*/NULL);
		__this->set_slideLimit_23(((float)((float)L_8-(float)(0.1f))));
		int32_t L_9 = __this->get_antiBunnyHopFactor_14();
		__this->set_jumpTimer_27(L_9);
		return;
	}
}
// System.Void FPSWalkerEnhanced::FixedUpdate()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral3381094468;
extern Il2CppCodeGenString* _stringLiteral2375469974;
extern Il2CppCodeGenString* _stringLiteral82539;
extern Il2CppCodeGenString* _stringLiteral79973777;
extern Il2CppCodeGenString* _stringLiteral2320462;
extern const uint32_t FPSWalkerEnhanced_FixedUpdate_m1406905105_MetadataUsageId;
extern "C"  void FPSWalkerEnhanced_FixedUpdate_m1406905105 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSWalkerEnhanced_FixedUpdate_m1406905105_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	bool V_3 = false;
	Vector3_t4282066566  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Vector3_t4282066566  V_5;
	memset(&V_5, 0, sizeof(V_5));
	Vector3_t4282066566  V_6;
	memset(&V_6, 0, sizeof(V_6));
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float G_B5_0 = 0.0f;
	FPSWalkerEnhanced_t600636149 * G_B18_0 = NULL;
	FPSWalkerEnhanced_t600636149 * G_B17_0 = NULL;
	float G_B19_0 = 0.0f;
	FPSWalkerEnhanced_t600636149 * G_B19_1 = NULL;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral3381094468, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2375469974, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = V_0;
		if ((((float)L_2) == ((float)(0.0f))))
		{
			goto IL_0041;
		}
	}
	{
		float L_3 = V_1;
		if ((((float)L_3) == ((float)(0.0f))))
		{
			goto IL_0041;
		}
	}
	{
		bool L_4 = __this->get_limitDiagonalSpeed_4();
		if (!L_4)
		{
			goto IL_0041;
		}
	}
	{
		G_B5_0 = (0.7071f);
		goto IL_0046;
	}

IL_0041:
	{
		G_B5_0 = (1.0f);
	}

IL_0046:
	{
		V_2 = G_B5_0;
		bool L_5 = __this->get_grounded_16();
		if (!L_5)
		{
			goto IL_02a2;
		}
	}
	{
		V_3 = (bool)0;
		Transform_t1659122786 * L_6 = __this->get_myTransform_18();
		NullCheck(L_6);
		Vector3_t4282066566  L_7 = Transform_get_position_m2211398607(L_6, /*hidden argument*/NULL);
		Vector3_t4282066566  L_8 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_10 = __this->get_address_of_hit_20();
		float L_11 = __this->get_rayDistance_24();
		bool L_12 = Physics_Raycast_m395141497(NULL /*static, unused*/, L_7, L_9, L_10, L_11, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00a6;
		}
	}
	{
		RaycastHit_t4003175726 * L_13 = __this->get_address_of_hit_20();
		Vector3_t4282066566  L_14 = RaycastHit_get_normal_m1346998891(L_13, /*hidden argument*/NULL);
		Vector3_t4282066566  L_15 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_16 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		float L_17 = __this->get_slideLimit_23();
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_00a1;
		}
	}
	{
		V_3 = (bool)1;
	}

IL_00a1:
	{
		goto IL_00ee;
	}

IL_00a6:
	{
		Vector3_t4282066566  L_18 = __this->get_contactPoint_25();
		Vector3_t4282066566  L_19 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_20 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		Vector3_t4282066566  L_21 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_22 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_21, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_23 = __this->get_address_of_hit_20();
		Physics_Raycast_m2482317716(NULL /*static, unused*/, L_20, L_22, L_23, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_24 = __this->get_address_of_hit_20();
		Vector3_t4282066566  L_25 = RaycastHit_get_normal_m1346998891(L_24, /*hidden argument*/NULL);
		Vector3_t4282066566  L_26 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_25, L_26, /*hidden argument*/NULL);
		float L_28 = __this->get_slideLimit_23();
		if ((!(((float)L_27) > ((float)L_28))))
		{
			goto IL_00ee;
		}
	}
	{
		V_3 = (bool)1;
	}

IL_00ee:
	{
		bool L_29 = __this->get_falling_22();
		if (!L_29)
		{
			goto IL_0147;
		}
	}
	{
		__this->set_falling_22((bool)0);
		Transform_t1659122786 * L_30 = __this->get_myTransform_18();
		NullCheck(L_30);
		Vector3_t4282066566  L_31 = Transform_get_position_m2211398607(L_30, /*hidden argument*/NULL);
		V_5 = L_31;
		float L_32 = (&V_5)->get_y_2();
		float L_33 = __this->get_fallStartLevel_21();
		float L_34 = __this->get_fallingDamageThreshold_8();
		if ((!(((float)L_32) < ((float)((float)((float)L_33-(float)L_34))))))
		{
			goto IL_0147;
		}
	}
	{
		float L_35 = __this->get_fallStartLevel_21();
		Transform_t1659122786 * L_36 = __this->get_myTransform_18();
		NullCheck(L_36);
		Vector3_t4282066566  L_37 = Transform_get_position_m2211398607(L_36, /*hidden argument*/NULL);
		V_6 = L_37;
		float L_38 = (&V_6)->get_y_2();
		FPSWalkerEnhanced_FallingDamageAlert_m4129903575(__this, ((float)((float)L_35-(float)L_38)), /*hidden argument*/NULL);
	}

IL_0147:
	{
		bool L_39 = __this->get_toggleRun_5();
		if (L_39)
		{
			goto IL_0178;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_40 = Input_GetButton_m4226175975(NULL /*static, unused*/, _stringLiteral82539, /*hidden argument*/NULL);
		G_B17_0 = __this;
		if (!L_40)
		{
			G_B18_0 = __this;
			goto IL_016d;
		}
	}
	{
		float L_41 = __this->get_runSpeed_3();
		G_B19_0 = L_41;
		G_B19_1 = G_B17_0;
		goto IL_0173;
	}

IL_016d:
	{
		float L_42 = __this->get_walkSpeed_2();
		G_B19_0 = L_42;
		G_B19_1 = G_B18_0;
	}

IL_0173:
	{
		NullCheck(G_B19_1);
		G_B19_1->set_speed_19(G_B19_0);
	}

IL_0178:
	{
		bool L_43 = V_3;
		if (!L_43)
		{
			goto IL_0189;
		}
	}
	{
		bool L_44 = __this->get_slideWhenOverSlopeLimit_9();
		if (L_44)
		{
			goto IL_01b3;
		}
	}

IL_0189:
	{
		bool L_45 = __this->get_slideOnTaggedObjects_10();
		if (!L_45)
		{
			goto IL_0211;
		}
	}
	{
		RaycastHit_t4003175726 * L_46 = __this->get_address_of_hit_20();
		Collider_t2939674232 * L_47 = RaycastHit_get_collider_m3116882274(L_46, /*hidden argument*/NULL);
		NullCheck(L_47);
		String_t* L_48 = Component_get_tag_m217485006(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_49 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_48, _stringLiteral79973777, /*hidden argument*/NULL);
		if (!L_49)
		{
			goto IL_0211;
		}
	}

IL_01b3:
	{
		RaycastHit_t4003175726 * L_50 = __this->get_address_of_hit_20();
		Vector3_t4282066566  L_51 = RaycastHit_get_normal_m1346998891(L_50, /*hidden argument*/NULL);
		V_4 = L_51;
		float L_52 = (&V_4)->get_x_1();
		float L_53 = (&V_4)->get_y_2();
		float L_54 = (&V_4)->get_z_3();
		Vector3_t4282066566  L_55;
		memset(&L_55, 0, sizeof(L_55));
		Vector3__ctor_m2926210380(&L_55, L_52, ((-L_53)), L_54, /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_55);
		Vector3_t4282066566 * L_56 = __this->get_address_of_moveDirection_15();
		Vector3_OrthoNormalize_m1810797014(NULL /*static, unused*/, (&V_4), L_56, /*hidden argument*/NULL);
		Vector3_t4282066566  L_57 = __this->get_moveDirection_15();
		float L_58 = __this->get_slideSpeed_11();
		Vector3_t4282066566  L_59 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_59);
		__this->set_playerControl_26((bool)0);
		goto IL_0252;
	}

IL_0211:
	{
		float L_60 = V_0;
		float L_61 = V_2;
		float L_62 = __this->get_antiBumpFactor_13();
		float L_63 = V_1;
		float L_64 = V_2;
		Vector3_t4282066566  L_65;
		memset(&L_65, 0, sizeof(L_65));
		Vector3__ctor_m2926210380(&L_65, ((float)((float)L_60*(float)L_61)), ((-L_62)), ((float)((float)L_63*(float)L_64)), /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_65);
		Transform_t1659122786 * L_66 = __this->get_myTransform_18();
		Vector3_t4282066566  L_67 = __this->get_moveDirection_15();
		NullCheck(L_66);
		Vector3_t4282066566  L_68 = Transform_TransformDirection_m83001769(L_66, L_67, /*hidden argument*/NULL);
		float L_69 = __this->get_speed_19();
		Vector3_t4282066566  L_70 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_68, L_69, /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_70);
		__this->set_playerControl_26((bool)1);
	}

IL_0252:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_71 = Input_GetButton_m4226175975(NULL /*static, unused*/, _stringLiteral2320462, /*hidden argument*/NULL);
		if (L_71)
		{
			goto IL_0274;
		}
	}
	{
		int32_t L_72 = __this->get_jumpTimer_27();
		__this->set_jumpTimer_27(((int32_t)((int32_t)L_72+(int32_t)1)));
		goto IL_029d;
	}

IL_0274:
	{
		int32_t L_73 = __this->get_jumpTimer_27();
		int32_t L_74 = __this->get_antiBunnyHopFactor_14();
		if ((((int32_t)L_73) < ((int32_t)L_74)))
		{
			goto IL_029d;
		}
	}
	{
		Vector3_t4282066566 * L_75 = __this->get_address_of_moveDirection_15();
		float L_76 = __this->get_jumpSpeed_6();
		L_75->set_y_2(L_76);
		__this->set_jumpTimer_27(0);
	}

IL_029d:
	{
		goto IL_0325;
	}

IL_02a2:
	{
		bool L_77 = __this->get_falling_22();
		if (L_77)
		{
			goto IL_02ce;
		}
	}
	{
		__this->set_falling_22((bool)1);
		Transform_t1659122786 * L_78 = __this->get_myTransform_18();
		NullCheck(L_78);
		Vector3_t4282066566  L_79 = Transform_get_position_m2211398607(L_78, /*hidden argument*/NULL);
		V_7 = L_79;
		float L_80 = (&V_7)->get_y_2();
		__this->set_fallStartLevel_21(L_80);
	}

IL_02ce:
	{
		bool L_81 = __this->get_airControl_12();
		if (!L_81)
		{
			goto IL_0325;
		}
	}
	{
		bool L_82 = __this->get_playerControl_26();
		if (!L_82)
		{
			goto IL_0325;
		}
	}
	{
		Vector3_t4282066566 * L_83 = __this->get_address_of_moveDirection_15();
		float L_84 = V_0;
		float L_85 = __this->get_speed_19();
		float L_86 = V_2;
		L_83->set_x_1(((float)((float)((float)((float)L_84*(float)L_85))*(float)L_86)));
		Vector3_t4282066566 * L_87 = __this->get_address_of_moveDirection_15();
		float L_88 = V_1;
		float L_89 = __this->get_speed_19();
		float L_90 = V_2;
		L_87->set_z_3(((float)((float)((float)((float)L_88*(float)L_89))*(float)L_90)));
		Transform_t1659122786 * L_91 = __this->get_myTransform_18();
		Vector3_t4282066566  L_92 = __this->get_moveDirection_15();
		NullCheck(L_91);
		Vector3_t4282066566  L_93 = Transform_TransformDirection_m83001769(L_91, L_92, /*hidden argument*/NULL);
		__this->set_moveDirection_15(L_93);
	}

IL_0325:
	{
		Vector3_t4282066566 * L_94 = __this->get_address_of_moveDirection_15();
		Vector3_t4282066566 * L_95 = L_94;
		float L_96 = L_95->get_y_2();
		float L_97 = __this->get_gravity_7();
		float L_98 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_95->set_y_2(((float)((float)L_96-(float)((float)((float)L_97*(float)L_98)))));
		CharacterController_t1618060635 * L_99 = __this->get_controller_17();
		Vector3_t4282066566  L_100 = __this->get_moveDirection_15();
		float L_101 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_102 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_100, L_101, /*hidden argument*/NULL);
		NullCheck(L_99);
		int32_t L_103 = CharacterController_Move_m3043020731(L_99, L_102, /*hidden argument*/NULL);
		__this->set_grounded_16((bool)((((int32_t)((((int32_t)((int32_t)((int32_t)L_103&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void FPSWalkerEnhanced::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral82539;
extern const uint32_t FPSWalkerEnhanced_Update_m1942708151_MetadataUsageId;
extern "C"  void FPSWalkerEnhanced_Update_m1942708151 (FPSWalkerEnhanced_t600636149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSWalkerEnhanced_Update_m1942708151_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	FPSWalkerEnhanced_t600636149 * G_B5_0 = NULL;
	FPSWalkerEnhanced_t600636149 * G_B4_0 = NULL;
	float G_B6_0 = 0.0f;
	FPSWalkerEnhanced_t600636149 * G_B6_1 = NULL;
	{
		bool L_0 = __this->get_toggleRun_5();
		if (!L_0)
		{
			goto IL_004d;
		}
	}
	{
		bool L_1 = __this->get_grounded_16();
		if (!L_1)
		{
			goto IL_004d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetButtonDown_m1879002085(NULL /*static, unused*/, _stringLiteral82539, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_004d;
		}
	}
	{
		float L_3 = __this->get_speed_19();
		float L_4 = __this->get_walkSpeed_2();
		G_B4_0 = __this;
		if ((!(((float)L_3) == ((float)L_4))))
		{
			G_B5_0 = __this;
			goto IL_0042;
		}
	}
	{
		float L_5 = __this->get_runSpeed_3();
		G_B6_0 = L_5;
		G_B6_1 = G_B4_0;
		goto IL_0048;
	}

IL_0042:
	{
		float L_6 = __this->get_walkSpeed_2();
		G_B6_0 = L_6;
		G_B6_1 = G_B5_0;
	}

IL_0048:
	{
		NullCheck(G_B6_1);
		G_B6_1->set_speed_19(G_B6_0);
	}

IL_004d:
	{
		return;
	}
}
// System.Void FPSWalkerEnhanced::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern "C"  void FPSWalkerEnhanced_OnControllerColliderHit_m4190582318 (FPSWalkerEnhanced_t600636149 * __this, ControllerColliderHit_t2416790841 * ___hit0, const MethodInfo* method)
{
	{
		ControllerColliderHit_t2416790841 * L_0 = ___hit0;
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = ControllerColliderHit_get_point_m1436351445(L_0, /*hidden argument*/NULL);
		__this->set_contactPoint_25(L_1);
		return;
	}
}
// System.Void FPSWalkerEnhanced::FallingDamageAlert(System.Single)
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral778548567;
extern Il2CppCodeGenString* _stringLiteral1789787826;
extern const uint32_t FPSWalkerEnhanced_FallingDamageAlert_m4129903575_MetadataUsageId;
extern "C"  void FPSWalkerEnhanced_FallingDamageAlert_m4129903575 (FPSWalkerEnhanced_t600636149 * __this, float ___fallDistance0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (FPSWalkerEnhanced_FallingDamageAlert_m4129903575_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___fallDistance0;
		float L_1 = L_0;
		Il2CppObject * L_2 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m2809334143(NULL /*static, unused*/, _stringLiteral778548567, L_2, _stringLiteral1789787826, /*hidden argument*/NULL);
		MonoBehaviour_print_m1497342762(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void levelManager::.ctor()
extern "C"  void levelManager__ctor_m2387053874 (levelManager_t2114375561 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void levelManager::LoadLevel(System.String)
extern "C"  void levelManager_LoadLevel_m3956732980 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name0;
		SceneManager_LoadScene_m2167814033(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PixelMapTerrain::.ctor()
extern Il2CppClass* Dimensions_t2407799789_il2cpp_TypeInfo_var;
extern Il2CppClass* MaterialU5BU5D_t170856778_il2cpp_TypeInfo_var;
extern const uint32_t PixelMapTerrain__ctor_m2555920652_MetadataUsageId;
extern "C"  void PixelMapTerrain__ctor_m2555920652 (PixelMapTerrain_t2288975167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain__ctor_m2555920652_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dimensions_t2407799789 * L_0 = (Dimensions_t2407799789 *)il2cpp_codegen_object_new(Dimensions_t2407799789_il2cpp_TypeInfo_var);
		Dimensions__ctor_m666081102(L_0, /*hidden argument*/NULL);
		__this->set_dimensions_3(L_0);
		__this->set_slopeEdges_4((bool)1);
		__this->set_slopeTolerance_5(((int32_t)25));
		__this->set_mat_9(((MaterialU5BU5D_t170856778*)SZArrayNew(MaterialU5BU5D_t170856778_il2cpp_TypeInfo_var, (uint32_t)2)));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PixelMapTerrain::GenerateTerrain(UnityEngine.Texture2D,Dimensions,UnityEngine.Material[])
extern Il2CppClass* Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* List_1_t2522024052_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var;
extern Il2CppClass* Mesh_t4241756145_il2cpp_TypeInfo_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m1634217978_MethodInfo_var;
extern const MethodInfo* List_1_Add_m352670381_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m4283739906_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshFilter_t3839065225_m638563702_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisMeshRenderer_t2804666580_m3074975883_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral91082444;
extern const uint32_t PixelMapTerrain_GenerateTerrain_m2171995231_MetadataUsageId;
extern "C"  void PixelMapTerrain_GenerateTerrain_m2171995231 (PixelMapTerrain_t2288975167 * __this, Texture2D_t3884108195 * ___heightmap0, Dimensions_t2407799789 * ___dimensions1, MaterialU5BU5D_t170856778* ___mat2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_GenerateTerrain_m2171995231_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	SingleU5BU5D_t2316563989* V_0 = NULL;
	Int32U5BU5D_t3230847821* V_1 = NULL;
	SingleU5BU5D_t2316563989* V_2 = NULL;
	SingleU5BU5D_t2316563989* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	int32_t V_11 = 0;
	int32_t V_12 = 0;
	int32_t V_13 = 0;
	int32_t V_14 = 0;
	int32_t V_15 = 0;
	int32_t V_16 = 0;
	int32_t V_17 = 0;
	int32_t V_18 = 0;
	List_1_t2522024052 * V_19 = NULL;
	List_1_t2522024052 * V_20 = NULL;
	int32_t V_21 = 0;
	int32_t V_22 = 0;
	int32_t V_23 = 0;
	int32_t V_24 = 0;
	int32_t V_25 = 0;
	Mesh_t4241756145 * V_26 = NULL;
	{
		Texture2D_t3884108195 * L_0 = ___heightmap0;
		Dimensions_t2407799789 * L_1 = ___dimensions1;
		NullCheck(L_1);
		int32_t L_2 = L_1->get_height_3();
		SingleU5BU5D_t2316563989* L_3 = PixelMapTerrain_HeightFromImage_m1847910979(__this, L_0, (((float)((float)L_2))), /*hidden argument*/NULL);
		V_0 = L_3;
		SingleU5BU5D_t2316563989* L_4 = V_0;
		Dimensions_t2407799789 * L_5 = ___dimensions1;
		NullCheck(L_5);
		int32_t L_6 = L_5->get_blockSize_2();
		Dimensions_t2407799789 * L_7 = ___dimensions1;
		NullCheck(L_7);
		int32_t L_8 = L_7->get_height_3();
		Int32U5BU5D_t3230847821* L_9 = PixelMapTerrain_BlockifyHeights_m613137302(__this, L_4, L_6, L_8, /*hidden argument*/NULL);
		V_1 = L_9;
		Int32U5BU5D_t3230847821* L_10 = V_1;
		SingleU5BU5D_t2316563989* L_11 = PixelMapTerrain_CubeHeights_m2893317114(__this, L_10, /*hidden argument*/NULL);
		V_2 = L_11;
		SingleU5BU5D_t2316563989* L_12 = V_2;
		V_3 = L_12;
		SingleU5BU5D_t2316563989* L_13 = V_3;
		NullCheck(L_13);
		__this->set_vertices_6(((Vector3U5BU5D_t215400611*)SZArrayNew(Vector3U5BU5D_t215400611_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_13)->max_length)))))));
		Vector3U5BU5D_t215400611* L_14 = __this->get_vertices_6();
		NullCheck(L_14);
		__this->set_triangles_7(((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_14)->max_length))))*(int32_t)3))*(int32_t)3)))));
		SingleU5BU5D_t2316563989* L_15 = V_2;
		NullCheck(L_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_16 = sqrtf((((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_15)->max_length))))))));
		V_4 = (((int32_t)((int32_t)L_16)));
		int32_t L_17 = V_4;
		V_5 = L_17;
		V_6 = 0;
		V_7 = (0.0f);
		V_8 = (0.0f);
		bool L_18 = __this->get_slopeEdges_4();
		if (!L_18)
		{
			goto IL_008e;
		}
	}
	{
		Dimensions_t2407799789 * L_19 = ___dimensions1;
		NullCheck(L_19);
		int32_t L_20 = L_19->get_x_0();
		int32_t L_21 = V_4;
		V_9 = ((int32_t)((int32_t)L_20/(int32_t)L_21));
		goto IL_009b;
	}

IL_008e:
	{
		Dimensions_t2407799789 * L_22 = ___dimensions1;
		NullCheck(L_22);
		int32_t L_23 = L_22->get_x_0();
		int32_t L_24 = V_4;
		V_9 = ((int32_t)((int32_t)L_23/(int32_t)((int32_t)((int32_t)L_24/(int32_t)2))));
	}

IL_009b:
	{
		V_10 = 0;
		goto IL_013e;
	}

IL_00a3:
	{
		V_11 = 0;
		goto IL_00ff;
	}

IL_00ab:
	{
		Vector3U5BU5D_t215400611* L_25 = __this->get_vertices_6();
		int32_t L_26 = V_6;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		float L_27 = V_7;
		SingleU5BU5D_t2316563989* L_28 = V_3;
		int32_t L_29 = V_6;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		float L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		float L_32 = V_8;
		Vector3_t4282066566  L_33;
		memset(&L_33, 0, sizeof(L_33));
		Vector3__ctor_m2926210380(&L_33, L_27, L_31, L_32, /*hidden argument*/NULL);
		(*(Vector3_t4282066566 *)((L_25)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_26)))) = L_33;
		bool L_34 = __this->get_slopeEdges_4();
		if (!L_34)
		{
			goto IL_00e2;
		}
	}
	{
		float L_35 = V_7;
		int32_t L_36 = V_9;
		V_7 = ((float)((float)L_35+(float)(((float)((float)L_36)))));
		goto IL_00f3;
	}

IL_00e2:
	{
		int32_t L_37 = V_11;
		if (((int32_t)((int32_t)L_37%(int32_t)2)))
		{
			goto IL_00f3;
		}
	}
	{
		float L_38 = V_7;
		int32_t L_39 = V_9;
		V_7 = ((float)((float)L_38+(float)(((float)((float)L_39)))));
	}

IL_00f3:
	{
		int32_t L_40 = V_6;
		V_6 = ((int32_t)((int32_t)L_40+(int32_t)1));
		int32_t L_41 = V_11;
		V_11 = ((int32_t)((int32_t)L_41+(int32_t)1));
	}

IL_00ff:
	{
		int32_t L_42 = V_11;
		int32_t L_43 = V_4;
		if ((((int32_t)L_42) < ((int32_t)L_43)))
		{
			goto IL_00ab;
		}
	}
	{
		V_7 = (0.0f);
		bool L_44 = __this->get_slopeEdges_4();
		if (!L_44)
		{
			goto IL_0127;
		}
	}
	{
		float L_45 = V_8;
		int32_t L_46 = V_9;
		V_8 = ((float)((float)L_45+(float)(((float)((float)L_46)))));
		goto IL_0138;
	}

IL_0127:
	{
		int32_t L_47 = V_10;
		if (((int32_t)((int32_t)L_47%(int32_t)2)))
		{
			goto IL_0138;
		}
	}
	{
		float L_48 = V_8;
		int32_t L_49 = V_9;
		V_8 = ((float)((float)L_48+(float)(((float)((float)L_49)))));
	}

IL_0138:
	{
		int32_t L_50 = V_10;
		V_10 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_013e:
	{
		int32_t L_51 = V_10;
		int32_t L_52 = V_5;
		if ((((int32_t)L_51) < ((int32_t)L_52)))
		{
			goto IL_00a3;
		}
	}
	{
		int32_t L_53 = V_4;
		V_4 = ((int32_t)((int32_t)L_53-(int32_t)1));
		int32_t L_54 = V_5;
		V_5 = ((int32_t)((int32_t)L_54-(int32_t)1));
		V_16 = 0;
		int32_t L_55 = V_4;
		V_17 = ((int32_t)((int32_t)L_55+(int32_t)1));
		V_18 = 0;
		List_1_t2522024052 * L_56 = (List_1_t2522024052 *)il2cpp_codegen_object_new(List_1_t2522024052_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_56, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		V_19 = L_56;
		List_1_t2522024052 * L_57 = (List_1_t2522024052 *)il2cpp_codegen_object_new(List_1_t2522024052_il2cpp_TypeInfo_var);
		List_1__ctor_m1634217978(L_57, /*hidden argument*/List_1__ctor_m1634217978_MethodInfo_var);
		V_20 = L_57;
		V_21 = 0;
		goto IL_02f5;
	}

IL_0175:
	{
		V_22 = 0;
		goto IL_02df;
	}

IL_017d:
	{
		int32_t L_58 = V_22;
		int32_t L_59 = V_16;
		V_12 = ((int32_t)((int32_t)L_58+(int32_t)L_59));
		int32_t L_60 = V_22;
		int32_t L_61 = V_16;
		V_13 = ((int32_t)((int32_t)((int32_t)((int32_t)L_60+(int32_t)L_61))+(int32_t)1));
		int32_t L_62 = V_22;
		int32_t L_63 = V_16;
		int32_t L_64 = V_17;
		V_14 = ((int32_t)((int32_t)((int32_t)((int32_t)L_62+(int32_t)L_63))+(int32_t)L_64));
		int32_t L_65 = V_22;
		int32_t L_66 = V_16;
		int32_t L_67 = V_17;
		V_15 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_65+(int32_t)L_66))+(int32_t)L_67))+(int32_t)1));
		Vector3U5BU5D_t215400611* L_68 = __this->get_vertices_6();
		int32_t L_69 = V_12;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, L_69);
		Vector3U5BU5D_t215400611* L_70 = __this->get_vertices_6();
		int32_t L_71 = V_13;
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, L_71);
		Vector3U5BU5D_t215400611* L_72 = __this->get_vertices_6();
		int32_t L_73 = V_14;
		NullCheck(L_72);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_72, L_73);
		Vector3_t4282066566  L_74 = PixelMapTerrain_PlaneNormal_m3954937112(__this, (*(Vector3_t4282066566 *)((L_68)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_69)))), (*(Vector3_t4282066566 *)((L_70)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_71)))), (*(Vector3_t4282066566 *)((L_72)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_73)))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_75 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_76 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_74, L_75, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_77 = fabsf((fmodf(L_76, (180.0f))));
		int32_t L_78 = __this->get_slopeTolerance_5();
		if ((!(((float)L_77) > ((float)(((float)((float)L_78)))))))
		{
			goto IL_0220;
		}
	}
	{
		List_1_t2522024052 * L_79 = V_20;
		int32_t L_80 = V_12;
		NullCheck(L_79);
		List_1_Add_m352670381(L_79, L_80, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_81 = V_20;
		int32_t L_82 = V_14;
		NullCheck(L_81);
		List_1_Add_m352670381(L_81, L_82, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_83 = V_20;
		int32_t L_84 = V_13;
		NullCheck(L_83);
		List_1_Add_m352670381(L_83, L_84, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		goto IL_023b;
	}

IL_0220:
	{
		List_1_t2522024052 * L_85 = V_19;
		int32_t L_86 = V_12;
		NullCheck(L_85);
		List_1_Add_m352670381(L_85, L_86, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_87 = V_19;
		int32_t L_88 = V_14;
		NullCheck(L_87);
		List_1_Add_m352670381(L_87, L_88, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_89 = V_19;
		int32_t L_90 = V_13;
		NullCheck(L_89);
		List_1_Add_m352670381(L_89, L_90, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
	}

IL_023b:
	{
		Vector3U5BU5D_t215400611* L_91 = __this->get_vertices_6();
		int32_t L_92 = V_13;
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, L_92);
		Vector3U5BU5D_t215400611* L_93 = __this->get_vertices_6();
		int32_t L_94 = V_15;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, L_94);
		Vector3U5BU5D_t215400611* L_95 = __this->get_vertices_6();
		int32_t L_96 = V_14;
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, L_96);
		Vector3_t4282066566  L_97 = PixelMapTerrain_PlaneNormal_m3954937112(__this, (*(Vector3_t4282066566 *)((L_91)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_92)))), (*(Vector3_t4282066566 *)((L_93)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_94)))), (*(Vector3_t4282066566 *)((L_95)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_96)))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_98 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_99 = Vector3_Angle_m1904328934(NULL /*static, unused*/, L_97, L_98, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_100 = fabsf((fmodf(L_99, (180.0f))));
		int32_t L_101 = __this->get_slopeTolerance_5();
		if ((!(((float)L_100) > ((float)(((float)((float)L_101)))))))
		{
			goto IL_02b8;
		}
	}
	{
		List_1_t2522024052 * L_102 = V_20;
		int32_t L_103 = V_13;
		NullCheck(L_102);
		List_1_Add_m352670381(L_102, L_103, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_104 = V_20;
		int32_t L_105 = V_14;
		NullCheck(L_104);
		List_1_Add_m352670381(L_104, L_105, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_106 = V_20;
		int32_t L_107 = V_15;
		NullCheck(L_106);
		List_1_Add_m352670381(L_106, L_107, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		goto IL_02d3;
	}

IL_02b8:
	{
		List_1_t2522024052 * L_108 = V_19;
		int32_t L_109 = V_13;
		NullCheck(L_108);
		List_1_Add_m352670381(L_108, L_109, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_110 = V_19;
		int32_t L_111 = V_14;
		NullCheck(L_110);
		List_1_Add_m352670381(L_110, L_111, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
		List_1_t2522024052 * L_112 = V_19;
		int32_t L_113 = V_15;
		NullCheck(L_112);
		List_1_Add_m352670381(L_112, L_113, /*hidden argument*/List_1_Add_m352670381_MethodInfo_var);
	}

IL_02d3:
	{
		int32_t L_114 = V_18;
		V_18 = ((int32_t)((int32_t)L_114+(int32_t)6));
		int32_t L_115 = V_22;
		V_22 = ((int32_t)((int32_t)L_115+(int32_t)1));
	}

IL_02df:
	{
		int32_t L_116 = V_22;
		int32_t L_117 = V_4;
		if ((((int32_t)L_116) < ((int32_t)L_117)))
		{
			goto IL_017d;
		}
	}
	{
		int32_t L_118 = V_16;
		int32_t L_119 = V_17;
		V_16 = ((int32_t)((int32_t)L_118+(int32_t)L_119));
		int32_t L_120 = V_21;
		V_21 = ((int32_t)((int32_t)L_120+(int32_t)1));
	}

IL_02f5:
	{
		int32_t L_121 = V_21;
		int32_t L_122 = V_5;
		if ((((int32_t)L_121) < ((int32_t)L_122)))
		{
			goto IL_0175;
		}
	}
	{
		Vector3U5BU5D_t215400611* L_123 = __this->get_vertices_6();
		NullCheck(L_123);
		__this->set_uvs_8(((Vector2U5BU5D_t4024180168*)SZArrayNew(Vector2U5BU5D_t4024180168_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_123)->max_length)))))));
		V_23 = 0;
		V_6 = 0;
		V_24 = 0;
		int32_t L_124 = V_4;
		V_4 = ((int32_t)((int32_t)L_124+(int32_t)1));
		V_25 = 0;
		goto IL_0381;
	}

IL_0328:
	{
		int32_t L_125 = V_25;
		int32_t L_126 = V_4;
		V_16 = ((int32_t)((int32_t)L_125/(int32_t)L_126));
		int32_t L_127 = V_25;
		int32_t L_128 = V_4;
		V_24 = ((int32_t)((int32_t)L_127%(int32_t)L_128));
		int32_t L_129 = V_16;
		if (((int32_t)((int32_t)L_129%(int32_t)2)))
		{
			goto IL_0347;
		}
	}
	{
		V_6 = 0;
		goto IL_034a;
	}

IL_0347:
	{
		V_6 = 1;
	}

IL_034a:
	{
		int32_t L_130 = V_24;
		if (((int32_t)((int32_t)L_130%(int32_t)2)))
		{
			goto IL_035b;
		}
	}
	{
		V_23 = 0;
		goto IL_035e;
	}

IL_035b:
	{
		V_23 = 1;
	}

IL_035e:
	{
		Vector2U5BU5D_t4024180168* L_131 = __this->get_uvs_8();
		int32_t L_132 = V_25;
		NullCheck(L_131);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_131, L_132);
		int32_t L_133 = V_23;
		int32_t L_134 = V_16;
		Vector2_t4282066565  L_135;
		memset(&L_135, 0, sizeof(L_135));
		Vector2__ctor_m1517109030(&L_135, (((float)((float)L_133))), (((float)((float)L_134))), /*hidden argument*/NULL);
		(*(Vector2_t4282066565 *)((L_131)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_132)))) = L_135;
		int32_t L_136 = V_25;
		V_25 = ((int32_t)((int32_t)L_136+(int32_t)1));
	}

IL_0381:
	{
		int32_t L_137 = V_25;
		Vector2U5BU5D_t4024180168* L_138 = __this->get_uvs_8();
		NullCheck(L_138);
		if ((((int32_t)L_137) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_138)->max_length)))))))
		{
			goto IL_0328;
		}
	}
	{
		Mesh_t4241756145 * L_139 = (Mesh_t4241756145 *)il2cpp_codegen_object_new(Mesh_t4241756145_il2cpp_TypeInfo_var);
		Mesh__ctor_m2684203808(L_139, /*hidden argument*/NULL);
		V_26 = L_139;
		Mesh_t4241756145 * L_140 = V_26;
		GameObject_t3674682005 * L_141 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_141);
		String_t* L_142 = Object_get_name_m3709440845(L_141, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_143 = String_Concat_m138640077(NULL /*static, unused*/, L_142, _stringLiteral91082444, /*hidden argument*/NULL);
		NullCheck(L_140);
		Object_set_name_m1123518500(L_140, L_143, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_144 = V_26;
		NullCheck(L_144);
		Mesh_Clear_m90337099(L_144, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_145 = V_26;
		Vector3U5BU5D_t215400611* L_146 = __this->get_vertices_6();
		NullCheck(L_145);
		Mesh_set_vertices_m2628866109(L_145, L_146, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_147 = V_26;
		NullCheck(L_147);
		Mesh_set_subMeshCount_m1647706132(L_147, 2, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_148 = V_26;
		List_1_t2522024052 * L_149 = V_19;
		NullCheck(L_149);
		Int32U5BU5D_t3230847821* L_150 = List_1_ToArray_m4283739906(L_149, /*hidden argument*/List_1_ToArray_m4283739906_MethodInfo_var);
		NullCheck(L_148);
		Mesh_SetTriangles_m636210907(L_148, L_150, 0, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_151 = V_26;
		List_1_t2522024052 * L_152 = V_20;
		NullCheck(L_152);
		Int32U5BU5D_t3230847821* L_153 = List_1_ToArray_m4283739906(L_152, /*hidden argument*/List_1_ToArray_m4283739906_MethodInfo_var);
		NullCheck(L_151);
		Mesh_SetTriangles_m636210907(L_151, L_153, 1, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_154 = V_26;
		Vector2U5BU5D_t4024180168* L_155 = __this->get_uvs_8();
		NullCheck(L_154);
		Mesh_set_uv_m498907190(L_154, L_155, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_156 = V_26;
		NullCheck(L_156);
		Mesh_RecalculateNormals_m1806625021(L_156, /*hidden argument*/NULL);
		Mesh_t4241756145 * L_157 = V_26;
		NullCheck(L_157);
		Mesh_Optimize_m386784321(L_157, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_158 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_158);
		MeshFilter_t3839065225 * L_159 = GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(L_158, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var);
		bool L_160 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_159, /*hidden argument*/NULL);
		if (!L_160)
		{
			goto IL_042d;
		}
	}
	{
		GameObject_t3674682005 * L_161 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_161);
		MeshFilter_t3839065225 * L_162 = GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(L_161, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_162, /*hidden argument*/NULL);
	}

IL_042d:
	{
		GameObject_t3674682005 * L_163 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_163);
		MeshRenderer_t2804666580 * L_164 = GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(L_163, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var);
		bool L_165 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_164, /*hidden argument*/NULL);
		if (!L_165)
		{
			goto IL_0452;
		}
	}
	{
		GameObject_t3674682005 * L_166 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_166);
		MeshRenderer_t2804666580 * L_167 = GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910(L_166, /*hidden argument*/GameObject_GetComponent_TisMeshRenderer_t2804666580_m2686897910_MethodInfo_var);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_167, /*hidden argument*/NULL);
	}

IL_0452:
	{
		GameObject_t3674682005 * L_168 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_168);
		MeshFilter_t3839065225 * L_169 = GameObject_AddComponent_TisMeshFilter_t3839065225_m638563702(L_168, /*hidden argument*/GameObject_AddComponent_TisMeshFilter_t3839065225_m638563702_MethodInfo_var);
		Mesh_t4241756145 * L_170 = V_26;
		NullCheck(L_169);
		MeshFilter_set_sharedMesh_m793190055(L_169, L_170, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_171 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_171);
		MeshRenderer_t2804666580 * L_172 = GameObject_AddComponent_TisMeshRenderer_t2804666580_m3074975883(L_171, /*hidden argument*/GameObject_AddComponent_TisMeshRenderer_t2804666580_m3074975883_MethodInfo_var);
		MaterialU5BU5D_t170856778* L_173 = ___mat2;
		NullCheck(L_172);
		Renderer_set_sharedMaterials_m1255100914(L_172, L_173, /*hidden argument*/NULL);
		return;
	}
}
// System.Single PixelMapTerrain::ColorToFloat(UnityEngine.Color)
extern "C"  float PixelMapTerrain_ColorToFloat_m1149907388 (PixelMapTerrain_t2288975167 * __this, Color_t4194546905  ___color0, const MethodInfo* method)
{
	{
		float L_0 = (&___color0)->get_r_0();
		float L_1 = (&___color0)->get_g_1();
		float L_2 = (&___color0)->get_b_2();
		return ((float)((float)(255.0f)*(float)((float)((float)((float)((float)L_0+(float)L_1))+(float)L_2))));
	}
}
// System.Single[] PixelMapTerrain::HeightFromImage(UnityEngine.Texture2D,System.Single)
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t PixelMapTerrain_HeightFromImage_m1847910979_MetadataUsageId;
extern "C"  SingleU5BU5D_t2316563989* PixelMapTerrain_HeightFromImage_m1847910979 (PixelMapTerrain_t2288975167 * __this, Texture2D_t3884108195 * ___img0, float ___heightModifier1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_HeightFromImage_m1847910979_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	ColorU5BU5D_t2441545636* V_0 = NULL;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	SingleU5BU5D_t2316563989* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	{
		Texture2D_t3884108195 * L_0 = ___img0;
		NullCheck(L_0);
		ColorU5BU5D_t2441545636* L_1 = Texture2D_GetPixels_m1759701362(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Texture2D_t3884108195 * L_2 = ___img0;
		NullCheck(L_2);
		int32_t L_3 = VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		V_1 = L_3;
		Texture2D_t3884108195 * L_4 = ___img0;
		NullCheck(L_4);
		int32_t L_5 = VirtFuncInvoker0< int32_t >::Invoke(6 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		V_2 = L_5;
		ColorU5BU5D_t2441545636* L_6 = V_0;
		NullCheck(L_6);
		V_3 = ((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_6)->max_length))))));
		V_4 = 0;
		int32_t L_7 = V_2;
		V_5 = ((int32_t)((int32_t)L_7-(int32_t)1));
		goto IL_0068;
	}

IL_002b:
	{
		int32_t L_8 = V_1;
		V_6 = ((int32_t)((int32_t)L_8-(int32_t)1));
		goto IL_005a;
	}

IL_0035:
	{
		SingleU5BU5D_t2316563989* L_9 = V_3;
		int32_t L_10 = V_4;
		ColorU5BU5D_t2441545636* L_11 = V_0;
		int32_t L_12 = V_6;
		int32_t L_13 = V_5;
		int32_t L_14 = V_2;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, ((int32_t)((int32_t)L_12+(int32_t)((int32_t)((int32_t)L_13*(int32_t)L_14)))));
		float L_15 = ((L_11)->GetAddressAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)L_12+(int32_t)((int32_t)((int32_t)L_13*(int32_t)L_14)))))))->get_a_3();
		float L_16 = ___heightModifier1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		(L_9)->SetAt(static_cast<il2cpp_array_size_t>(L_10), (float)((float)((float)L_15*(float)(((float)((float)L_16))))));
		int32_t L_17 = V_4;
		V_4 = ((int32_t)((int32_t)L_17+(int32_t)1));
		int32_t L_18 = V_6;
		V_6 = ((int32_t)((int32_t)L_18-(int32_t)1));
	}

IL_005a:
	{
		int32_t L_19 = V_6;
		if ((((int32_t)L_19) > ((int32_t)(-1))))
		{
			goto IL_0035;
		}
	}
	{
		int32_t L_20 = V_5;
		V_5 = ((int32_t)((int32_t)L_20-(int32_t)1));
	}

IL_0068:
	{
		int32_t L_21 = V_5;
		if ((((int32_t)L_21) > ((int32_t)(-1))))
		{
			goto IL_002b;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_22 = V_3;
		return L_22;
	}
}
// System.Void PixelMapTerrain::OnDestroy()
extern const MethodInfo* GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var;
extern const uint32_t PixelMapTerrain_OnDestroy_m3502944261_MetadataUsageId;
extern "C"  void PixelMapTerrain_OnDestroy_m3502944261 (PixelMapTerrain_t2288975167 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_OnDestroy_m3502944261_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		MeshFilter_t3839065225 * L_1 = GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873(L_0, /*hidden argument*/GameObject_GetComponent_TisMeshFilter_t3839065225_m3315251873_MethodInfo_var);
		NullCheck(L_1);
		Mesh_t4241756145 * L_2 = MeshFilter_get_sharedMesh_m2700148450(L_1, /*hidden argument*/NULL);
		Object_DestroyImmediate_m349958679(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32[] PixelMapTerrain::BlockifyHeights(System.Single[],System.Int32,System.Int32)
extern Il2CppClass* Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var;
extern const uint32_t PixelMapTerrain_BlockifyHeights_m613137302_MetadataUsageId;
extern "C"  Int32U5BU5D_t3230847821* PixelMapTerrain_BlockifyHeights_m613137302 (PixelMapTerrain_t2288975167 * __this, SingleU5BU5D_t2316563989* ___heights0, int32_t ___blockSize1, int32_t ___mapHeight2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_BlockifyHeights_m613137302_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Int32U5BU5D_t3230847821* V_0 = NULL;
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	{
		SingleU5BU5D_t2316563989* L_0 = ___heights0;
		NullCheck(L_0);
		V_0 = ((Int32U5BU5D_t3230847821*)SZArrayNew(Int32U5BU5D_t3230847821_il2cpp_TypeInfo_var, (uint32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))));
		int32_t L_1 = ___mapHeight2;
		V_1 = ((float)((float)(1.0f)/(float)(((float)((float)L_1)))));
		V_2 = 0;
		goto IL_0026;
	}

IL_0019:
	{
		Int32U5BU5D_t3230847821* L_2 = V_0;
		int32_t L_3 = V_2;
		SingleU5BU5D_t2316563989* L_4 = ___heights0;
		int32_t L_5 = V_2;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		float L_7 = (L_4)->GetAt(static_cast<il2cpp_array_size_t>(L_6));
		float L_8 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		(L_2)->SetAt(static_cast<il2cpp_array_size_t>(L_3), (int32_t)(((int32_t)((int32_t)((float)((float)L_7/(float)L_8))))));
		int32_t L_9 = V_2;
		V_2 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_10 = V_2;
		SingleU5BU5D_t2316563989* L_11 = ___heights0;
		NullCheck(L_11);
		if ((((int32_t)L_10) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_11)->max_length)))))))
		{
			goto IL_0019;
		}
	}
	{
		Int32U5BU5D_t3230847821* L_12 = V_0;
		return L_12;
	}
}
// System.Single[] PixelMapTerrain::CubeHeights(System.Int32[])
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern Il2CppClass* SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var;
extern const uint32_t PixelMapTerrain_CubeHeights_m2893317114_MetadataUsageId;
extern "C"  SingleU5BU5D_t2316563989* PixelMapTerrain_CubeHeights_m2893317114 (PixelMapTerrain_t2288975167 * __this, Int32U5BU5D_t3230847821* ___heights0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_CubeHeights_m2893317114_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	SingleU5BU5D_t2316563989* V_3 = NULL;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	{
		Int32U5BU5D_t3230847821* L_0 = ___heights0;
		NullCheck(L_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_1 = sqrtf((((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_0)->max_length))))))));
		V_0 = (((int32_t)((int32_t)L_1)));
		Int32U5BU5D_t3230847821* L_2 = ___heights0;
		NullCheck(L_2);
		float L_3 = sqrtf((((float)((float)(((int32_t)((int32_t)(((Il2CppArray *)L_2)->max_length))))))));
		V_1 = (((int32_t)((int32_t)L_3)));
		int32_t L_4 = V_0;
		V_2 = ((int32_t)((int32_t)L_4*(int32_t)2));
		Int32U5BU5D_t3230847821* L_5 = ___heights0;
		NullCheck(L_5);
		V_3 = ((SingleU5BU5D_t2316563989*)SZArrayNew(SingleU5BU5D_t2316563989_il2cpp_TypeInfo_var, (uint32_t)((int32_t)((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_5)->max_length))))*(int32_t)4))));
		V_4 = 0;
		int32_t L_6 = V_2;
		V_5 = L_6;
		V_6 = 0;
		V_7 = 0;
		goto IL_00a3;
	}

IL_0036:
	{
		V_8 = 0;
		goto IL_008c;
	}

IL_003e:
	{
		SingleU5BU5D_t2316563989* L_7 = V_3;
		int32_t L_8 = V_8;
		int32_t L_9 = V_4;
		Int32U5BU5D_t3230847821* L_10 = ___heights0;
		int32_t L_11 = V_6;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		int32_t L_12 = L_11;
		int32_t L_13 = (L_10)->GetAt(static_cast<il2cpp_array_size_t>(L_12));
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, ((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)2))+(int32_t)L_9)));
		(L_7)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)L_8*(int32_t)2))+(int32_t)L_9))), (float)(((float)((float)L_13))));
		SingleU5BU5D_t2316563989* L_14 = V_3;
		int32_t L_15 = V_8;
		int32_t L_16 = V_4;
		Int32U5BU5D_t3230847821* L_17 = ___heights0;
		int32_t L_18 = V_6;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		int32_t L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)2))+(int32_t)L_16))+(int32_t)1)));
		(L_14)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_15*(int32_t)2))+(int32_t)L_16))+(int32_t)1))), (float)(((float)((float)L_20))));
		SingleU5BU5D_t2316563989* L_21 = V_3;
		int32_t L_22 = V_8;
		int32_t L_23 = V_4;
		int32_t L_24 = V_5;
		Int32U5BU5D_t3230847821* L_25 = ___heights0;
		int32_t L_26 = V_6;
		NullCheck(L_25);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_25, L_26);
		int32_t L_27 = L_26;
		int32_t L_28 = (L_25)->GetAt(static_cast<il2cpp_array_size_t>(L_27));
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_22*(int32_t)2))+(int32_t)L_23))+(int32_t)L_24)));
		(L_21)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_22*(int32_t)2))+(int32_t)L_23))+(int32_t)L_24))), (float)(((float)((float)L_28))));
		SingleU5BU5D_t2316563989* L_29 = V_3;
		int32_t L_30 = V_8;
		int32_t L_31 = V_4;
		int32_t L_32 = V_5;
		Int32U5BU5D_t3230847821* L_33 = ___heights0;
		int32_t L_34 = V_6;
		NullCheck(L_33);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_33, L_34);
		int32_t L_35 = L_34;
		int32_t L_36 = (L_33)->GetAt(static_cast<il2cpp_array_size_t>(L_35));
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)2))+(int32_t)L_31))+(int32_t)L_32))+(int32_t)1)));
		(L_29)->SetAt(static_cast<il2cpp_array_size_t>(((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)L_30*(int32_t)2))+(int32_t)L_31))+(int32_t)L_32))+(int32_t)1))), (float)(((float)((float)L_36))));
		int32_t L_37 = V_6;
		V_6 = ((int32_t)((int32_t)L_37+(int32_t)1));
		int32_t L_38 = V_8;
		V_8 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_008c:
	{
		int32_t L_39 = V_8;
		int32_t L_40 = V_0;
		if ((((int32_t)L_39) < ((int32_t)L_40)))
		{
			goto IL_003e;
		}
	}
	{
		int32_t L_41 = V_4;
		int32_t L_42 = V_5;
		V_4 = ((int32_t)((int32_t)L_41+(int32_t)((int32_t)((int32_t)L_42*(int32_t)2))));
		int32_t L_43 = V_7;
		V_7 = ((int32_t)((int32_t)L_43+(int32_t)1));
	}

IL_00a3:
	{
		int32_t L_44 = V_7;
		int32_t L_45 = V_1;
		if ((((int32_t)L_44) < ((int32_t)L_45)))
		{
			goto IL_0036;
		}
	}
	{
		SingleU5BU5D_t2316563989* L_46 = V_3;
		return L_46;
	}
}
// UnityEngine.Vector3 PixelMapTerrain::PlaneNormal(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t PixelMapTerrain_PlaneNormal_m3954937112_MetadataUsageId;
extern "C"  Vector3_t4282066566  PixelMapTerrain_PlaneNormal_m3954937112 (PixelMapTerrain_t2288975167 * __this, Vector3_t4282066566  ___p00, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (PixelMapTerrain_PlaneNormal_m3954937112_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Vector3_t4282066566  L_0 = ___p11;
		Vector3_t4282066566  L_1 = ___p00;
		Vector3_t4282066566  L_2 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = ___p22;
		Vector3_t4282066566  L_4 = ___p00;
		Vector3_t4282066566  L_5 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		Vector3_t4282066566  L_6 = Vector3_Cross_m2894122475(NULL /*static, unused*/, L_2, L_5, /*hidden argument*/NULL);
		V_0 = L_6;
		float L_7 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_8 = ((Mathf_t4203372500_StaticFields*)Mathf_t4203372500_il2cpp_TypeInfo_var->static_fields)->get_Epsilon_0();
		if ((!(((float)L_7) < ((float)L_8))))
		{
			goto IL_003a;
		}
	}
	{
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		return L_9;
	}

IL_003a:
	{
		Vector3_Normalize_m3984983796((&V_0), /*hidden argument*/NULL);
		Vector3_t4282066566  L_10 = V_0;
		return L_10;
	}
}
// System.Void SkyboxChanger::.ctor()
extern "C"  void SkyboxChanger__ctor_m1152136755 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkyboxChanger::Awake()
extern const MethodInfo* Component_GetComponent_TisDropdown_t4201779933_m1526532837_MethodInfo_var;
extern const uint32_t SkyboxChanger_Awake_m1389741974_MetadataUsageId;
extern "C"  void SkyboxChanger_Awake_m1389741974 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkyboxChanger_Awake_m1389741974_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dropdown_t4201779933 * L_0 = Component_GetComponent_TisDropdown_t4201779933_m1526532837(__this, /*hidden argument*/Component_GetComponent_TisDropdown_t4201779933_m1526532837_MethodInfo_var);
		__this->set__dropdown_3(L_0);
		return;
	}
}
// System.Void SkyboxChanger::ChangeSkybox()
extern Il2CppCodeGenString* _stringLiteral114861309;
extern const uint32_t SkyboxChanger_ChangeSkybox_m1815741195_MetadataUsageId;
extern "C"  void SkyboxChanger_ChangeSkybox_m1815741195 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkyboxChanger_ChangeSkybox_m1815741195_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MaterialU5BU5D_t170856778* L_0 = __this->get_Skyboxes_2();
		Dropdown_t4201779933 * L_1 = __this->get__dropdown_3();
		NullCheck(L_1);
		int32_t L_2 = Dropdown_get_value_m3628689352(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_2);
		int32_t L_3 = L_2;
		Material_t3870600107 * L_4 = (L_0)->GetAt(static_cast<il2cpp_array_size_t>(L_3));
		RenderSettings_set_skybox_m3777670233(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		Material_t3870600107 * L_5 = RenderSettings_get_skybox_m505912468(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		Material_SetFloat_m981710063(L_5, _stringLiteral114861309, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkyboxRotator::.ctor()
extern "C"  void SkyboxRotator__ctor_m3439010216 (SkyboxRotator_t551786275 * __this, const MethodInfo* method)
{
	{
		__this->set_RotationPerSecond_2((1.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SkyboxRotator::Update()
extern Il2CppCodeGenString* _stringLiteral114861309;
extern const uint32_t SkyboxRotator_Update_m961996389_MetadataUsageId;
extern "C"  void SkyboxRotator_Update_m961996389 (SkyboxRotator_t551786275 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SkyboxRotator_Update_m961996389_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = __this->get__rotate_3();
		if (!L_0)
		{
			goto IL_0026;
		}
	}
	{
		Material_t3870600107 * L_1 = RenderSettings_get_skybox_m505912468(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = __this->get_RotationPerSecond_2();
		NullCheck(L_1);
		Material_SetFloat_m981710063(L_1, _stringLiteral114861309, ((float)((float)L_2*(float)L_3)), /*hidden argument*/NULL);
	}

IL_0026:
	{
		return;
	}
}
// System.Void SkyboxRotator::ToggleSkyboxRotation()
extern "C"  void SkyboxRotator_ToggleSkyboxRotation_m1119967416 (SkyboxRotator_t551786275 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get__rotate_3();
		__this->set__rotate_3((bool)((((int32_t)L_0) == ((int32_t)0))? 1 : 0));
		return;
	}
}
// System.Void SmoothMouseLook::.ctor()
extern Il2CppClass* List_1_t1365137228_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m232134726_MethodInfo_var;
extern const uint32_t SmoothMouseLook__ctor_m3836670325_MetadataUsageId;
extern "C"  void SmoothMouseLook__ctor_m3836670325 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook__ctor_m3836670325_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_sensitivityX_3((15.0f));
		__this->set_sensitivityY_4((15.0f));
		__this->set_minimumX_5((-360.0f));
		__this->set_maximumX_6((360.0f));
		__this->set_minimumY_7((-60.0f));
		__this->set_maximumY_8((60.0f));
		List_1_t1365137228 * L_0 = (List_1_t1365137228 *)il2cpp_codegen_object_new(List_1_t1365137228_il2cpp_TypeInfo_var);
		List_1__ctor_m232134726(L_0, /*hidden argument*/List_1__ctor_m232134726_MethodInfo_var);
		__this->set_rotArrayX_11(L_0);
		List_1_t1365137228 * L_1 = (List_1_t1365137228 *)il2cpp_codegen_object_new(List_1_t1365137228_il2cpp_TypeInfo_var);
		List_1__ctor_m232134726(L_1, /*hidden argument*/List_1__ctor_m232134726_MethodInfo_var);
		__this->set_rotArrayY_13(L_1);
		__this->set_frameCounter_15((20.0f));
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SmoothMouseLook::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1_Add_m4221462838_MethodInfo_var;
extern const MethodInfo* List_1_get_Count_m917538300_MethodInfo_var;
extern const MethodInfo* List_1_RemoveAt_m2428010918_MethodInfo_var;
extern const MethodInfo* List_1_get_Item_m4194768793_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2374335;
extern Il2CppCodeGenString* _stringLiteral2907718526;
extern Il2CppCodeGenString* _stringLiteral2907718525;
extern const uint32_t SmoothMouseLook_Update_m404557880_MetadataUsageId;
extern "C"  void SmoothMouseLook_Update_m404557880 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_Update_m404557880_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Quaternion_t1553702882  V_2;
	memset(&V_2, 0, sizeof(V_2));
	Quaternion_t1553702882  V_3;
	memset(&V_3, 0, sizeof(V_3));
	int32_t V_4 = 0;
	Quaternion_t1553702882  V_5;
	memset(&V_5, 0, sizeof(V_5));
	int32_t V_6 = 0;
	Quaternion_t1553702882  V_7;
	memset(&V_7, 0, sizeof(V_7));
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_0 = Input_GetButton_m4226175975(NULL /*static, unused*/, _stringLiteral2374335, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0010;
		}
	}
	{
		return;
	}

IL_0010:
	{
		int32_t L_1 = __this->get_axes_2();
		if (L_1)
		{
			goto IL_0205;
		}
	}
	{
		__this->set_rotAverageY_14((0.0f));
		__this->set_rotAverageX_12((0.0f));
		float L_2 = __this->get_rotationY_10();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_3 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_4 = __this->get_sensitivityY_4();
		__this->set_rotationY_10(((float)((float)L_2+(float)((float)((float)L_3*(float)L_4)))));
		float L_5 = __this->get_rotationX_9();
		float L_6 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_7 = __this->get_sensitivityX_3();
		__this->set_rotationX_9(((float)((float)L_5+(float)((float)((float)L_6*(float)L_7)))));
		List_1_t1365137228 * L_8 = __this->get_rotArrayY_13();
		float L_9 = __this->get_rotationY_10();
		NullCheck(L_8);
		List_1_Add_m4221462838(L_8, L_9, /*hidden argument*/List_1_Add_m4221462838_MethodInfo_var);
		List_1_t1365137228 * L_10 = __this->get_rotArrayX_11();
		float L_11 = __this->get_rotationX_9();
		NullCheck(L_10);
		List_1_Add_m4221462838(L_10, L_11, /*hidden argument*/List_1_Add_m4221462838_MethodInfo_var);
		List_1_t1365137228 * L_12 = __this->get_rotArrayY_13();
		NullCheck(L_12);
		int32_t L_13 = List_1_get_Count_m917538300(L_12, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		float L_14 = __this->get_frameCounter_15();
		if ((!(((float)(((float)((float)L_13)))) >= ((float)L_14))))
		{
			goto IL_00b2;
		}
	}
	{
		List_1_t1365137228 * L_15 = __this->get_rotArrayY_13();
		NullCheck(L_15);
		List_1_RemoveAt_m2428010918(L_15, 0, /*hidden argument*/List_1_RemoveAt_m2428010918_MethodInfo_var);
	}

IL_00b2:
	{
		List_1_t1365137228 * L_16 = __this->get_rotArrayX_11();
		NullCheck(L_16);
		int32_t L_17 = List_1_get_Count_m917538300(L_16, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		float L_18 = __this->get_frameCounter_15();
		if ((!(((float)(((float)((float)L_17)))) >= ((float)L_18))))
		{
			goto IL_00d5;
		}
	}
	{
		List_1_t1365137228 * L_19 = __this->get_rotArrayX_11();
		NullCheck(L_19);
		List_1_RemoveAt_m2428010918(L_19, 0, /*hidden argument*/List_1_RemoveAt_m2428010918_MethodInfo_var);
	}

IL_00d5:
	{
		V_0 = 0;
		goto IL_00f9;
	}

IL_00dc:
	{
		float L_20 = __this->get_rotAverageY_14();
		List_1_t1365137228 * L_21 = __this->get_rotArrayY_13();
		int32_t L_22 = V_0;
		NullCheck(L_21);
		float L_23 = List_1_get_Item_m4194768793(L_21, L_22, /*hidden argument*/List_1_get_Item_m4194768793_MethodInfo_var);
		__this->set_rotAverageY_14(((float)((float)L_20+(float)L_23)));
		int32_t L_24 = V_0;
		V_0 = ((int32_t)((int32_t)L_24+(int32_t)1));
	}

IL_00f9:
	{
		int32_t L_25 = V_0;
		List_1_t1365137228 * L_26 = __this->get_rotArrayY_13();
		NullCheck(L_26);
		int32_t L_27 = List_1_get_Count_m917538300(L_26, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		if ((((int32_t)L_25) < ((int32_t)L_27)))
		{
			goto IL_00dc;
		}
	}
	{
		V_1 = 0;
		goto IL_012e;
	}

IL_0111:
	{
		float L_28 = __this->get_rotAverageX_12();
		List_1_t1365137228 * L_29 = __this->get_rotArrayX_11();
		int32_t L_30 = V_1;
		NullCheck(L_29);
		float L_31 = List_1_get_Item_m4194768793(L_29, L_30, /*hidden argument*/List_1_get_Item_m4194768793_MethodInfo_var);
		__this->set_rotAverageX_12(((float)((float)L_28+(float)L_31)));
		int32_t L_32 = V_1;
		V_1 = ((int32_t)((int32_t)L_32+(int32_t)1));
	}

IL_012e:
	{
		int32_t L_33 = V_1;
		List_1_t1365137228 * L_34 = __this->get_rotArrayX_11();
		NullCheck(L_34);
		int32_t L_35 = List_1_get_Count_m917538300(L_34, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		if ((((int32_t)L_33) < ((int32_t)L_35)))
		{
			goto IL_0111;
		}
	}
	{
		float L_36 = __this->get_rotAverageY_14();
		List_1_t1365137228 * L_37 = __this->get_rotArrayY_13();
		NullCheck(L_37);
		int32_t L_38 = List_1_get_Count_m917538300(L_37, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		__this->set_rotAverageY_14(((float)((float)L_36/(float)(((float)((float)L_38))))));
		float L_39 = __this->get_rotAverageX_12();
		List_1_t1365137228 * L_40 = __this->get_rotArrayX_11();
		NullCheck(L_40);
		int32_t L_41 = List_1_get_Count_m917538300(L_40, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		__this->set_rotAverageX_12(((float)((float)L_39/(float)(((float)((float)L_41))))));
		float L_42 = __this->get_rotAverageY_14();
		float L_43 = __this->get_minimumY_7();
		float L_44 = __this->get_maximumY_8();
		float L_45 = SmoothMouseLook_ClampAngle_m4181407322(NULL /*static, unused*/, L_42, L_43, L_44, /*hidden argument*/NULL);
		__this->set_rotAverageY_14(L_45);
		float L_46 = __this->get_rotAverageX_12();
		float L_47 = __this->get_minimumX_5();
		float L_48 = __this->get_maximumX_6();
		float L_49 = SmoothMouseLook_ClampAngle_m4181407322(NULL /*static, unused*/, L_46, L_47, L_48, /*hidden argument*/NULL);
		__this->set_rotAverageX_12(L_49);
		float L_50 = __this->get_rotAverageY_14();
		Vector3_t4282066566  L_51 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_52 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_50, L_51, /*hidden argument*/NULL);
		V_2 = L_52;
		float L_53 = __this->get_rotAverageX_12();
		Vector3_t4282066566  L_54 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_55 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_53, L_54, /*hidden argument*/NULL);
		V_3 = L_55;
		Transform_t1659122786 * L_56 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_57 = __this->get_originalRotation_16();
		Quaternion_t1553702882  L_58 = V_2;
		Quaternion_t1553702882  L_59 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_57, L_58, /*hidden argument*/NULL);
		NullCheck(L_56);
		Transform_set_localRotation_m3719981474(L_56, L_59, /*hidden argument*/NULL);
		Transform_t1659122786 * L_60 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_60);
		Transform_t1659122786 * L_61 = Transform_get_parent_m2236876972(L_60, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_62 = __this->get_parentOriginalRotation_17();
		Quaternion_t1553702882  L_63 = V_3;
		Quaternion_t1553702882  L_64 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_62, L_63, /*hidden argument*/NULL);
		NullCheck(L_61);
		Transform_set_localRotation_m3719981474(L_61, L_64, /*hidden argument*/NULL);
		goto IL_0409;
	}

IL_0205:
	{
		int32_t L_65 = __this->get_axes_2();
		if ((!(((uint32_t)L_65) == ((uint32_t)1))))
		{
			goto IL_0312;
		}
	}
	{
		__this->set_rotAverageX_12((0.0f));
		float L_66 = __this->get_rotationX_9();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_67 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718525, /*hidden argument*/NULL);
		float L_68 = __this->get_sensitivityX_3();
		__this->set_rotationX_9(((float)((float)L_66+(float)((float)((float)L_67*(float)L_68)))));
		List_1_t1365137228 * L_69 = __this->get_rotArrayX_11();
		float L_70 = __this->get_rotationX_9();
		NullCheck(L_69);
		List_1_Add_m4221462838(L_69, L_70, /*hidden argument*/List_1_Add_m4221462838_MethodInfo_var);
		List_1_t1365137228 * L_71 = __this->get_rotArrayX_11();
		NullCheck(L_71);
		int32_t L_72 = List_1_get_Count_m917538300(L_71, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		float L_73 = __this->get_frameCounter_15();
		if ((!(((float)(((float)((float)L_72)))) >= ((float)L_73))))
		{
			goto IL_026e;
		}
	}
	{
		List_1_t1365137228 * L_74 = __this->get_rotArrayX_11();
		NullCheck(L_74);
		List_1_RemoveAt_m2428010918(L_74, 0, /*hidden argument*/List_1_RemoveAt_m2428010918_MethodInfo_var);
	}

IL_026e:
	{
		V_4 = 0;
		goto IL_0296;
	}

IL_0276:
	{
		float L_75 = __this->get_rotAverageX_12();
		List_1_t1365137228 * L_76 = __this->get_rotArrayX_11();
		int32_t L_77 = V_4;
		NullCheck(L_76);
		float L_78 = List_1_get_Item_m4194768793(L_76, L_77, /*hidden argument*/List_1_get_Item_m4194768793_MethodInfo_var);
		__this->set_rotAverageX_12(((float)((float)L_75+(float)L_78)));
		int32_t L_79 = V_4;
		V_4 = ((int32_t)((int32_t)L_79+(int32_t)1));
	}

IL_0296:
	{
		int32_t L_80 = V_4;
		List_1_t1365137228 * L_81 = __this->get_rotArrayX_11();
		NullCheck(L_81);
		int32_t L_82 = List_1_get_Count_m917538300(L_81, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		if ((((int32_t)L_80) < ((int32_t)L_82)))
		{
			goto IL_0276;
		}
	}
	{
		float L_83 = __this->get_rotAverageX_12();
		List_1_t1365137228 * L_84 = __this->get_rotArrayX_11();
		NullCheck(L_84);
		int32_t L_85 = List_1_get_Count_m917538300(L_84, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		__this->set_rotAverageX_12(((float)((float)L_83/(float)(((float)((float)L_85))))));
		float L_86 = __this->get_rotAverageX_12();
		float L_87 = __this->get_minimumX_5();
		float L_88 = __this->get_maximumX_6();
		float L_89 = SmoothMouseLook_ClampAngle_m4181407322(NULL /*static, unused*/, L_86, L_87, L_88, /*hidden argument*/NULL);
		__this->set_rotAverageX_12(L_89);
		float L_90 = __this->get_rotAverageX_12();
		Vector3_t4282066566  L_91 = Vector3_get_up_m4046647141(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_92 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_90, L_91, /*hidden argument*/NULL);
		V_5 = L_92;
		Transform_t1659122786 * L_93 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_t1659122786 * L_94 = Transform_get_parent_m2236876972(L_93, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_95 = __this->get_parentOriginalRotation_17();
		Quaternion_t1553702882  L_96 = V_5;
		Quaternion_t1553702882  L_97 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_95, L_96, /*hidden argument*/NULL);
		NullCheck(L_94);
		Transform_set_localRotation_m3719981474(L_94, L_97, /*hidden argument*/NULL);
		goto IL_0409;
	}

IL_0312:
	{
		__this->set_rotAverageY_14((0.0f));
		float L_98 = __this->get_rotationY_10();
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_99 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2907718526, /*hidden argument*/NULL);
		float L_100 = __this->get_sensitivityY_4();
		__this->set_rotationY_10(((float)((float)L_98+(float)((float)((float)L_99*(float)L_100)))));
		List_1_t1365137228 * L_101 = __this->get_rotArrayY_13();
		float L_102 = __this->get_rotationY_10();
		NullCheck(L_101);
		List_1_Add_m4221462838(L_101, L_102, /*hidden argument*/List_1_Add_m4221462838_MethodInfo_var);
		List_1_t1365137228 * L_103 = __this->get_rotArrayY_13();
		NullCheck(L_103);
		int32_t L_104 = List_1_get_Count_m917538300(L_103, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		float L_105 = __this->get_frameCounter_15();
		if ((!(((float)(((float)((float)L_104)))) >= ((float)L_105))))
		{
			goto IL_036f;
		}
	}
	{
		List_1_t1365137228 * L_106 = __this->get_rotArrayY_13();
		NullCheck(L_106);
		List_1_RemoveAt_m2428010918(L_106, 0, /*hidden argument*/List_1_RemoveAt_m2428010918_MethodInfo_var);
	}

IL_036f:
	{
		V_6 = 0;
		goto IL_0397;
	}

IL_0377:
	{
		float L_107 = __this->get_rotAverageY_14();
		List_1_t1365137228 * L_108 = __this->get_rotArrayY_13();
		int32_t L_109 = V_6;
		NullCheck(L_108);
		float L_110 = List_1_get_Item_m4194768793(L_108, L_109, /*hidden argument*/List_1_get_Item_m4194768793_MethodInfo_var);
		__this->set_rotAverageY_14(((float)((float)L_107+(float)L_110)));
		int32_t L_111 = V_6;
		V_6 = ((int32_t)((int32_t)L_111+(int32_t)1));
	}

IL_0397:
	{
		int32_t L_112 = V_6;
		List_1_t1365137228 * L_113 = __this->get_rotArrayY_13();
		NullCheck(L_113);
		int32_t L_114 = List_1_get_Count_m917538300(L_113, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		if ((((int32_t)L_112) < ((int32_t)L_114)))
		{
			goto IL_0377;
		}
	}
	{
		float L_115 = __this->get_rotAverageY_14();
		List_1_t1365137228 * L_116 = __this->get_rotArrayY_13();
		NullCheck(L_116);
		int32_t L_117 = List_1_get_Count_m917538300(L_116, /*hidden argument*/List_1_get_Count_m917538300_MethodInfo_var);
		__this->set_rotAverageY_14(((float)((float)L_115/(float)(((float)((float)L_117))))));
		float L_118 = __this->get_rotAverageY_14();
		float L_119 = __this->get_minimumY_7();
		float L_120 = __this->get_maximumY_8();
		float L_121 = SmoothMouseLook_ClampAngle_m4181407322(NULL /*static, unused*/, L_118, L_119, L_120, /*hidden argument*/NULL);
		__this->set_rotAverageY_14(L_121);
		float L_122 = __this->get_rotAverageY_14();
		Vector3_t4282066566  L_123 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_124 = Quaternion_AngleAxis_m644124247(NULL /*static, unused*/, L_122, L_123, /*hidden argument*/NULL);
		V_7 = L_124;
		Transform_t1659122786 * L_125 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_126 = __this->get_originalRotation_16();
		Quaternion_t1553702882  L_127 = V_7;
		Quaternion_t1553702882  L_128 = Quaternion_op_Multiply_m3077481361(NULL /*static, unused*/, L_126, L_127, /*hidden argument*/NULL);
		NullCheck(L_125);
		Transform_set_localRotation_m3719981474(L_125, L_128, /*hidden argument*/NULL);
	}

IL_0409:
	{
		return;
	}
}
// System.Void SmoothMouseLook::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var;
extern const uint32_t SmoothMouseLook_Start_m2783808117_MetadataUsageId;
extern "C"  void SmoothMouseLook_Start_m2783808117 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_Start_m2783808117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Rigidbody_t3346577219 * V_0 = NULL;
	{
		Rigidbody_t3346577219 * L_0 = Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var);
		V_0 = L_0;
		Rigidbody_t3346577219 * L_1 = V_0;
		bool L_2 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0019;
		}
	}
	{
		Rigidbody_t3346577219 * L_3 = V_0;
		NullCheck(L_3);
		Rigidbody_set_freezeRotation_m3989473889(L_3, (bool)1, /*hidden argument*/NULL);
	}

IL_0019:
	{
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Quaternion_t1553702882  L_5 = Transform_get_localRotation_m3343229381(L_4, /*hidden argument*/NULL);
		__this->set_originalRotation_16(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = Transform_get_parent_m2236876972(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t1553702882  L_8 = Transform_get_localRotation_m3343229381(L_7, /*hidden argument*/NULL);
		__this->set_parentOriginalRotation_17(L_8);
		return;
	}
}
// System.Single SmoothMouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t SmoothMouseLook_ClampAngle_m4181407322_MetadataUsageId;
extern "C"  float SmoothMouseLook_ClampAngle_m4181407322 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (SmoothMouseLook_ClampAngle_m4181407322_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___angle0;
		___angle0 = (fmodf(L_0, (360.0f)));
		float L_1 = ___angle0;
		if ((!(((float)L_1) >= ((float)(-360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_2 = ___angle0;
		if ((!(((float)L_2) <= ((float)(360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_3 = ___angle0;
		if ((!(((float)L_3) < ((float)(-360.0f)))))
		{
			goto IL_0033;
		}
	}
	{
		float L_4 = ___angle0;
		___angle0 = ((float)((float)L_4+(float)(360.0f)));
	}

IL_0033:
	{
		float L_5 = ___angle0;
		if ((!(((float)L_5) > ((float)(360.0f)))))
		{
			goto IL_0047;
		}
	}
	{
		float L_6 = ___angle0;
		___angle0 = ((float)((float)L_6-(float)(360.0f)));
	}

IL_0047:
	{
		float L_7 = ___angle0;
		float L_8 = ___min1;
		float L_9 = ___max2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_10 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void SUN::.ctor()
extern "C"  void SUN__ctor_m1493783231 (SUN_t82476 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SUN::Start()
extern "C"  void SUN_Start_m440921023 (SUN_t82476 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void SUN::Update()
extern "C"  void SUN_Update_m789501998 (SUN_t82476 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector3_get_right_m4015137012(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_3 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_RotateAround_m2745906802(L_0, L_1, L_2, ((float)((float)(10.0f)*(float)L_3)), /*hidden argument*/NULL);
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_LookAt_m724138832(L_4, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testai::.ctor()
extern "C"  void testai__ctor_m1582698913 (testai_t3417797690 * __this, const MethodInfo* method)
{
	{
		__this->set_searchingTurnSpeed_2((120.0f));
		__this->set_searchingDuration_3((4.0f));
		__this->set_sightRange_4((20.0f));
		Vector3_t4282066566  L_0;
		memset(&L_0, 0, sizeof(L_0));
		Vector3__ctor_m2926210380(&L_0, (0.0f), (0.5f), (0.0f), /*hidden argument*/NULL);
		__this->set_offset_7(L_0);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void testai::Awake()
extern const MethodInfo* Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649_MethodInfo_var;
extern const uint32_t testai_Awake_m1820304132_MetadataUsageId;
extern "C"  void testai_Awake_m1820304132 (testai_t3417797690 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (testai_Awake_m1820304132_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		NavMeshAgent_t588466745 * L_0 = Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649(__this, /*hidden argument*/Component_GetComponent_TisNavMeshAgent_t588466745_m3159610649_MethodInfo_var);
		__this->set_navMeshAgent_10(L_0);
		return;
	}
}
// System.Void testai::Start()
extern "C"  void testai_Start_m529836705 (testai_t3417797690 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void testai::Update()
extern "C"  void testai_Update_m3545888140 (testai_t3417797690 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void testai::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void testai_OnTriggerEnter_m318990711 (testai_t3417797690 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::.ctor()
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t ProtectCameraFromWallClip__ctor_m2964235756_MetadataUsageId;
extern "C"  void ProtectCameraFromWallClip__ctor_m2964235756 (ProtectCameraFromWallClip_t1836063158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtectCameraFromWallClip__ctor_m2964235756_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->set_clipMoveTime_2((0.05f));
		__this->set_returnTime_3((0.4f));
		__this->set_sphereCastRadius_4((0.1f));
		__this->set_closestDistance_6((0.5f));
		__this->set_dontClipTag_7(_stringLiteral2393081601);
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.Cameras.ProtectCameraFromWallClip::get_protecting()
extern "C"  bool ProtectCameraFromWallClip_get_protecting_m673850368 (ProtectCameraFromWallClip_t1836063158 * __this, const MethodInfo* method)
{
	{
		bool L_0 = __this->get_U3CprotectingU3Ek__BackingField_16();
		return L_0;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::set_protecting(System.Boolean)
extern "C"  void ProtectCameraFromWallClip_set_protecting_m903363359 (ProtectCameraFromWallClip_t1836063158 * __this, bool ___value0, const MethodInfo* method)
{
	{
		bool L_0 = ___value0;
		__this->set_U3CprotectingU3Ek__BackingField_16(L_0);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::Start()
extern Il2CppClass* RayHitComparer_t1977523238_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t2727095145_m140979021_MethodInfo_var;
extern const uint32_t ProtectCameraFromWallClip_Start_m1911373548_MetadataUsageId;
extern "C"  void ProtectCameraFromWallClip_Start_m1911373548 (ProtectCameraFromWallClip_t1836063158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtectCameraFromWallClip_Start_m1911373548_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Camera_t2727095145 * L_0 = Component_GetComponentInChildren_TisCamera_t2727095145_m140979021(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t2727095145_m140979021_MethodInfo_var);
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		__this->set_m_Cam_8(L_1);
		Transform_t1659122786 * L_2 = __this->get_m_Cam_8();
		NullCheck(L_2);
		Transform_t1659122786 * L_3 = Transform_get_parent_m2236876972(L_2, /*hidden argument*/NULL);
		__this->set_m_Pivot_9(L_3);
		Transform_t1659122786 * L_4 = __this->get_m_Cam_8();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_localPosition_m668140784(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		float L_6 = Vector3_get_magnitude_m989985786((&V_0), /*hidden argument*/NULL);
		__this->set_m_OriginalDist_10(L_6);
		float L_7 = __this->get_m_OriginalDist_10();
		__this->set_m_CurrentDist_12(L_7);
		RayHitComparer_t1977523238 * L_8 = (RayHitComparer_t1977523238 *)il2cpp_codegen_object_new(RayHitComparer_t1977523238_il2cpp_TypeInfo_var);
		RayHitComparer__ctor_m3860494645(L_8, /*hidden argument*/NULL);
		__this->set_m_RayHitComparer_15(L_8);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip::LateUpdate()
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppClass* Mathf_t4203372500_il2cpp_TypeInfo_var;
extern const uint32_t ProtectCameraFromWallClip_LateUpdate_m658339815_MetadataUsageId;
extern "C"  void ProtectCameraFromWallClip_LateUpdate_m658339815 (ProtectCameraFromWallClip_t1836063158 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ProtectCameraFromWallClip_LateUpdate_m658339815_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	ColliderU5BU5D_t2697150633* V_1 = NULL;
	bool V_2 = false;
	bool V_3 = false;
	int32_t V_4 = 0;
	float V_5 = 0.0f;
	int32_t V_6 = 0;
	Vector3_t4282066566  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float* G_B22_0 = NULL;
	float G_B22_1 = 0.0f;
	float G_B22_2 = 0.0f;
	ProtectCameraFromWallClip_t1836063158 * G_B22_3 = NULL;
	float* G_B21_0 = NULL;
	float G_B21_1 = 0.0f;
	float G_B21_2 = 0.0f;
	ProtectCameraFromWallClip_t1836063158 * G_B21_3 = NULL;
	float G_B23_0 = 0.0f;
	float* G_B23_1 = NULL;
	float G_B23_2 = 0.0f;
	float G_B23_3 = 0.0f;
	ProtectCameraFromWallClip_t1836063158 * G_B23_4 = NULL;
	{
		float L_0 = __this->get_m_OriginalDist_10();
		V_0 = L_0;
		Ray_t3134616544 * L_1 = __this->get_address_of_m_Ray_13();
		Transform_t1659122786 * L_2 = __this->get_m_Pivot_9();
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_4 = __this->get_m_Pivot_9();
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_forward_m877665793(L_4, /*hidden argument*/NULL);
		float L_6 = __this->get_sphereCastRadius_4();
		Vector3_t4282066566  L_7 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		Vector3_t4282066566  L_8 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_3, L_7, /*hidden argument*/NULL);
		Ray_set_origin_m1191170081(L_1, L_8, /*hidden argument*/NULL);
		Ray_t3134616544 * L_9 = __this->get_address_of_m_Ray_13();
		Transform_t1659122786 * L_10 = __this->get_m_Pivot_9();
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_forward_m877665793(L_10, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Ray_set_direction_m2672750186(L_9, L_12, /*hidden argument*/NULL);
		Ray_t3134616544 * L_13 = __this->get_address_of_m_Ray_13();
		Vector3_t4282066566  L_14 = Ray_get_origin_m3064983562(L_13, /*hidden argument*/NULL);
		float L_15 = __this->get_sphereCastRadius_4();
		ColliderU5BU5D_t2697150633* L_16 = Physics_OverlapSphere_m359079608(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_1 = L_16;
		V_2 = (bool)0;
		V_3 = (bool)0;
		V_4 = 0;
		goto IL_00be;
	}

IL_0076:
	{
		ColliderU5BU5D_t2697150633* L_17 = V_1;
		int32_t L_18 = V_4;
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
		int32_t L_19 = L_18;
		Collider_t2939674232 * L_20 = (L_17)->GetAt(static_cast<il2cpp_array_size_t>(L_19));
		NullCheck(L_20);
		bool L_21 = Collider_get_isTrigger_m3877528150(L_20, /*hidden argument*/NULL);
		if (L_21)
		{
			goto IL_00b8;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_22 = V_1;
		int32_t L_23 = V_4;
		NullCheck(L_22);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_22, L_23);
		int32_t L_24 = L_23;
		Collider_t2939674232 * L_25 = (L_22)->GetAt(static_cast<il2cpp_array_size_t>(L_24));
		NullCheck(L_25);
		Rigidbody_t3346577219 * L_26 = Collider_get_attachedRigidbody_m2821754842(L_25, /*hidden argument*/NULL);
		bool L_27 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_26, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_27)
		{
			goto IL_00b1;
		}
	}
	{
		ColliderU5BU5D_t2697150633* L_28 = V_1;
		int32_t L_29 = V_4;
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, L_29);
		int32_t L_30 = L_29;
		Collider_t2939674232 * L_31 = (L_28)->GetAt(static_cast<il2cpp_array_size_t>(L_30));
		NullCheck(L_31);
		Rigidbody_t3346577219 * L_32 = Collider_get_attachedRigidbody_m2821754842(L_31, /*hidden argument*/NULL);
		String_t* L_33 = __this->get_dontClipTag_7();
		NullCheck(L_32);
		bool L_34 = Component_CompareTag_m305486283(L_32, L_33, /*hidden argument*/NULL);
		if (L_34)
		{
			goto IL_00b8;
		}
	}

IL_00b1:
	{
		V_2 = (bool)1;
		goto IL_00c8;
	}

IL_00b8:
	{
		int32_t L_35 = V_4;
		V_4 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_00be:
	{
		int32_t L_36 = V_4;
		ColliderU5BU5D_t2697150633* L_37 = V_1;
		NullCheck(L_37);
		if ((((int32_t)L_36) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_37)->max_length)))))))
		{
			goto IL_0076;
		}
	}

IL_00c8:
	{
		bool L_38 = V_2;
		if (!L_38)
		{
			goto IL_011d;
		}
	}
	{
		Ray_t3134616544 * L_39 = __this->get_address_of_m_Ray_13();
		Ray_t3134616544 * L_40 = L_39;
		Vector3_t4282066566  L_41 = Ray_get_origin_m3064983562(L_40, /*hidden argument*/NULL);
		Transform_t1659122786 * L_42 = __this->get_m_Pivot_9();
		NullCheck(L_42);
		Vector3_t4282066566  L_43 = Transform_get_forward_m877665793(L_42, /*hidden argument*/NULL);
		float L_44 = __this->get_sphereCastRadius_4();
		Vector3_t4282066566  L_45 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_43, L_44, /*hidden argument*/NULL);
		Vector3_t4282066566  L_46 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_41, L_45, /*hidden argument*/NULL);
		Ray_set_origin_m1191170081(L_40, L_46, /*hidden argument*/NULL);
		Ray_t3134616544  L_47 = __this->get_m_Ray_13();
		float L_48 = __this->get_m_OriginalDist_10();
		float L_49 = __this->get_sphereCastRadius_4();
		RaycastHitU5BU5D_t528650843* L_50 = Physics_RaycastAll_m2128989030(NULL /*static, unused*/, L_47, ((float)((float)L_48-(float)L_49)), /*hidden argument*/NULL);
		__this->set_m_Hits_14(L_50);
		goto IL_0141;
	}

IL_011d:
	{
		Ray_t3134616544  L_51 = __this->get_m_Ray_13();
		float L_52 = __this->get_sphereCastRadius_4();
		float L_53 = __this->get_m_OriginalDist_10();
		float L_54 = __this->get_sphereCastRadius_4();
		RaycastHitU5BU5D_t528650843* L_55 = Physics_SphereCastAll_m2682495480(NULL /*static, unused*/, L_51, L_52, ((float)((float)L_53+(float)L_54)), /*hidden argument*/NULL);
		__this->set_m_Hits_14(L_55);
	}

IL_0141:
	{
		RaycastHitU5BU5D_t528650843* L_56 = __this->get_m_Hits_14();
		RayHitComparer_t1977523238 * L_57 = __this->get_m_RayHitComparer_15();
		Array_Sort_m3443575448(NULL /*static, unused*/, (Il2CppArray *)(Il2CppArray *)L_56, L_57, /*hidden argument*/NULL);
		V_5 = (std::numeric_limits<float>::infinity());
		V_6 = 0;
		goto IL_0223;
	}

IL_0161:
	{
		RaycastHitU5BU5D_t528650843* L_58 = __this->get_m_Hits_14();
		int32_t L_59 = V_6;
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, L_59);
		float L_60 = RaycastHit_get_distance_m800944203(((L_58)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_59))), /*hidden argument*/NULL);
		float L_61 = V_5;
		if ((!(((float)L_60) < ((float)L_61))))
		{
			goto IL_021d;
		}
	}
	{
		RaycastHitU5BU5D_t528650843* L_62 = __this->get_m_Hits_14();
		int32_t L_63 = V_6;
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, L_63);
		Collider_t2939674232 * L_64 = RaycastHit_get_collider_m3116882274(((L_62)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_63))), /*hidden argument*/NULL);
		NullCheck(L_64);
		bool L_65 = Collider_get_isTrigger_m3877528150(L_64, /*hidden argument*/NULL);
		if (L_65)
		{
			goto IL_021d;
		}
	}
	{
		RaycastHitU5BU5D_t528650843* L_66 = __this->get_m_Hits_14();
		int32_t L_67 = V_6;
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, L_67);
		Collider_t2939674232 * L_68 = RaycastHit_get_collider_m3116882274(((L_66)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_67))), /*hidden argument*/NULL);
		NullCheck(L_68);
		Rigidbody_t3346577219 * L_69 = Collider_get_attachedRigidbody_m2821754842(L_68, /*hidden argument*/NULL);
		bool L_70 = Object_op_Inequality_m1296218211(NULL /*static, unused*/, L_69, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_70)
		{
			goto IL_01df;
		}
	}
	{
		RaycastHitU5BU5D_t528650843* L_71 = __this->get_m_Hits_14();
		int32_t L_72 = V_6;
		NullCheck(L_71);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_71, L_72);
		Collider_t2939674232 * L_73 = RaycastHit_get_collider_m3116882274(((L_71)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_72))), /*hidden argument*/NULL);
		NullCheck(L_73);
		Rigidbody_t3346577219 * L_74 = Collider_get_attachedRigidbody_m2821754842(L_73, /*hidden argument*/NULL);
		String_t* L_75 = __this->get_dontClipTag_7();
		NullCheck(L_74);
		bool L_76 = Component_CompareTag_m305486283(L_74, L_75, /*hidden argument*/NULL);
		if (L_76)
		{
			goto IL_021d;
		}
	}

IL_01df:
	{
		RaycastHitU5BU5D_t528650843* L_77 = __this->get_m_Hits_14();
		int32_t L_78 = V_6;
		NullCheck(L_77);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_77, L_78);
		float L_79 = RaycastHit_get_distance_m800944203(((L_77)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_78))), /*hidden argument*/NULL);
		V_5 = L_79;
		Transform_t1659122786 * L_80 = __this->get_m_Pivot_9();
		RaycastHitU5BU5D_t528650843* L_81 = __this->get_m_Hits_14();
		int32_t L_82 = V_6;
		NullCheck(L_81);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_81, L_82);
		Vector3_t4282066566  L_83 = RaycastHit_get_point_m4165497838(((L_81)->GetAddressAt(static_cast<il2cpp_array_size_t>(L_82))), /*hidden argument*/NULL);
		NullCheck(L_80);
		Vector3_t4282066566  L_84 = Transform_InverseTransformPoint_m1626812000(L_80, L_83, /*hidden argument*/NULL);
		V_7 = L_84;
		float L_85 = (&V_7)->get_z_3();
		V_0 = ((-L_85));
		V_3 = (bool)1;
	}

IL_021d:
	{
		int32_t L_86 = V_6;
		V_6 = ((int32_t)((int32_t)L_86+(int32_t)1));
	}

IL_0223:
	{
		int32_t L_87 = V_6;
		RaycastHitU5BU5D_t528650843* L_88 = __this->get_m_Hits_14();
		NullCheck(L_88);
		if ((((int32_t)L_87) < ((int32_t)(((int32_t)((int32_t)(((Il2CppArray *)L_88)->max_length)))))))
		{
			goto IL_0161;
		}
	}
	{
		bool L_89 = V_3;
		if (!L_89)
		{
			goto IL_026a;
		}
	}
	{
		Ray_t3134616544 * L_90 = __this->get_address_of_m_Ray_13();
		Vector3_t4282066566  L_91 = Ray_get_origin_m3064983562(L_90, /*hidden argument*/NULL);
		Transform_t1659122786 * L_92 = __this->get_m_Pivot_9();
		NullCheck(L_92);
		Vector3_t4282066566  L_93 = Transform_get_forward_m877665793(L_92, /*hidden argument*/NULL);
		Vector3_t4282066566  L_94 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_93, /*hidden argument*/NULL);
		float L_95 = V_0;
		float L_96 = __this->get_sphereCastRadius_4();
		Vector3_t4282066566  L_97 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_94, ((float)((float)L_95+(float)L_96)), /*hidden argument*/NULL);
		Color_t4194546905  L_98 = Color_get_red_m4288945411(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_DrawRay_m123146114(NULL /*static, unused*/, L_91, L_97, L_98, /*hidden argument*/NULL);
	}

IL_026a:
	{
		bool L_99 = V_3;
		ProtectCameraFromWallClip_set_protecting_m903363359(__this, L_99, /*hidden argument*/NULL);
		float L_100 = __this->get_m_CurrentDist_12();
		float L_101 = V_0;
		float* L_102 = __this->get_address_of_m_MoveVelocity_11();
		float L_103 = __this->get_m_CurrentDist_12();
		float L_104 = V_0;
		G_B21_0 = L_102;
		G_B21_1 = L_101;
		G_B21_2 = L_100;
		G_B21_3 = __this;
		if ((!(((float)L_103) > ((float)L_104))))
		{
			G_B22_0 = L_102;
			G_B22_1 = L_101;
			G_B22_2 = L_100;
			G_B22_3 = __this;
			goto IL_0296;
		}
	}
	{
		float L_105 = __this->get_clipMoveTime_2();
		G_B23_0 = L_105;
		G_B23_1 = G_B21_0;
		G_B23_2 = G_B21_1;
		G_B23_3 = G_B21_2;
		G_B23_4 = G_B21_3;
		goto IL_029c;
	}

IL_0296:
	{
		float L_106 = __this->get_returnTime_3();
		G_B23_0 = L_106;
		G_B23_1 = G_B22_0;
		G_B23_2 = G_B22_1;
		G_B23_3 = G_B22_2;
		G_B23_4 = G_B22_3;
	}

IL_029c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t4203372500_il2cpp_TypeInfo_var);
		float L_107 = Mathf_SmoothDamp_m488292903(NULL /*static, unused*/, G_B23_3, G_B23_2, G_B23_1, G_B23_0, /*hidden argument*/NULL);
		NullCheck(G_B23_4);
		G_B23_4->set_m_CurrentDist_12(L_107);
		float L_108 = __this->get_m_CurrentDist_12();
		float L_109 = __this->get_closestDistance_6();
		float L_110 = __this->get_m_OriginalDist_10();
		float L_111 = Mathf_Clamp_m3872743893(NULL /*static, unused*/, L_108, L_109, L_110, /*hidden argument*/NULL);
		__this->set_m_CurrentDist_12(L_111);
		Transform_t1659122786 * L_112 = __this->get_m_Cam_8();
		Vector3_t4282066566  L_113 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_114 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_113, /*hidden argument*/NULL);
		float L_115 = __this->get_m_CurrentDist_12();
		Vector3_t4282066566  L_116 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_114, L_115, /*hidden argument*/NULL);
		NullCheck(L_112);
		Transform_set_localPosition_m3504330903(L_112, L_116, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::.ctor()
extern "C"  void RayHitComparer__ctor_m3860494645 (RayHitComparer_t1977523238 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m1772956182(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityStandardAssets.Cameras.ProtectCameraFromWallClip/RayHitComparer::Compare(System.Object,System.Object)
extern Il2CppClass* RaycastHit_t4003175726_il2cpp_TypeInfo_var;
extern const uint32_t RayHitComparer_Compare_m1625515384_MetadataUsageId;
extern "C"  int32_t RayHitComparer_Compare_m1625515384 (RayHitComparer_t1977523238 * __this, Il2CppObject * ___x0, Il2CppObject * ___y1, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RayHitComparer_Compare_m1625515384_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	RaycastHit_t4003175726  V_2;
	memset(&V_2, 0, sizeof(V_2));
	{
		Il2CppObject * L_0 = ___x0;
		V_0 = ((*(RaycastHit_t4003175726 *)((RaycastHit_t4003175726 *)UnBox (L_0, RaycastHit_t4003175726_il2cpp_TypeInfo_var))));
		float L_1 = RaycastHit_get_distance_m800944203((&V_0), /*hidden argument*/NULL);
		V_1 = L_1;
		Il2CppObject * L_2 = ___y1;
		V_2 = ((*(RaycastHit_t4003175726 *)((RaycastHit_t4003175726 *)UnBox (L_2, RaycastHit_t4003175726_il2cpp_TypeInfo_var))));
		float L_3 = RaycastHit_get_distance_m800944203((&V_2), /*hidden argument*/NULL);
		int32_t L_4 = Single_CompareTo_m3518345828((&V_1), L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void WaterFlowFREEDemo::.ctor()
extern "C"  void WaterFlowFREEDemo__ctor_m2160876471 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::Start()
extern Il2CppClass* UnityAction_1_t4186098975_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern const MethodInfo* WaterFlowFREEDemo_U3CStartU3Em__0_m2914074683_MethodInfo_var;
extern const MethodInfo* UnityAction_1__ctor_m480548041_MethodInfo_var;
extern const MethodInfo* UnityEvent_1_AddListener_m994670587_MethodInfo_var;
extern const MethodInfo* WaterFlowFREEDemo_U3CStartU3Em__1_m2403540506_MethodInfo_var;
extern const MethodInfo* WaterFlowFREEDemo_U3CStartU3Em__2_m1893006329_MethodInfo_var;
extern const MethodInfo* WaterFlowFREEDemo_U3CStartU3Em__3_m1382472152_MethodInfo_var;
extern const MethodInfo* WaterFlowFREEDemo_U3CStartU3Em__4_m871937975_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern Il2CppCodeGenString* _stringLiteral1497203166;
extern Il2CppCodeGenString* _stringLiteral1497203167;
extern const uint32_t WaterFlowFREEDemo_Start_m1108014263_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_Start_m1108014263 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_Start_m1108014263_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = __this->get_m_SimpleWater_8();
		NullCheck(L_0);
		Renderer_t3076687687 * L_1 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_0, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_1);
		Material_t3870600107 * L_2 = Renderer_get_material_m2720864603(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Color_t4194546905  L_3 = Material_GetColor_m1709543664(L_2, _stringLiteral2785059396, /*hidden argument*/NULL);
		__this->set_m_WaterSimpleOriginal_Color_5(L_3);
		GameObject_t3674682005 * L_4 = __this->get_m_SimpleWater_8();
		NullCheck(L_4);
		Renderer_t3076687687 * L_5 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_4, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_5);
		Material_t3870600107 * L_6 = Renderer_get_material_m2720864603(L_5, /*hidden argument*/NULL);
		NullCheck(L_6);
		float L_7 = Material_GetFloat_m2541456626(L_6, _stringLiteral1497203166, /*hidden argument*/NULL);
		__this->set_m_WaterSimpleOriginal_UMoveSpeed_6(L_7);
		GameObject_t3674682005 * L_8 = __this->get_m_SimpleWater_8();
		NullCheck(L_8);
		Renderer_t3076687687 * L_9 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_8, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_9);
		Material_t3870600107 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		float L_11 = Material_GetFloat_m2541456626(L_10, _stringLiteral1497203167, /*hidden argument*/NULL);
		__this->set_m_WaterSimpleOriginal_VMoveSpeed_7(L_11);
		Color_t4194546905  L_12 = __this->get_m_WaterSimpleOriginal_Color_5();
		__this->set_m_WaterSimple_Color_2(L_12);
		float L_13 = __this->get_m_WaterSimpleOriginal_UMoveSpeed_6();
		__this->set_m_WaterSimple_UMoveSpeed_3(L_13);
		float L_14 = __this->get_m_WaterSimpleOriginal_VMoveSpeed_7();
		__this->set_m_WaterSimple_VMoveSpeed_4(L_14);
		Slider_t79469677 * L_15 = __this->get_m_SimpleR_10();
		NullCheck(L_15);
		Slider_set_minValue_m3023646485(L_15, (0.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_16 = __this->get_m_SimpleR_10();
		NullCheck(L_16);
		Slider_set_maxValue_m4261736743(L_16, (1.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_17 = __this->get_m_SimpleR_10();
		Color_t4194546905 * L_18 = __this->get_address_of_m_WaterSimpleOriginal_Color_5();
		float L_19 = L_18->get_r_0();
		NullCheck(L_17);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_17, L_19);
		Slider_t79469677 * L_20 = __this->get_m_SimpleR_10();
		NullCheck(L_20);
		SliderEvent_t2627072750 * L_21 = Slider_get_onValueChanged_m3585429728(L_20, /*hidden argument*/NULL);
		IntPtr_t L_22;
		L_22.set_m_value_0((void*)(void*)WaterFlowFREEDemo_U3CStartU3Em__0_m2914074683_MethodInfo_var);
		UnityAction_1_t4186098975 * L_23 = (UnityAction_1_t4186098975 *)il2cpp_codegen_object_new(UnityAction_1_t4186098975_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m480548041(L_23, __this, L_22, /*hidden argument*/UnityAction_1__ctor_m480548041_MethodInfo_var);
		NullCheck(L_21);
		UnityEvent_1_AddListener_m994670587(L_21, L_23, /*hidden argument*/UnityEvent_1_AddListener_m994670587_MethodInfo_var);
		Slider_t79469677 * L_24 = __this->get_m_SimpleG_11();
		NullCheck(L_24);
		Slider_set_minValue_m3023646485(L_24, (0.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_25 = __this->get_m_SimpleG_11();
		NullCheck(L_25);
		Slider_set_maxValue_m4261736743(L_25, (1.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_26 = __this->get_m_SimpleG_11();
		Color_t4194546905 * L_27 = __this->get_address_of_m_WaterSimpleOriginal_Color_5();
		float L_28 = L_27->get_g_1();
		NullCheck(L_26);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_26, L_28);
		Slider_t79469677 * L_29 = __this->get_m_SimpleG_11();
		NullCheck(L_29);
		SliderEvent_t2627072750 * L_30 = Slider_get_onValueChanged_m3585429728(L_29, /*hidden argument*/NULL);
		IntPtr_t L_31;
		L_31.set_m_value_0((void*)(void*)WaterFlowFREEDemo_U3CStartU3Em__1_m2403540506_MethodInfo_var);
		UnityAction_1_t4186098975 * L_32 = (UnityAction_1_t4186098975 *)il2cpp_codegen_object_new(UnityAction_1_t4186098975_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m480548041(L_32, __this, L_31, /*hidden argument*/UnityAction_1__ctor_m480548041_MethodInfo_var);
		NullCheck(L_30);
		UnityEvent_1_AddListener_m994670587(L_30, L_32, /*hidden argument*/UnityEvent_1_AddListener_m994670587_MethodInfo_var);
		Slider_t79469677 * L_33 = __this->get_m_SimpleB_12();
		NullCheck(L_33);
		Slider_set_minValue_m3023646485(L_33, (0.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_34 = __this->get_m_SimpleB_12();
		NullCheck(L_34);
		Slider_set_maxValue_m4261736743(L_34, (1.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_35 = __this->get_m_SimpleB_12();
		Color_t4194546905 * L_36 = __this->get_address_of_m_WaterSimpleOriginal_Color_5();
		float L_37 = L_36->get_b_2();
		NullCheck(L_35);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_35, L_37);
		Slider_t79469677 * L_38 = __this->get_m_SimpleB_12();
		NullCheck(L_38);
		SliderEvent_t2627072750 * L_39 = Slider_get_onValueChanged_m3585429728(L_38, /*hidden argument*/NULL);
		IntPtr_t L_40;
		L_40.set_m_value_0((void*)(void*)WaterFlowFREEDemo_U3CStartU3Em__2_m1893006329_MethodInfo_var);
		UnityAction_1_t4186098975 * L_41 = (UnityAction_1_t4186098975 *)il2cpp_codegen_object_new(UnityAction_1_t4186098975_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m480548041(L_41, __this, L_40, /*hidden argument*/UnityAction_1__ctor_m480548041_MethodInfo_var);
		NullCheck(L_39);
		UnityEvent_1_AddListener_m994670587(L_39, L_41, /*hidden argument*/UnityEvent_1_AddListener_m994670587_MethodInfo_var);
		Slider_t79469677 * L_42 = __this->get_m_SimpleUSpeed_13();
		NullCheck(L_42);
		Slider_set_minValue_m3023646485(L_42, (-6.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_43 = __this->get_m_SimpleUSpeed_13();
		NullCheck(L_43);
		Slider_set_maxValue_m4261736743(L_43, (6.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_44 = __this->get_m_SimpleUSpeed_13();
		float L_45 = __this->get_m_WaterSimpleOriginal_UMoveSpeed_6();
		NullCheck(L_44);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_44, L_45);
		Slider_t79469677 * L_46 = __this->get_m_SimpleUSpeed_13();
		NullCheck(L_46);
		SliderEvent_t2627072750 * L_47 = Slider_get_onValueChanged_m3585429728(L_46, /*hidden argument*/NULL);
		IntPtr_t L_48;
		L_48.set_m_value_0((void*)(void*)WaterFlowFREEDemo_U3CStartU3Em__3_m1382472152_MethodInfo_var);
		UnityAction_1_t4186098975 * L_49 = (UnityAction_1_t4186098975 *)il2cpp_codegen_object_new(UnityAction_1_t4186098975_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m480548041(L_49, __this, L_48, /*hidden argument*/UnityAction_1__ctor_m480548041_MethodInfo_var);
		NullCheck(L_47);
		UnityEvent_1_AddListener_m994670587(L_47, L_49, /*hidden argument*/UnityEvent_1_AddListener_m994670587_MethodInfo_var);
		Slider_t79469677 * L_50 = __this->get_m_SimpleVSpeed_14();
		NullCheck(L_50);
		Slider_set_minValue_m3023646485(L_50, (-6.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_51 = __this->get_m_SimpleVSpeed_14();
		NullCheck(L_51);
		Slider_set_maxValue_m4261736743(L_51, (6.0f), /*hidden argument*/NULL);
		Slider_t79469677 * L_52 = __this->get_m_SimpleVSpeed_14();
		float L_53 = __this->get_m_WaterSimpleOriginal_VMoveSpeed_7();
		NullCheck(L_52);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_52, L_53);
		Slider_t79469677 * L_54 = __this->get_m_SimpleVSpeed_14();
		NullCheck(L_54);
		SliderEvent_t2627072750 * L_55 = Slider_get_onValueChanged_m3585429728(L_54, /*hidden argument*/NULL);
		IntPtr_t L_56;
		L_56.set_m_value_0((void*)(void*)WaterFlowFREEDemo_U3CStartU3Em__4_m871937975_MethodInfo_var);
		UnityAction_1_t4186098975 * L_57 = (UnityAction_1_t4186098975 *)il2cpp_codegen_object_new(UnityAction_1_t4186098975_il2cpp_TypeInfo_var);
		UnityAction_1__ctor_m480548041(L_57, __this, L_56, /*hidden argument*/UnityAction_1__ctor_m480548041_MethodInfo_var);
		NullCheck(L_55);
		UnityEvent_1_AddListener_m994670587(L_55, L_57, /*hidden argument*/UnityEvent_1_AddListener_m994670587_MethodInfo_var);
		return;
	}
}
// System.Void WaterFlowFREEDemo::Update()
extern "C"  void WaterFlowFREEDemo_Update_m4289523254 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnSlider_SimpleR(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t WaterFlowFREEDemo_OnSlider_SimpleR_m2682694877_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleR_m2682694877 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnSlider_SimpleR_m2682694877_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		Color_t4194546905 * L_1 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_2 = L_1->get_g_1();
		Color_t4194546905 * L_3 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_4 = L_3->get_b_2();
		Color_t4194546905 * L_5 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_6 = L_5->get_a_3();
		Color_t4194546905  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2252924356(&L_7, L_0, L_2, L_4, L_6, /*hidden argument*/NULL);
		__this->set_m_WaterSimple_Color_2(L_7);
		GameObject_t3674682005 * L_8 = __this->get_m_SimpleWater_8();
		NullCheck(L_8);
		Renderer_t3076687687 * L_9 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_8, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_9);
		Material_t3870600107 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		Color_t4194546905  L_11 = __this->get_m_WaterSimple_Color_2();
		NullCheck(L_10);
		Material_SetColor_m1918430019(L_10, _stringLiteral2785059396, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnSlider_SimpleG(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t WaterFlowFREEDemo_OnSlider_SimpleG_m4003603528_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleG_m4003603528 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnSlider_SimpleG_m4003603528_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905 * L_0 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_1 = L_0->get_r_0();
		float L_2 = ___value0;
		Color_t4194546905 * L_3 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_4 = L_3->get_b_2();
		Color_t4194546905 * L_5 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_6 = L_5->get_a_3();
		Color_t4194546905  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2252924356(&L_7, L_1, L_2, L_4, L_6, /*hidden argument*/NULL);
		__this->set_m_WaterSimple_Color_2(L_7);
		GameObject_t3674682005 * L_8 = __this->get_m_SimpleWater_8();
		NullCheck(L_8);
		Renderer_t3076687687 * L_9 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_8, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_9);
		Material_t3870600107 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		Color_t4194546905  L_11 = __this->get_m_WaterSimple_Color_2();
		NullCheck(L_10);
		Material_SetColor_m1918430019(L_10, _stringLiteral2785059396, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnSlider_SimpleB(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2785059396;
extern const uint32_t WaterFlowFREEDemo_OnSlider_SimpleB_m2261307117_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleB_m2261307117 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnSlider_SimpleB_m2261307117_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Color_t4194546905 * L_0 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_1 = L_0->get_r_0();
		Color_t4194546905 * L_2 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_3 = L_2->get_g_1();
		float L_4 = ___value0;
		Color_t4194546905 * L_5 = __this->get_address_of_m_WaterSimple_Color_2();
		float L_6 = L_5->get_a_3();
		Color_t4194546905  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Color__ctor_m2252924356(&L_7, L_1, L_3, L_4, L_6, /*hidden argument*/NULL);
		__this->set_m_WaterSimple_Color_2(L_7);
		GameObject_t3674682005 * L_8 = __this->get_m_SimpleWater_8();
		NullCheck(L_8);
		Renderer_t3076687687 * L_9 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_8, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_9);
		Material_t3870600107 * L_10 = Renderer_get_material_m2720864603(L_9, /*hidden argument*/NULL);
		Color_t4194546905  L_11 = __this->get_m_WaterSimple_Color_2();
		NullCheck(L_10);
		Material_SetColor_m1918430019(L_10, _stringLiteral2785059396, L_11, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnSlider_SimpleUSpeed(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1497203166;
extern const uint32_t WaterFlowFREEDemo_OnSlider_SimpleUSpeed_m794485011_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleUSpeed_m794485011 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnSlider_SimpleUSpeed_m794485011_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_m_WaterSimple_UMoveSpeed_3(L_0);
		GameObject_t3674682005 * L_1 = __this->get_m_SimpleWater_8();
		NullCheck(L_1);
		Renderer_t3076687687 * L_2 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_2);
		Material_t3870600107 * L_3 = Renderer_get_material_m2720864603(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_m_WaterSimple_UMoveSpeed_3();
		NullCheck(L_3);
		Material_SetFloat_m981710063(L_3, _stringLiteral1497203166, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnSlider_SimpleVSpeed(System.Single)
extern const MethodInfo* GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral1497203167;
extern const uint32_t WaterFlowFREEDemo_OnSlider_SimpleVSpeed_m1005835924_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleVSpeed_m1005835924 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnSlider_SimpleVSpeed_m1005835924_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___value0;
		__this->set_m_WaterSimple_VMoveSpeed_4(L_0);
		GameObject_t3674682005 * L_1 = __this->get_m_SimpleWater_8();
		NullCheck(L_1);
		Renderer_t3076687687 * L_2 = GameObject_GetComponent_TisRenderer_t3076687687_m4102086307(L_1, /*hidden argument*/GameObject_GetComponent_TisRenderer_t3076687687_m4102086307_MethodInfo_var);
		NullCheck(L_2);
		Material_t3870600107 * L_3 = Renderer_get_material_m2720864603(L_2, /*hidden argument*/NULL);
		float L_4 = __this->get_m_WaterSimple_VMoveSpeed_4();
		NullCheck(L_3);
		Material_SetFloat_m981710063(L_3, _stringLiteral1497203167, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::OnFindMoreInFullVersion()
extern Il2CppCodeGenString* _stringLiteral2524692499;
extern const uint32_t WaterFlowFREEDemo_OnFindMoreInFullVersion_m4067824716_MetadataUsageId;
extern "C"  void WaterFlowFREEDemo_OnFindMoreInFullVersion_m4067824716 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (WaterFlowFREEDemo_OnFindMoreInFullVersion_m4067824716_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_OpenURL_m1861717334(NULL /*static, unused*/, _stringLiteral2524692499, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::<Start>m__0(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__0_m2914074683 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method)
{
	{
		Slider_t79469677 * L_0 = __this->get_m_SimpleR_10();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		WaterFlowFREEDemo_OnSlider_SimpleR_m2682694877(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::<Start>m__1(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__1_m2403540506 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method)
{
	{
		Slider_t79469677 * L_0 = __this->get_m_SimpleG_11();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		WaterFlowFREEDemo_OnSlider_SimpleG_m4003603528(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::<Start>m__2(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__2_m1893006329 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method)
{
	{
		Slider_t79469677 * L_0 = __this->get_m_SimpleB_12();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		WaterFlowFREEDemo_OnSlider_SimpleB_m2261307117(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::<Start>m__3(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__3_m1382472152 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method)
{
	{
		Slider_t79469677 * L_0 = __this->get_m_SimpleUSpeed_13();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		WaterFlowFREEDemo_OnSlider_SimpleUSpeed_m794485011(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void WaterFlowFREEDemo::<Start>m__4(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__4_m871937975 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method)
{
	{
		Slider_t79469677 * L_0 = __this->get_m_SimpleVSpeed_14();
		NullCheck(L_0);
		float L_1 = VirtFuncInvoker0< float >::Invoke(45 /* System.Single UnityEngine.UI.Slider::get_value() */, L_0);
		WaterFlowFREEDemo_OnSlider_SimpleVSpeed_m1005835924(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
