﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Example
struct Example_t341682506;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Example::.ctor()
extern "C"  void Example__ctor_m1330927969 (Example_t341682506 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Example::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Example_OnTriggerEnter_m3487513015 (Example_t341682506 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
