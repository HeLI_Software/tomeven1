﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// text(1)
struct textU281U29_t2877074419;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// text(1)/$Main$52/$
struct  U24_t2609517845  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// text(1) text(1)/$Main$52/$::$self_$53
	textU281U29_t2877074419 * ___U24self_U2453_2;

public:
	inline static int32_t get_offset_of_U24self_U2453_2() { return static_cast<int32_t>(offsetof(U24_t2609517845, ___U24self_U2453_2)); }
	inline textU281U29_t2877074419 * get_U24self_U2453_2() const { return ___U24self_U2453_2; }
	inline textU281U29_t2877074419 ** get_address_of_U24self_U2453_2() { return &___U24self_U2453_2; }
	inline void set_U24self_U2453_2(textU281U29_t2877074419 * value)
	{
		___U24self_U2453_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2453_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
