﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(1)/$Main$52/$
struct U24_t2609517845;
// text(1)
struct textU281U29_t2877074419;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_textU281U292877074419.h"

// System.Void text(1)/$Main$52/$::.ctor(text(1))
extern "C"  void U24__ctor_m77688612 (U24_t2609517845 * __this, textU281U29_t2877074419 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean text(1)/$Main$52/$::MoveNext()
extern "C"  bool U24_MoveNext_m3576380523 (U24_t2609517845 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
