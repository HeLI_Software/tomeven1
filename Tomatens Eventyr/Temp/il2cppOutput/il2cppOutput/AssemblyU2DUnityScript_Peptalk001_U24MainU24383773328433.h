﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Peptalk001
struct Peptalk001_t3966958762;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Peptalk001/$Main$38
struct  U24MainU2438_t3773328433  : public GenericGenerator_1_t207610107
{
public:
	// Peptalk001 Peptalk001/$Main$38::$self_$40
	Peptalk001_t3966958762 * ___U24self_U2440_0;

public:
	inline static int32_t get_offset_of_U24self_U2440_0() { return static_cast<int32_t>(offsetof(U24MainU2438_t3773328433, ___U24self_U2440_0)); }
	inline Peptalk001_t3966958762 * get_U24self_U2440_0() const { return ___U24self_U2440_0; }
	inline Peptalk001_t3966958762 ** get_address_of_U24self_U2440_0() { return &___U24self_U2440_0; }
	inline void set_U24self_U2440_0(Peptalk001_t3966958762 * value)
	{
		___U24self_U2440_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2440_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
