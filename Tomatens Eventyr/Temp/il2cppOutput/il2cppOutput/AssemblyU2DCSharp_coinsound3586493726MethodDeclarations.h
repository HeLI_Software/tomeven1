﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// coinsound
struct coinsound_t3586493726;

#include "codegen/il2cpp-codegen.h"

// System.Void coinsound::.ctor()
extern "C"  void coinsound__ctor_m1759472141 (coinsound_t3586493726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void coinsound::Start()
extern "C"  void coinsound_Start_m706609933 (coinsound_t3586493726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void coinsound::Update()
extern "C"  void coinsound_Update_m435923616 (coinsound_t3586493726 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
