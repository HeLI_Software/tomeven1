﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Jordberprat
struct Jordberprat_t2203708557;

#include "codegen/il2cpp-codegen.h"

// System.Void Jordberprat::.ctor()
extern "C"  void Jordberprat__ctor_m237308649 (Jordberprat_t2203708557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Jordberprat::Main()
extern "C"  void Jordberprat_Main_m477338804 (Jordberprat_t2203708557 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
