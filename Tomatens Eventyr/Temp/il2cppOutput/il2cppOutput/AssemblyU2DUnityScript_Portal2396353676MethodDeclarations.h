﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Portal
struct Portal_t2396353676;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Portal::.ctor()
extern "C"  void Portal__ctor_m356886852 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Portal::OnTriggerEnter(UnityEngine.Collider)
extern "C"  Il2CppObject * Portal_OnTriggerEnter_m3753774150 (Portal_t2396353676 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Portal::Main()
extern "C"  void Portal_Main_m65554169 (Portal_t2396353676 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
