﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadNewScene
struct LoadNewScene_t2813835762;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen2762607199.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadNewScene/$DisplayScene$17
struct  U24DisplaySceneU2417_t4064840153  : public GenericGenerator_1_t2762607199
{
public:
	// LoadNewScene LoadNewScene/$DisplayScene$17::$self_$27
	LoadNewScene_t2813835762 * ___U24self_U2427_0;

public:
	inline static int32_t get_offset_of_U24self_U2427_0() { return static_cast<int32_t>(offsetof(U24DisplaySceneU2417_t4064840153, ___U24self_U2427_0)); }
	inline LoadNewScene_t2813835762 * get_U24self_U2427_0() const { return ___U24self_U2427_0; }
	inline LoadNewScene_t2813835762 ** get_address_of_U24self_U2427_0() { return &___U24self_U2427_0; }
	inline void set_U24self_U2427_0(LoadNewScene_t2813835762 * value)
	{
		___U24self_U2427_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2427_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
