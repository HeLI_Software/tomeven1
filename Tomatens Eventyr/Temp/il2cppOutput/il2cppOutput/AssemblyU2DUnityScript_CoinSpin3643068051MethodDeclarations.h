﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CoinSpin
struct CoinSpin_t3643068051;

#include "codegen/il2cpp-codegen.h"

// System.Void CoinSpin::.ctor()
extern "C"  void CoinSpin__ctor_m460253149 (CoinSpin_t3643068051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinSpin::Update()
extern "C"  void CoinSpin_Update_m3109807824 (CoinSpin_t3643068051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CoinSpin::Main()
extern "C"  void CoinSpin_Main_m2562740544 (CoinSpin_t3643068051 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
