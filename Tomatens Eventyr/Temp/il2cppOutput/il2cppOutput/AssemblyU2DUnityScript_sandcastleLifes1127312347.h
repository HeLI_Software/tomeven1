﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// sandcastleLifes
struct  sandcastleLifes_t1127312347  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 sandcastleLifes::enemyhealth
	int32_t ___enemyhealth_3;

public:
	inline static int32_t get_offset_of_enemyhealth_3() { return static_cast<int32_t>(offsetof(sandcastleLifes_t1127312347, ___enemyhealth_3)); }
	inline int32_t get_enemyhealth_3() const { return ___enemyhealth_3; }
	inline int32_t* get_address_of_enemyhealth_3() { return &___enemyhealth_3; }
	inline void set_enemyhealth_3(int32_t value)
	{
		___enemyhealth_3 = value;
	}
};

struct sandcastleLifes_t1127312347_StaticFields
{
public:
	// System.Int32 sandcastleLifes::trigger1
	int32_t ___trigger1_2;

public:
	inline static int32_t get_offset_of_trigger1_2() { return static_cast<int32_t>(offsetof(sandcastleLifes_t1127312347_StaticFields, ___trigger1_2)); }
	inline int32_t get_trigger1_2() const { return ___trigger1_2; }
	inline int32_t* get_address_of_trigger1_2() { return &___trigger1_2; }
	inline void set_trigger1_2(int32_t value)
	{
		___trigger1_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
