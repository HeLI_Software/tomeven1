﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// bartalk
struct bartalk_t3961876287;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bartalk/$Main$45/$
struct  U24_t2073436461  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// bartalk bartalk/$Main$45/$::$self_$46
	bartalk_t3961876287 * ___U24self_U2446_2;

public:
	inline static int32_t get_offset_of_U24self_U2446_2() { return static_cast<int32_t>(offsetof(U24_t2073436461, ___U24self_U2446_2)); }
	inline bartalk_t3961876287 * get_U24self_U2446_2() const { return ___U24self_U2446_2; }
	inline bartalk_t3961876287 ** get_address_of_U24self_U2446_2() { return &___U24self_U2446_2; }
	inline void set_U24self_U2446_2(bartalk_t3961876287 * value)
	{
		___U24self_U2446_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2446_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
