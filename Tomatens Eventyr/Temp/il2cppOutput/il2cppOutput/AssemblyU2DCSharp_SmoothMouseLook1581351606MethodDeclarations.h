﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SmoothMouseLook
struct SmoothMouseLook_t1581351606;

#include "codegen/il2cpp-codegen.h"

// System.Void SmoothMouseLook::.ctor()
extern "C"  void SmoothMouseLook__ctor_m3836670325 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothMouseLook::Update()
extern "C"  void SmoothMouseLook_Update_m404557880 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SmoothMouseLook::Start()
extern "C"  void SmoothMouseLook_Start_m2783808117 (SmoothMouseLook_t1581351606 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single SmoothMouseLook::ClampAngle(System.Single,System.Single,System.Single)
extern "C"  float SmoothMouseLook_ClampAngle_m4181407322 (Il2CppObject * __this /* static, unused */, float ___angle0, float ___min1, float ___max2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
