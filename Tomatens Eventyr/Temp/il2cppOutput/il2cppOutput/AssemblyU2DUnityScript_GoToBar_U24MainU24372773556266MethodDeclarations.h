﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToBar/$Main$37
struct U24MainU2437_t2773556266;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToBar/$Main$37::.ctor()
extern "C"  void U24MainU2437__ctor_m3889436454 (U24MainU2437_t2773556266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> GoToBar/$Main$37::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2437_GetEnumerator_m1256932934 (U24MainU2437_t2773556266 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
