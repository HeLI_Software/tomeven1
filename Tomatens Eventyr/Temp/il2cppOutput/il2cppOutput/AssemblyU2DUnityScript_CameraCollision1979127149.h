﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.GameObject
struct GameObject_t3674682005;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// CameraCollision
struct  CameraCollision_t1979127149  : public MonoBehaviour_t667441552
{
public:
	// System.Boolean CameraCollision::canScroll
	bool ___canScroll_2;
	// UnityEngine.Transform CameraCollision::focusPoint
	Transform_t1659122786 * ___focusPoint_3;
	// System.Single CameraCollision::detectionRadius
	float ___detectionRadius_4;
	// System.Single CameraCollision::zoomDistance
	float ___zoomDistance_5;
	// System.Int32 CameraCollision::maxZoomOut
	int32_t ___maxZoomOut_6;
	// System.Int32 CameraCollision::maxZoomIn
	int32_t ___maxZoomIn_7;
	// System.Int32 CameraCollision::zoom
	int32_t ___zoom_8;
	// UnityEngine.RaycastHit CameraCollision::hit
	RaycastHit_t4003175726  ___hit_9;
	// UnityEngine.GameObject CameraCollision::camFollow
	GameObject_t3674682005 * ___camFollow_10;
	// UnityEngine.GameObject CameraCollision::camSpot
	GameObject_t3674682005 * ___camSpot_11;

public:
	inline static int32_t get_offset_of_canScroll_2() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___canScroll_2)); }
	inline bool get_canScroll_2() const { return ___canScroll_2; }
	inline bool* get_address_of_canScroll_2() { return &___canScroll_2; }
	inline void set_canScroll_2(bool value)
	{
		___canScroll_2 = value;
	}

	inline static int32_t get_offset_of_focusPoint_3() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___focusPoint_3)); }
	inline Transform_t1659122786 * get_focusPoint_3() const { return ___focusPoint_3; }
	inline Transform_t1659122786 ** get_address_of_focusPoint_3() { return &___focusPoint_3; }
	inline void set_focusPoint_3(Transform_t1659122786 * value)
	{
		___focusPoint_3 = value;
		Il2CppCodeGenWriteBarrier(&___focusPoint_3, value);
	}

	inline static int32_t get_offset_of_detectionRadius_4() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___detectionRadius_4)); }
	inline float get_detectionRadius_4() const { return ___detectionRadius_4; }
	inline float* get_address_of_detectionRadius_4() { return &___detectionRadius_4; }
	inline void set_detectionRadius_4(float value)
	{
		___detectionRadius_4 = value;
	}

	inline static int32_t get_offset_of_zoomDistance_5() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___zoomDistance_5)); }
	inline float get_zoomDistance_5() const { return ___zoomDistance_5; }
	inline float* get_address_of_zoomDistance_5() { return &___zoomDistance_5; }
	inline void set_zoomDistance_5(float value)
	{
		___zoomDistance_5 = value;
	}

	inline static int32_t get_offset_of_maxZoomOut_6() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___maxZoomOut_6)); }
	inline int32_t get_maxZoomOut_6() const { return ___maxZoomOut_6; }
	inline int32_t* get_address_of_maxZoomOut_6() { return &___maxZoomOut_6; }
	inline void set_maxZoomOut_6(int32_t value)
	{
		___maxZoomOut_6 = value;
	}

	inline static int32_t get_offset_of_maxZoomIn_7() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___maxZoomIn_7)); }
	inline int32_t get_maxZoomIn_7() const { return ___maxZoomIn_7; }
	inline int32_t* get_address_of_maxZoomIn_7() { return &___maxZoomIn_7; }
	inline void set_maxZoomIn_7(int32_t value)
	{
		___maxZoomIn_7 = value;
	}

	inline static int32_t get_offset_of_zoom_8() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___zoom_8)); }
	inline int32_t get_zoom_8() const { return ___zoom_8; }
	inline int32_t* get_address_of_zoom_8() { return &___zoom_8; }
	inline void set_zoom_8(int32_t value)
	{
		___zoom_8 = value;
	}

	inline static int32_t get_offset_of_hit_9() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___hit_9)); }
	inline RaycastHit_t4003175726  get_hit_9() const { return ___hit_9; }
	inline RaycastHit_t4003175726 * get_address_of_hit_9() { return &___hit_9; }
	inline void set_hit_9(RaycastHit_t4003175726  value)
	{
		___hit_9 = value;
	}

	inline static int32_t get_offset_of_camFollow_10() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___camFollow_10)); }
	inline GameObject_t3674682005 * get_camFollow_10() const { return ___camFollow_10; }
	inline GameObject_t3674682005 ** get_address_of_camFollow_10() { return &___camFollow_10; }
	inline void set_camFollow_10(GameObject_t3674682005 * value)
	{
		___camFollow_10 = value;
		Il2CppCodeGenWriteBarrier(&___camFollow_10, value);
	}

	inline static int32_t get_offset_of_camSpot_11() { return static_cast<int32_t>(offsetof(CameraCollision_t1979127149, ___camSpot_11)); }
	inline GameObject_t3674682005 * get_camSpot_11() const { return ___camSpot_11; }
	inline GameObject_t3674682005 ** get_address_of_camSpot_11() { return &___camSpot_11; }
	inline void set_camSpot_11(GameObject_t3674682005 * value)
	{
		___camSpot_11 = value;
		Il2CppCodeGenWriteBarrier(&___camSpot_11, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
