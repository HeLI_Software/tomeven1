﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t2939674232;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Portal/$OnTriggerEnter$48/$
struct  U24_t2841025597  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// System.Int32 Portal/$OnTriggerEnter$48/$::$morecash$49
	int32_t ___U24morecashU2449_2;
	// UnityEngine.Collider Portal/$OnTriggerEnter$48/$::$hit$50
	Collider_t2939674232 * ___U24hitU2450_3;

public:
	inline static int32_t get_offset_of_U24morecashU2449_2() { return static_cast<int32_t>(offsetof(U24_t2841025597, ___U24morecashU2449_2)); }
	inline int32_t get_U24morecashU2449_2() const { return ___U24morecashU2449_2; }
	inline int32_t* get_address_of_U24morecashU2449_2() { return &___U24morecashU2449_2; }
	inline void set_U24morecashU2449_2(int32_t value)
	{
		___U24morecashU2449_2 = value;
	}

	inline static int32_t get_offset_of_U24hitU2450_3() { return static_cast<int32_t>(offsetof(U24_t2841025597, ___U24hitU2450_3)); }
	inline Collider_t2939674232 * get_U24hitU2450_3() const { return ___U24hitU2450_3; }
	inline Collider_t2939674232 ** get_address_of_U24hitU2450_3() { return &___U24hitU2450_3; }
	inline void set_U24hitU2450_3(Collider_t2939674232 * value)
	{
		___U24hitU2450_3 = value;
		Il2CppCodeGenWriteBarrier(&___U24hitU2450_3, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
