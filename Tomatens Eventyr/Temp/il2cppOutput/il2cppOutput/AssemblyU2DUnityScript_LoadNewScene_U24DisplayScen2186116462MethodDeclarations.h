﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadNewScene/$DisplayScene$17/$
struct U24_t2186116462;
// LoadNewScene
struct LoadNewScene_t2813835762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_LoadNewScene2813835762.h"

// System.Void LoadNewScene/$DisplayScene$17/$::.ctor(LoadNewScene)
extern "C"  void U24__ctor_m2453716342 (U24_t2186116462 * __this, LoadNewScene_t2813835762 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean LoadNewScene/$DisplayScene$17/$::MoveNext()
extern "C"  bool U24_MoveNext_m509452482 (U24_t2186116462 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
