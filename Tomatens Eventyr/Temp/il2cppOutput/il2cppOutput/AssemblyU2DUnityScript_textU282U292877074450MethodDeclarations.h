﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(2)
struct textU282U29_t2877074450;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void text(2)::.ctor()
extern "C"  void textU282U29__ctor_m930878084 (textU282U29_t2877074450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator text(2)::Main()
extern "C"  Il2CppObject * textU282U29_Main_m3439319271 (textU282U29_t2877074450 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
