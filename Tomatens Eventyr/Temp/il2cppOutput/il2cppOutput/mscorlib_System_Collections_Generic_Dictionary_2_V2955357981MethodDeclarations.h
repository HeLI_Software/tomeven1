﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_V1347443627MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define ValueCollection__ctor_m4248164483(__this, ___dictionary0, method) ((  void (*) (ValueCollection_t2955357981 *, Dictionary_2_t4254752268 *, const MethodInfo*))ValueCollection__ctor_m2824870125_gshared)(__this, ___dictionary0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Add(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m1530513231(__this, ___item0, method) ((  void (*) (ValueCollection_t2955357981 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Add_m653316517_gshared)(__this, ___item0, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Clear()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m4053644184(__this, method) ((  void (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Clear_m3589495022_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Contains(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m2854410167(__this, ___item0, method) ((  bool (*) (ValueCollection_t2955357981 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Contains_m1506710757_gshared)(__this, ___item0, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.ICollection<TValue>.Remove(TValue)
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m1282175516(__this, ___item0, method) ((  bool (*) (ValueCollection_t2955357981 *, bool, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_Remove_m883008202_gshared)(__this, ___item0, method)
// System.Collections.Generic.IEnumerator`1<TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.IEnumerable<TValue>.GetEnumerator()
#define ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4158739302(__this, method) ((  Il2CppObject* (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_Generic_IEnumerableU3CTValueU3E_GetEnumerator_m4243354478_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ValueCollection_System_Collections_ICollection_CopyTo_m3998922396(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2955357981 *, Il2CppArray *, int32_t, const MethodInfo*))ValueCollection_System_Collections_ICollection_CopyTo_m2558142578_gshared)(__this, ___array0, ___index1, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
#define ValueCollection_System_Collections_IEnumerable_GetEnumerator_m1424989975(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_IEnumerable_GetEnumerator_m2848139905_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.Generic.ICollection<TValue>.get_IsReadOnly()
#define ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m1129586026(__this, method) ((  bool (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_Generic_ICollectionU3CTValueU3E_get_IsReadOnly_m4076853912_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
#define ValueCollection_System_Collections_ICollection_get_IsSynchronized_m3663442506(__this, method) ((  bool (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_IsSynchronized_m4249306232_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
#define ValueCollection_System_Collections_ICollection_get_SyncRoot_m527179638(__this, method) ((  Il2CppObject * (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_System_Collections_ICollection_get_SyncRoot_m2240931882_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::CopyTo(TValue[],System.Int32)
#define ValueCollection_CopyTo_m1082676234(__this, ___array0, ___index1, method) ((  void (*) (ValueCollection_t2955357981 *, BooleanU5BU5D_t3456302923*, int32_t, const MethodInfo*))ValueCollection_CopyTo_m2147895796_gshared)(__this, ___array0, ___index1, method)
// System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::GetEnumerator()
#define ValueCollection_GetEnumerator_m4104610989(__this, method) ((  Enumerator_t2186585676  (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_GetEnumerator_m387880093_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Camera,System.Boolean>::get_Count()
#define ValueCollection_get_Count_m653902864(__this, method) ((  int32_t (*) (ValueCollection_t2955357981 *, const MethodInfo*))ValueCollection_get_Count_m971561266_gshared)(__this, method)
