﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_22545618620MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m1595808118(__this, ___key0, ___value1, method) ((  void (*) (KeyValuePair_2_t4153532974 *, Camera_t2727095145 *, bool, const MethodInfo*))KeyValuePair_2__ctor_m2040323320_gshared)(__this, ___key0, ___value1, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m641072434(__this, method) ((  Camera_t2727095145 * (*) (KeyValuePair_2_t4153532974 *, const MethodInfo*))KeyValuePair_2_get_Key_m700889072_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m1981329139(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4153532974 *, Camera_t2727095145 *, const MethodInfo*))KeyValuePair_2_set_Key_m1751794225_gshared)(__this, ___value0, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m3086265302(__this, method) ((  bool (*) (KeyValuePair_2_t4153532974 *, const MethodInfo*))KeyValuePair_2_get_Value_m3809014448_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m1070420339(__this, ___value0, method) ((  void (*) (KeyValuePair_2_t4153532974 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m3162969521_gshared)(__this, ___value0, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Camera,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m3944862389(__this, method) ((  String_t* (*) (KeyValuePair_2_t4153532974 *, const MethodInfo*))KeyValuePair_2_ToString_m3396952209_gshared)(__this, method)
