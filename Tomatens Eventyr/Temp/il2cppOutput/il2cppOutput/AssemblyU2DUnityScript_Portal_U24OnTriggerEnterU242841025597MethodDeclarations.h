﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Portal/$OnTriggerEnter$48/$
struct U24_t2841025597;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Portal/$OnTriggerEnter$48/$::.ctor(UnityEngine.Collider)
extern "C"  void U24__ctor_m3790511726 (U24_t2841025597 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Portal/$OnTriggerEnter$48/$::MoveNext()
extern "C"  bool U24_MoveNext_m114794641 (U24_t2841025597 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
