﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// AdvancedAIV2
struct AdvancedAIV2_t2935784454;
// AISimple
struct AISimple_t158361594;
// attacing
struct attacing_t538739327;
// Attack
struct Attack_t1971575400;
// bartalk
struct bartalk_t3961876287;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;
// bartalk/$Main$45
struct U24MainU2445_t1284837720;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;
// bartalk/$Main$45/$
struct U24_t2073436461;
// UnityEngine.AudioSource
struct AudioSource_t1740077639;
// System.Object
struct Il2CppObject;
// Black_Perl
struct Black_Perl_t354423663;
// UnityEngine.Collider
struct Collider_t2939674232;
// CameraCollision
struct CameraCollision_t1979127149;
// cameraSpin
struct cameraSpin_t2282820871;
// CharacterMovement
struct CharacterMovement_t2528585432;
// UnityEngine.ControllerColliderHit
struct ControllerColliderHit_t2416790841;
// UnityEngine.Rigidbody
struct Rigidbody_t3346577219;
// coin taken
struct coinU20taken_t1434922616;
// CoinSpin
struct CoinSpin_t3643068051;
// enemy
struct enemy_t96653192;
// gobacktofirstbar
struct gobacktofirstbar_t2648077805;
// GoToBar
struct GoToBar_t1847036784;
// GoToBar/$Main$37
struct U24MainU2437_t2773556266;
// GoToBar/$Main$37/$
struct U24_t2507849599;
// GoToBar002
struct GoToBar002_t2246850754;
// GoToBar1
struct GoToBar1_t1423565505;
// health
struct health_t3073704540;
// HitVars
struct HitVars_t2591603775;
// Jaagub
struct Jaagub_t2211131146;
// Jordberprat
struct Jordberprat_t2203708557;
// LoadNewScene
struct LoadNewScene_t2813835762;
// LoadNewScene/$DisplayScene$17
struct U24DisplaySceneU2417_t4064840153;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;
// LoadNewScene/$DisplayScene$17/$
struct U24_t2186116462;
// UnityEngine.UI.Text
struct Text_t9039225;
// UnityEngine.UI.Image
struct Image_t538875265;
// LoadNewScene/$HideScene$28
struct U24HideSceneU2428_t3689129383;
// LoadNewScene/$HideScene$28/$
struct U24_t1905319356;
// map
struct map_t107868;
// Peptalk001
struct Peptalk001_t3966958762;
// Peptalk001/$Main$38
struct U24MainU2438_t3773328433;
// Peptalk001/$Main$38/$
struct U24_t1216227782;
// Peptalk002
struct Peptalk002_t3966958763;
// Peptalk002/$Main$41
struct U24MainU2441_t3576814952;
// Peptalk002/$Main$41/$
struct U24_t1345333565;
// pickup
struct pickup_t3306490492;
// Portal
struct Portal_t2396353676;
// Portal/$OnTriggerEnter$48
struct U24OnTriggerEnterU2448_t458821736;
// Portal/$OnTriggerEnter$48/$
struct U24_t2841025597;
// poschange
struct poschange_t1524020996;
// RespawnMenuV2
struct RespawnMenuV2_t3986403171;
// sandcastleLifes
struct sandcastleLifes_t1127312347;
// ScoreSystem
struct ScoreSystem_t3184889665;
// scorevalue
struct scorevalue_t1604558367;
// text
struct text_t3556653;
// text(1)
struct textU281U29_t2877074419;
// text(1)/$Main$52
struct U24MainU2452_t3645169472;
// text(1)/$Main$52/$
struct U24_t2609517845;
// text(2)
struct textU282U29_t2877074450;
// text(2)/$Main$55
struct U24MainU2455_t1848218116;
// text(2)/$Main$55/$
struct U24_t2316117721;
// text/$Main$58
struct U24MainU2458_t32180044;
// text/$Main$58/$
struct U24_t860252705;
// til hovudlevel
struct tilU20hovudlevel_t1326528079;
// title buttons
struct titleU20buttons_t1264625081;
// TitleScreenButtons
struct TitleScreenButtons_t1628429853;
// ToPoteland
struct ToPoteland_t2745890390;
// ToPoteland/$Main$44
struct U24MainU2444_t3891303456;
// ToPoteland/$Main$44/$
struct U24_t2921075189;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array1146569071.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790.h"
#include "AssemblyU2DUnityScript_U3CModuleU3E86524790MethodDeclarations.h"
#include "AssemblyU2DUnityScript_AdvancedAIV22935784454.h"
#include "AssemblyU2DUnityScript_AdvancedAIV22935784454MethodDeclarations.h"
#include "mscorlib_System_Void2863195528.h"
#include "UnityEngine_UnityEngine_MonoBehaviour667441552MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector34282066566MethodDeclarations.h"
#include "mscorlib_System_Single4291918972.h"
#include "mscorlib_System_Int321153838500.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"
#include "UnityEngine_UnityEngine_Time4241968337MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform1659122786MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component3501516275MethodDeclarations.h"
#include "AssemblyU2DUnityScript_RespawnMenuV23986403171.h"
#include "AssemblyU2DUnityScript_RespawnMenuV23986403171MethodDeclarations.h"
#include "mscorlib_System_Boolean476798718.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "mscorlib_System_Object4170816371.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion1553702882.h"
#include "UnityEngine_UnityEngine_Debug4195163081MethodDeclarations.h"
#include "mscorlib_System_String7231557.h"
#include "AssemblyU2DUnityScript_AISimple158361594.h"
#include "AssemblyU2DUnityScript_AISimple158361594MethodDeclarations.h"
#include "AssemblyU2DUnityScript_attacing538739327.h"
#include "AssemblyU2DUnityScript_attacing538739327MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Screen3187157168MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GUI3134605553MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics3358180733MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit4003175726.h"
#include "UnityEngine_UnityEngine_Rect4241904616.h"
#include "UnityEngine_UnityEngine_Rect4241904616MethodDeclarations.h"
#include "UnityEngine_UnityEngine_SendMessageOptions3856946179.h"
#include "AssemblyU2DUnityScript_Attack1971575400.h"
#include "AssemblyU2DUnityScript_Attack1971575400MethodDeclarations.h"
#include "AssemblyU2DUnityScript_bartalk3961876287.h"
#include "AssemblyU2DUnityScript_bartalk3961876287MethodDeclarations.h"
#include "AssemblyU2DUnityScript_bartalk_U24MainU24451284837720MethodDeclarations.h"
#include "AssemblyU2DUnityScript_bartalk_U24MainU24451284837720.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107MethodDeclarations.h"
#include "AssemblyU2DUnityScript_bartalk_U24MainU2445_U242073436461MethodDeclarations.h"
#include "AssemblyU2DUnityScript_bartalk_U24MainU2445_U242073436461.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"
#include "UnityEngine_UnityEngine_WaitForSeconds1615819279.h"
#include "UnityEngine_UnityEngine_AudioSource1740077639.h"
#include "UnityEngine_UnityEngine_Component3501516275.h"
#include "AssemblyU2DUnityScript_Black_Perl354423663.h"
#include "AssemblyU2DUnityScript_Black_Perl354423663MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"
#include "UnityEngine_UnityEngine_GameObject3674682005MethodDeclarations.h"
#include "mscorlib_System_String7231557MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Application2856536070MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject3674682005.h"
#include "AssemblyU2DUnityScript_sandcastleLifes1127312347.h"
#include "AssemblyU2DUnityScript_sandcastleLifes1127312347MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CameraCollision1979127149.h"
#include "AssemblyU2DUnityScript_CameraCollision1979127149MethodDeclarations.h"
#include "mscorlib_System_Type2863145774MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera2727095145MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object3071478659MethodDeclarations.h"
#include "mscorlib_System_Type2863145774.h"
#include "mscorlib_System_RuntimeTypeHandle2669177232.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"
#include "UnityEngine_UnityEngine_Object3071478659.h"
#include "UnityEngine_UnityEngine_Input4200062272MethodDeclarations.h"
#include "AssemblyU2DUnityScript_cameraSpin2282820871.h"
#include "AssemblyU2DUnityScript_cameraSpin2282820871MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Space4209342076.h"
#include "AssemblyU2DUnityScript_CharacterMovement2528585432.h"
#include "AssemblyU2DUnityScript_CharacterMovement2528585432MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635.h"
#include "UnityEngine_UnityEngine_CharacterController1618060635MethodDeclarations.h"
#include "UnityEngine_UnityEngine_CollisionFlags490137529.h"
#include "UnityEngine_UnityEngine_KeyCode3128317986.h"
#include "AssemblyU2DUnityScript_LoadNewScene2813835762.h"
#include "AssemblyU2DUnityScript_LoadNewScene2813835762MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine3621161934.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841.h"
#include "UnityEngine_UnityEngine_ControllerColliderHit2416790841MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody3346577219.h"
#include "AssemblyU2DUnityScript_coinU20taken1434922616.h"
#include "AssemblyU2DUnityScript_coinU20taken1434922616MethodDeclarations.h"
#include "AssemblyU2DUnityScript_CoinSpin3643068051.h"
#include "AssemblyU2DUnityScript_CoinSpin3643068051MethodDeclarations.h"
#include "AssemblyU2DUnityScript_enemy96653192.h"
#include "AssemblyU2DUnityScript_enemy96653192MethodDeclarations.h"
#include "AssemblyU2DUnityScript_gobacktofirstbar2648077805.h"
#include "AssemblyU2DUnityScript_gobacktofirstbar2648077805MethodDeclarations.h"
#include "AssemblyU2DUnityScript_GoToBar1847036784.h"
#include "AssemblyU2DUnityScript_GoToBar1847036784MethodDeclarations.h"
#include "AssemblyU2DUnityScript_GoToBar_U24MainU24372773556266MethodDeclarations.h"
#include "AssemblyU2DUnityScript_GoToBar_U24MainU24372773556266.h"
#include "AssemblyU2DUnityScript_GoToBar_U24MainU2437_U242507849599MethodDeclarations.h"
#include "AssemblyU2DUnityScript_GoToBar_U24MainU2437_U242507849599.h"
#include "AssemblyU2DUnityScript_GoToBar0022246850754.h"
#include "AssemblyU2DUnityScript_GoToBar0022246850754MethodDeclarations.h"
#include "AssemblyU2DUnityScript_GoToBar11423565505.h"
#include "AssemblyU2DUnityScript_GoToBar11423565505MethodDeclarations.h"
#include "AssemblyU2DUnityScript_health3073704540.h"
#include "AssemblyU2DUnityScript_health3073704540MethodDeclarations.h"
#include "AssemblyU2DUnityScript_HitVars2591603775.h"
#include "AssemblyU2DUnityScript_HitVars2591603775MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Jaagub2211131146.h"
#include "AssemblyU2DUnityScript_Jaagub2211131146MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Jordberprat2203708557.h"
#include "AssemblyU2DUnityScript_Jordberprat2203708557MethodDeclarations.h"
#include "AssemblyU2DUnityScript_scorevalue1604558367.h"
#include "AssemblyU2DUnityScript_scorevalue1604558367MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24DisplayScen4064840153MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24DisplayScen4064840153.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24HideSceneU23689129383MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24HideSceneU23689129383.h"
#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen2762607199MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24DisplayScen2186116462MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24DisplayScen2186116462.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic836799438MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225.h"
#include "UnityEngine_UI_UnityEngine_UI_Text9039225MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image538875265.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24HideSceneU21905319356MethodDeclarations.h"
#include "AssemblyU2DUnityScript_LoadNewScene_U24HideSceneU21905319356.h"
#include "AssemblyU2DUnityScript_map107868.h"
#include "AssemblyU2DUnityScript_map107868MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk0013966958762.h"
#include "AssemblyU2DUnityScript_Peptalk0013966958762MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk001_U24MainU24383773328433MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk001_U24MainU24383773328433.h"
#include "AssemblyU2DUnityScript_Peptalk001_U24MainU2438_U241216227782MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk001_U24MainU2438_U241216227782.h"
#include "AssemblyU2DUnityScript_Peptalk0023966958763.h"
#include "AssemblyU2DUnityScript_Peptalk0023966958763MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk002_U24MainU24413576814952MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk002_U24MainU24413576814952.h"
#include "AssemblyU2DUnityScript_Peptalk002_U24MainU2441_U241345333565MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Peptalk002_U24MainU2441_U241345333565.h"
#include "UnityEngine_UnityEngine_Behaviour200106419MethodDeclarations.h"
#include "AssemblyU2DUnityScript_pickup3306490492.h"
#include "AssemblyU2DUnityScript_pickup3306490492MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Portal2396353676.h"
#include "AssemblyU2DUnityScript_Portal2396353676MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Portal_U24OnTriggerEnterU244458821736MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Portal_U24OnTriggerEnterU244458821736.h"
#include "AssemblyU2DUnityScript_Portal_U24OnTriggerEnterU242841025597MethodDeclarations.h"
#include "AssemblyU2DUnityScript_Portal_U24OnTriggerEnterU242841025597.h"
#include "AssemblyU2DUnityScript_poschange1524020996.h"
#include "AssemblyU2DUnityScript_poschange1524020996MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector24282066565.h"
#include "UnityEngine_UnityEngine_Collider2939674232MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ScoreSystem3184889665.h"
#include "AssemblyU2DUnityScript_ScoreSystem3184889665MethodDeclarations.h"
#include "Boo_Lang_Boo_Lang_Runtime_RuntimeServices3947355960MethodDeclarations.h"
#include "AssemblyU2DUnityScript_text3556653.h"
#include "AssemblyU2DUnityScript_text3556653MethodDeclarations.h"
#include "AssemblyU2DUnityScript_text_U24MainU245832180044MethodDeclarations.h"
#include "AssemblyU2DUnityScript_text_U24MainU245832180044.h"
#include "AssemblyU2DUnityScript_textU281U292877074419.h"
#include "AssemblyU2DUnityScript_textU281U292877074419MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU281U29_U24MainU24523645169472MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU281U29_U24MainU24523645169472.h"
#include "AssemblyU2DUnityScript_textU281U29_U24MainU2452_U22609517845MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU281U29_U24MainU2452_U22609517845.h"
#include "AssemblyU2DUnityScript_textU282U292877074450.h"
#include "AssemblyU2DUnityScript_textU282U292877074450MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU282U29_U24MainU24551848218116MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU282U29_U24MainU24551848218116.h"
#include "AssemblyU2DUnityScript_textU282U29_U24MainU2455_U22316117721MethodDeclarations.h"
#include "AssemblyU2DUnityScript_textU282U29_U24MainU2455_U22316117721.h"
#include "AssemblyU2DUnityScript_text_U24MainU2458_U24860252705MethodDeclarations.h"
#include "AssemblyU2DUnityScript_text_U24MainU2458_U24860252705.h"
#include "AssemblyU2DUnityScript_tilU20hovudlevel1326528079.h"
#include "AssemblyU2DUnityScript_tilU20hovudlevel1326528079MethodDeclarations.h"
#include "AssemblyU2DUnityScript_titleU20buttons1264625081.h"
#include "AssemblyU2DUnityScript_titleU20buttons1264625081MethodDeclarations.h"
#include "AssemblyU2DUnityScript_TitleScreenButtons1628429853.h"
#include "AssemblyU2DUnityScript_TitleScreenButtons1628429853MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ToPoteland2745890390.h"
#include "AssemblyU2DUnityScript_ToPoteland2745890390MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ToPoteland_U24MainU24443891303456MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ToPoteland_U24MainU24443891303456.h"
#include "AssemblyU2DUnityScript_ToPoteland_U24MainU2444_U242921075189MethodDeclarations.h"
#include "AssemblyU2DUnityScript_ToPoteland_U24MainU2444_U242921075189.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C"  Il2CppObject * Component_GetComponent_TisIl2CppObject_m267839954_gshared (Component_t3501516275 * __this, const MethodInfo* method);
#define Component_GetComponent_TisIl2CppObject_m267839954(__this, method) ((  Il2CppObject * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t1740077639_m2453081547(__this, method) ((  AudioSource_t1740077639 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t3346577219_m2174365699(__this, method) ((  Rigidbody_t3346577219 * (*) (Component_t3501516275 *, const MethodInfo*))Component_GetComponent_TisIl2CppObject_m267839954_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C"  Il2CppObject * GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared (GameObject_t3674682005 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisIl2CppObject_m2447772384(__this, method) ((  Il2CppObject * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t9039225_m2604051366(__this, method) ((  Text_t9039225 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Image>()
#define GameObject_GetComponent_TisImage_t538875265_m3560905424(__this, method) ((  Image_t538875265 * (*) (GameObject_t3674682005 *, const MethodInfo*))GameObject_GetComponent_TisIl2CppObject_m2447772384_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void AdvancedAIV2::.ctor()
extern "C"  void AdvancedAIV2__ctor_m1907856714 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_lookAtDistance_4((25.0f));
		__this->set_chaseRange_5((15.0f));
		__this->set_attackRange_6((1.5f));
		__this->set_moveSpeed_7((5.0f));
		__this->set_Damping_8((6.0f));
		__this->set_attackRepeatTime_9(1);
		__this->set_TheDammage_10(((int32_t)40));
		__this->set_gravity_13((20.0f));
		Vector3_t4282066566  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_MoveDirection_14(L_0);
		return;
	}
}
// System.Void AdvancedAIV2::Start()
extern "C"  void AdvancedAIV2_Start_m854994506 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_attackTime_11(L_0);
		return;
	}
}
// System.Void AdvancedAIV2::Update()
extern Il2CppClass* RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var;
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t AdvancedAIV2_Update_m740878083_MetadataUsageId;
extern "C"  void AdvancedAIV2_Update_m740878083 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdvancedAIV2_Update_m740878083_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var);
		bool L_0 = ((RespawnMenuV2_t3986403171_StaticFields*)RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var->static_fields)->get_playerIsDead_4();
		if (L_0)
		{
			goto IL_0042;
		}
	}
	{
		Transform_t1659122786 * L_1 = __this->get_Target_3();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t4282066566  L_4 = Transform_get_position_m2211398607(L_3, /*hidden argument*/NULL);
		float L_5 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		float L_6 = L_5;
		Il2CppObject * L_7 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_6);
		__this->set_Distance_2(L_7);
		VirtActionInvoker0::Invoke(6 /* System.Void AdvancedAIV2::lookAt() */, __this);
		VirtActionInvoker0::Invoke(8 /* System.Void AdvancedAIV2::attack() */, __this);
		VirtActionInvoker0::Invoke(7 /* System.Void AdvancedAIV2::chase() */, __this);
	}

IL_0042:
	{
		return;
	}
}
// System.Void AdvancedAIV2::lookAt()
extern "C"  void AdvancedAIV2_lookAt_m1605957388 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = __this->get_Target_3();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t1553702882  L_8 = Transform_get_rotation_m11483428(L_7, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_9 = V_0;
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = __this->get_Damping_8();
		Quaternion_t1553702882  L_12 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_8, L_9, ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_rotation_m1525803229(L_6, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AdvancedAIV2::chase()
extern "C"  void AdvancedAIV2_chase_m1826617078 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AdvancedAIV2::attack()
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1467519444;
extern Il2CppCodeGenString* _stringLiteral3486046676;
extern const uint32_t AdvancedAIV2_attack_m4187346082_MetadataUsageId;
extern "C"  void AdvancedAIV2_attack_m4187346082 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AdvancedAIV2_attack_m4187346082_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_1 = __this->get_attackTime_11();
		if ((((float)L_0) <= ((float)L_1)))
		{
			goto IL_0048;
		}
	}
	{
		Transform_t1659122786 * L_2 = __this->get_Target_3();
		int32_t L_3 = __this->get_TheDammage_10();
		int32_t L_4 = L_3;
		Il2CppObject * L_5 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		Component_SendMessage_m904598583(L_2, _stringLiteral1467519444, L_5, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3486046676, /*hidden argument*/NULL);
		float L_6 = Time_get_time_m342192902(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_7 = __this->get_attackRepeatTime_9();
		__this->set_attackTime_11(((float)((float)L_6+(float)(((float)((float)L_7))))));
	}

IL_0048:
	{
		return;
	}
}
// System.Void AdvancedAIV2::ApplyDammage()
extern "C"  void AdvancedAIV2_ApplyDammage_m2140479086 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	{
		float L_0 = __this->get_chaseRange_5();
		__this->set_chaseRange_5(((float)((float)L_0+(float)(((float)((float)((int32_t)30)))))));
		float L_1 = __this->get_moveSpeed_7();
		__this->set_moveSpeed_7(((float)((float)L_1+(float)(((float)((float)2))))));
		float L_2 = __this->get_lookAtDistance_4();
		__this->set_lookAtDistance_4(((float)((float)L_2+(float)(((float)((float)((int32_t)40)))))));
		return;
	}
}
// System.Void AdvancedAIV2::Main()
extern "C"  void AdvancedAIV2_Main_m254132787 (AdvancedAIV2_t2935784454 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void AISimple::.ctor()
extern "C"  void AISimple__ctor_m1034678870 (AISimple_t158361594 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_lookAtDistance_4((25.0f));
		__this->set_attackRange_5((15.0f));
		__this->set_moveSpeed_6((5.0f));
		__this->set_Damping_7((6.0f));
		return;
	}
}
// System.Void AISimple::Update()
extern Il2CppClass* Single_t4291918972_il2cpp_TypeInfo_var;
extern const uint32_t AISimple_Update_m3737135991_MetadataUsageId;
extern "C"  void AISimple_Update_m3737135991 (AISimple_t158361594 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (AISimple_Update_m3737135991_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = __this->get_Target_3();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		float L_4 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		float L_5 = L_4;
		Il2CppObject * L_6 = Box(Single_t4291918972_il2cpp_TypeInfo_var, &L_5);
		__this->set_Distance_2(L_6);
		VirtActionInvoker0::Invoke(5 /* System.Void AISimple::lookAt() */, __this);
		VirtActionInvoker0::Invoke(6 /* System.Void AISimple::attack() */, __this);
		return;
	}
}
// System.Void AISimple::lookAt()
extern "C"  void AISimple_lookAt_m307248000 (AISimple_t158361594 * __this, const MethodInfo* method)
{
	Quaternion_t1553702882  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Transform_t1659122786 * L_0 = __this->get_Target_3();
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_3 = Transform_get_position_m2211398607(L_2, /*hidden argument*/NULL);
		Vector3_t4282066566  L_4 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_1, L_3, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_5 = Quaternion_LookRotation_m1257501645(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Quaternion_t1553702882  L_8 = Transform_get_rotation_m11483428(L_7, /*hidden argument*/NULL);
		Quaternion_t1553702882  L_9 = V_0;
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_11 = __this->get_Damping_7();
		Quaternion_t1553702882  L_12 = Quaternion_Slerp_m844700366(NULL /*static, unused*/, L_8, L_9, ((float)((float)L_10*(float)L_11)), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_rotation_m1525803229(L_6, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AISimple::attack()
extern "C"  void AISimple_attack_m2888636694 (AISimple_t158361594 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_1 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_2 = __this->get_moveSpeed_6();
		Vector3_t4282066566  L_3 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		float L_4 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_5 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Translate_m2849099360(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AISimple::Main()
extern "C"  void AISimple_Main_m3689649063 (AISimple_t158361594 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void attacing::.ctor()
extern "C"  void attacing__ctor_m1210375537 (attacing_t538739327 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_hitpoints_2(((int32_t)10));
		__this->set_range_4((((float)((float)5))));
		return;
	}
}
// System.Void attacing::Update()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* RaycastHit_t4003175726_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2684433798;
extern Il2CppCodeGenString* _stringLiteral2895764390;
extern const uint32_t attacing_Update_m593798076_MetadataUsageId;
extern "C"  void attacing_Update_m593798076 (attacing_t538739327 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (attacing_Update_m593798076_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))-(int32_t)((int32_t)50)))))), (((float)((float)((int32_t)((int32_t)L_1/(int32_t)2))))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)60)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_3 = GUI_Button_m885093907(NULL /*static, unused*/, L_2, _stringLiteral2684433798, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_009a;
		}
	}
	{
		Initobj (RaycastHit_t4003175726_il2cpp_TypeInfo_var, (&V_0));
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t4282066566  L_5 = Transform_get_position_m2211398607(L_4, /*hidden argument*/NULL);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t4282066566  L_8 = Transform_TransformDirection_m83001769(L_6, L_7, /*hidden argument*/NULL);
		bool L_9 = Physics_Raycast_m2482317716(NULL /*static, unused*/, L_5, L_8, (&V_0), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_009a;
		}
	}
	{
		float L_10 = RaycastHit_get_distance_m800944203((&V_0), /*hidden argument*/NULL);
		__this->set_totarget_3(L_10);
		float L_11 = __this->get_totarget_3();
		float L_12 = __this->get_range_4();
		if ((((float)L_11) >= ((float)L_12)))
		{
			goto IL_009a;
		}
	}
	{
		Transform_t1659122786 * L_13 = RaycastHit_get_transform_m905149094((&V_0), /*hidden argument*/NULL);
		int32_t L_14 = __this->get_hitpoints_2();
		int32_t L_15 = L_14;
		Il2CppObject * L_16 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		Component_SendMessage_m1163914169(L_13, _stringLiteral2895764390, L_16, 1, /*hidden argument*/NULL);
	}

IL_009a:
	{
		return;
	}
}
// System.Void attacing::Main()
extern "C"  void attacing_Main_m2171296044 (attacing_t538739327 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Attack::.ctor()
extern "C"  void Attack__ctor_m3138788072 (Attack_t1971575400 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_hitpoints_2(((int32_t)10));
		__this->set_range_4((((float)((float)5))));
		return;
	}
}
// System.Void Attack::Update()
extern Il2CppClass* RaycastHit_t4003175726_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2895764390;
extern const uint32_t Attack_Update_m245044517_MetadataUsageId;
extern "C"  void Attack_Update_m245044517 (Attack_t1971575400 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Attack_Update_m245044517_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit_t4003175726  V_0;
	memset(&V_0, 0, sizeof(V_0));
	{
		Initobj (RaycastHit_t4003175726_il2cpp_TypeInfo_var, (&V_0));
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t4282066566  L_1 = Transform_get_position_m2211398607(L_0, /*hidden argument*/NULL);
		Transform_t1659122786 * L_2 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_3 = Vector3_get_forward_m1039372701(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t4282066566  L_4 = Transform_TransformDirection_m83001769(L_2, L_3, /*hidden argument*/NULL);
		bool L_5 = Physics_Raycast_m2482317716(NULL /*static, unused*/, L_1, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_006a;
		}
	}
	{
		float L_6 = RaycastHit_get_distance_m800944203((&V_0), /*hidden argument*/NULL);
		__this->set_totarget_3(L_6);
		float L_7 = __this->get_totarget_3();
		float L_8 = __this->get_range_4();
		if ((((float)L_7) >= ((float)L_8)))
		{
			goto IL_006a;
		}
	}
	{
		Transform_t1659122786 * L_9 = RaycastHit_get_transform_m905149094((&V_0), /*hidden argument*/NULL);
		int32_t L_10 = __this->get_hitpoints_2();
		int32_t L_11 = L_10;
		Il2CppObject * L_12 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		Component_SendMessage_m1163914169(L_9, _stringLiteral2895764390, L_12, 1, /*hidden argument*/NULL);
	}

IL_006a:
	{
		return;
	}
}
// System.Void Attack::Main()
extern "C"  void Attack_Main_m3341881557 (Attack_t1971575400 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void bartalk::.ctor()
extern "C"  void bartalk__ctor_m367497719 (bartalk_t3961876287 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator bartalk::Main()
extern Il2CppClass* U24MainU2445_t1284837720_il2cpp_TypeInfo_var;
extern const uint32_t bartalk_Main_m373104404_MetadataUsageId;
extern "C"  Il2CppObject * bartalk_Main_m373104404 (bartalk_t3961876287 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (bartalk_Main_m373104404_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2445_t1284837720 * L_0 = (U24MainU2445_t1284837720 *)il2cpp_codegen_object_new(U24MainU2445_t1284837720_il2cpp_TypeInfo_var);
		U24MainU2445__ctor_m3470316635(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2445_GetEnumerator_m2712048344(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void bartalk/$Main$45::.ctor(bartalk)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2445__ctor_m3470316635_MetadataUsageId;
extern "C"  void U24MainU2445__ctor_m3470316635 (U24MainU2445_t1284837720 * __this, bartalk_t3961876287 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2445__ctor_m3470316635_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		bartalk_t3961876287 * L_0 = ___self_0;
		__this->set_U24self_U2447_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> bartalk/$Main$45::GetEnumerator()
extern Il2CppClass* U24_t2073436461_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2445_GetEnumerator_m2712048344_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2445_GetEnumerator_m2712048344 (U24MainU2445_t1284837720 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2445_GetEnumerator_m2712048344_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		bartalk_t3961876287 * L_0 = __this->get_U24self_U2447_0();
		U24_t2073436461 * L_1 = (U24_t2073436461 *)il2cpp_codegen_object_new(U24_t2073436461_il2cpp_TypeInfo_var);
		U24__ctor_m2809262448(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void bartalk/$Main$45/$::.ctor(bartalk)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m2809262448_MetadataUsageId;
extern "C"  void U24__ctor_m2809262448 (U24_t2073436461 * __this, bartalk_t3961876287 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2809262448_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		bartalk_t3961876287 * L_0 = ___self_0;
		__this->set_U24self_U2446_2(L_0);
		return;
	}
}
// System.Boolean bartalk/$Main$45/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m2211308675_MetadataUsageId;
extern "C"  bool U24_MoveNext_m2211308675 (U24_t2073436461 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m2211308675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0043;
		}
		if (L_0 == 2)
		{
			goto IL_002b;
		}
	}

IL_0017:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)((int32_t)10)))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_0044;
	}

IL_002b:
	{
		bartalk_t3961876287 * L_3 = __this->get_U24self_U2446_2();
		NullCheck(L_3);
		AudioSource_t1740077639 * L_4 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_3, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_4);
		AudioSource_Play_m1360558992(L_4, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0043:
	{
		G_B4_0 = 0;
	}

IL_0044:
	{
		return (bool)G_B4_0;
	}
}
// System.Void Black_Perl::.ctor()
extern "C"  void Black_Perl__ctor_m2474732353 (Black_Perl_t354423663 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Black_Perl::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* sandcastleLifes_t1127312347_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t Black_Perl_OnTriggerEnter_m1162736599_MetadataUsageId;
extern "C"  void Black_Perl_OnTriggerEnter_m1162736599 (Black_Perl_t354423663 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Black_Perl_OnTriggerEnter_m1162736599_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_002b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(sandcastleLifes_t1127312347_il2cpp_TypeInfo_var);
		int32_t L_4 = ((sandcastleLifes_t1127312347_StaticFields*)sandcastleLifes_t1127312347_il2cpp_TypeInfo_var->static_fields)->get_trigger1_2();
		if ((!(((uint32_t)L_4) == ((uint32_t)1))))
		{
			goto IL_002b;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 6, /*hidden argument*/NULL);
	}

IL_002b:
	{
		return;
	}
}
// System.Void Black_Perl::Main()
extern "C"  void Black_Perl_Main_m2489176412 (Black_Perl_t354423663 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CameraCollision::.ctor()
extern "C"  void CameraCollision__ctor_m1440316553 (CameraCollision_t1979127149 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_canScroll_2((bool)1);
		__this->set_detectionRadius_4((0.15f));
		__this->set_zoomDistance_5((((float)((float)1))));
		__this->set_maxZoomOut_6(5);
		__this->set_maxZoomIn_7(3);
		return;
	}
}
// System.Void CameraCollision::Start()
extern const Il2CppType* Camera_t2727095145_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Camera_t2727095145_il2cpp_TypeInfo_var;
extern Il2CppClass* GameObject_t3674682005_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4276285927;
extern Il2CppCodeGenString* _stringLiteral3148844886;
extern const uint32_t CameraCollision_Start_m387454345_MetadataUsageId;
extern "C"  void CameraCollision_Start_m387454345 (CameraCollision_t1979127149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraCollision_Start_m387454345_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(Camera_t2727095145_0_0_0_var), /*hidden argument*/NULL);
		Component_t3501516275 * L_1 = Component_GetComponent_m936021879(__this, L_0, /*hidden argument*/NULL);
		NullCheck(((Camera_t2727095145 *)CastclassSealed(L_1, Camera_t2727095145_il2cpp_TypeInfo_var)));
		Camera_set_nearClipPlane_m534185950(((Camera_t2727095145 *)CastclassSealed(L_1, Camera_t2727095145_il2cpp_TypeInfo_var)), (0.01f), /*hidden argument*/NULL);
		Transform_t1659122786 * L_2 = __this->get_focusPoint_3();
		bool L_3 = Object_op_Equality_m3964590952(NULL /*static, unused*/, L_2, (Object_t3071478659 *)NULL, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0046;
		}
	}
	{
		Transform_t1659122786 * L_4 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Transform_get_parent_m2236876972(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		__this->set_focusPoint_3(L_6);
	}

IL_0046:
	{
		GameObject_t3674682005 * L_7 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m845034556(L_7, /*hidden argument*/NULL);
		__this->set_camSpot_11(L_7);
		GameObject_t3674682005 * L_8 = __this->get_camSpot_11();
		NullCheck(L_8);
		Transform_t1659122786 * L_9 = GameObject_get_transform_m1278640159(L_8, /*hidden argument*/NULL);
		NullCheck(L_9);
		Object_set_name_m1123518500(L_9, _stringLiteral4276285927, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_10 = __this->get_camSpot_11();
		NullCheck(L_10);
		Transform_t1659122786 * L_11 = GameObject_get_transform_m1278640159(L_10, /*hidden argument*/NULL);
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_12);
		Transform_t1659122786 * L_13 = Transform_get_parent_m2236876972(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_parent_m3231272063(L_11, L_13, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_14 = __this->get_camSpot_11();
		NullCheck(L_14);
		Transform_t1659122786 * L_15 = GameObject_get_transform_m1278640159(L_14, /*hidden argument*/NULL);
		Transform_t1659122786 * L_16 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_16);
		Vector3_t4282066566  L_17 = Transform_get_position_m2211398607(L_16, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_position_m3111394108(L_15, L_17, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_18 = (GameObject_t3674682005 *)il2cpp_codegen_object_new(GameObject_t3674682005_il2cpp_TypeInfo_var);
		GameObject__ctor_m845034556(L_18, /*hidden argument*/NULL);
		__this->set_camFollow_10(L_18);
		GameObject_t3674682005 * L_19 = __this->get_camFollow_10();
		NullCheck(L_19);
		Transform_t1659122786 * L_20 = GameObject_get_transform_m1278640159(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Object_set_name_m1123518500(L_20, _stringLiteral3148844886, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_21 = __this->get_camFollow_10();
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		Transform_t1659122786 * L_23 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = Transform_get_parent_m2236876972(L_23, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_parent_m3231272063(L_22, L_24, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_25 = __this->get_camFollow_10();
		NullCheck(L_25);
		Transform_t1659122786 * L_26 = GameObject_get_transform_m1278640159(L_25, /*hidden argument*/NULL);
		Transform_t1659122786 * L_27 = __this->get_focusPoint_3();
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = Transform_get_position_m2211398607(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		Transform_set_position_m3111394108(L_26, L_28, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_29 = __this->get_camFollow_10();
		NullCheck(L_29);
		Transform_t1659122786 * L_30 = GameObject_get_transform_m1278640159(L_29, /*hidden argument*/NULL);
		Transform_t1659122786 * L_31 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_30);
		Transform_LookAt_m2663225588(L_30, L_31, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CameraCollision::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2268470611;
extern const uint32_t CameraCollision_Update_m3427002276_MetadataUsageId;
extern "C"  void CameraCollision_Update_m3427002276 (CameraCollision_t1979127149 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CameraCollision_Update_m3427002276_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_0 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		if ((((float)L_0) <= ((float)(((float)((float)0))))))
		{
			goto IL_008b;
		}
	}
	{
		bool L_1 = __this->get_canScroll_2();
		if ((!(((uint32_t)L_1) == ((uint32_t)1))))
		{
			goto IL_008b;
		}
	}
	{
		int32_t L_2 = __this->get_zoom_8();
		int32_t L_3 = __this->get_maxZoomIn_7();
		if ((((int32_t)L_2) >= ((int32_t)L_3)))
		{
			goto IL_008b;
		}
	}
	{
		GameObject_t3674682005 * L_4 = __this->get_camSpot_11();
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = __this->get_camSpot_11();
		NullCheck(L_6);
		Transform_t1659122786 * L_7 = GameObject_get_transform_m1278640159(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_position_m2211398607(L_7, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_9 = __this->get_camFollow_10();
		NullCheck(L_9);
		Transform_t1659122786 * L_10 = GameObject_get_transform_m1278640159(L_9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t4282066566  L_11 = Transform_get_forward_m877665793(L_10, /*hidden argument*/NULL);
		Vector3_t4282066566  L_12 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		Vector3_t4282066566  L_13 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, (((float)((float)1))), L_12, /*hidden argument*/NULL);
		Vector3_t4282066566  L_14 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_8, L_13, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_position_m3111394108(L_5, L_14, /*hidden argument*/NULL);
		int32_t L_15 = __this->get_maxZoomOut_6();
		__this->set_maxZoomOut_6(((int32_t)((int32_t)L_15+(int32_t)1)));
		int32_t L_16 = __this->get_maxZoomIn_7();
		__this->set_maxZoomIn_7(((int32_t)((int32_t)L_16-(int32_t)1)));
	}

IL_008b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		float L_17 = Input_GetAxis_m2027668530(NULL /*static, unused*/, _stringLiteral2268470611, /*hidden argument*/NULL);
		if ((((float)L_17) >= ((float)(((float)((float)0))))))
		{
			goto IL_0117;
		}
	}
	{
		bool L_18 = __this->get_canScroll_2();
		if ((!(((uint32_t)L_18) == ((uint32_t)1))))
		{
			goto IL_0117;
		}
	}
	{
		int32_t L_19 = __this->get_zoom_8();
		int32_t L_20 = __this->get_maxZoomOut_6();
		if ((((int32_t)L_19) <= ((int32_t)((-L_20)))))
		{
			goto IL_0117;
		}
	}
	{
		GameObject_t3674682005 * L_21 = __this->get_camSpot_11();
		NullCheck(L_21);
		Transform_t1659122786 * L_22 = GameObject_get_transform_m1278640159(L_21, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_23 = __this->get_camSpot_11();
		NullCheck(L_23);
		Transform_t1659122786 * L_24 = GameObject_get_transform_m1278640159(L_23, /*hidden argument*/NULL);
		NullCheck(L_24);
		Vector3_t4282066566  L_25 = Transform_get_position_m2211398607(L_24, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_26 = __this->get_camFollow_10();
		NullCheck(L_26);
		Transform_t1659122786 * L_27 = GameObject_get_transform_m1278640159(L_26, /*hidden argument*/NULL);
		NullCheck(L_27);
		Vector3_t4282066566  L_28 = Transform_get_forward_m877665793(L_27, /*hidden argument*/NULL);
		Vector3_t4282066566  L_29 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		Vector3_t4282066566  L_30 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, (((float)((float)1))), L_29, /*hidden argument*/NULL);
		Vector3_t4282066566  L_31 = Vector3_op_Subtraction_m2842958165(NULL /*static, unused*/, L_25, L_30, /*hidden argument*/NULL);
		NullCheck(L_22);
		Transform_set_position_m3111394108(L_22, L_31, /*hidden argument*/NULL);
		int32_t L_32 = __this->get_maxZoomOut_6();
		__this->set_maxZoomOut_6(((int32_t)((int32_t)L_32-(int32_t)1)));
		int32_t L_33 = __this->get_maxZoomIn_7();
		__this->set_maxZoomIn_7(((int32_t)((int32_t)L_33+(int32_t)1)));
	}

IL_0117:
	{
		GameObject_t3674682005 * L_34 = __this->get_camFollow_10();
		NullCheck(L_34);
		Transform_t1659122786 * L_35 = GameObject_get_transform_m1278640159(L_34, /*hidden argument*/NULL);
		NullCheck(L_35);
		Vector3_t4282066566  L_36 = Transform_get_position_m2211398607(L_35, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_37 = __this->get_camSpot_11();
		NullCheck(L_37);
		Transform_t1659122786 * L_38 = GameObject_get_transform_m1278640159(L_37, /*hidden argument*/NULL);
		NullCheck(L_38);
		Vector3_t4282066566  L_39 = Transform_get_position_m2211398607(L_38, /*hidden argument*/NULL);
		float L_40 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_36, L_39, /*hidden argument*/NULL);
		V_0 = L_40;
		GameObject_t3674682005 * L_41 = __this->get_camFollow_10();
		NullCheck(L_41);
		Transform_t1659122786 * L_42 = GameObject_get_transform_m1278640159(L_41, /*hidden argument*/NULL);
		NullCheck(L_42);
		Vector3_t4282066566  L_43 = Transform_get_position_m2211398607(L_42, /*hidden argument*/NULL);
		Transform_t1659122786 * L_44 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_44);
		Vector3_t4282066566  L_45 = Transform_get_position_m2211398607(L_44, /*hidden argument*/NULL);
		float L_46 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_43, L_45, /*hidden argument*/NULL);
		V_1 = L_46;
		GameObject_t3674682005 * L_47 = __this->get_camFollow_10();
		NullCheck(L_47);
		Transform_t1659122786 * L_48 = GameObject_get_transform_m1278640159(L_47, /*hidden argument*/NULL);
		NullCheck(L_48);
		Vector3_t4282066566  L_49 = Transform_get_position_m2211398607(L_48, /*hidden argument*/NULL);
		float L_50 = __this->get_detectionRadius_4();
		GameObject_t3674682005 * L_51 = __this->get_camFollow_10();
		NullCheck(L_51);
		Transform_t1659122786 * L_52 = GameObject_get_transform_m1278640159(L_51, /*hidden argument*/NULL);
		NullCheck(L_52);
		Vector3_t4282066566  L_53 = Transform_get_forward_m877665793(L_52, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_54 = __this->get_address_of_hit_9();
		float L_55 = V_0;
		bool L_56 = Physics_SphereCast_m2556362501(NULL /*static, unused*/, L_49, L_50, L_53, L_54, L_55, /*hidden argument*/NULL);
		if (!L_56)
		{
			goto IL_02b5;
		}
	}
	{
		GameObject_t3674682005 * L_57 = __this->get_camFollow_10();
		NullCheck(L_57);
		Transform_t1659122786 * L_58 = GameObject_get_transform_m1278640159(L_57, /*hidden argument*/NULL);
		NullCheck(L_58);
		Vector3_t4282066566  L_59 = Transform_get_position_m2211398607(L_58, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_60 = __this->get_address_of_hit_9();
		Vector3_t4282066566  L_61 = RaycastHit_get_point_m4165497838(L_60, /*hidden argument*/NULL);
		float L_62 = Vector3_Distance_m3366690344(NULL /*static, unused*/, L_59, L_61, /*hidden argument*/NULL);
		V_2 = L_62;
		float L_63 = V_2;
		float L_64 = V_1;
		if ((((float)L_63) >= ((float)L_64)))
		{
			goto IL_0221;
		}
	}
	{
		float L_65 = V_1;
		if ((((float)L_65) <= ((float)(((float)((float)1))))))
		{
			goto IL_0201;
		}
	}
	{
		Transform_t1659122786 * L_66 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_67 = __this->get_address_of_hit_9();
		Vector3_t4282066566  L_68 = RaycastHit_get_point_m4165497838(L_67, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_69 = __this->get_camFollow_10();
		NullCheck(L_69);
		Transform_t1659122786 * L_70 = GameObject_get_transform_m1278640159(L_69, /*hidden argument*/NULL);
		NullCheck(L_70);
		Vector3_t4282066566  L_71 = Transform_get_forward_m877665793(L_70, /*hidden argument*/NULL);
		Vector3_t4282066566  L_72 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_71, /*hidden argument*/NULL);
		Vector3_t4282066566  L_73 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, (((float)((float)1))), L_72, /*hidden argument*/NULL);
		Vector3_t4282066566  L_74 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_68, L_73, /*hidden argument*/NULL);
		NullCheck(L_66);
		Transform_set_position_m3111394108(L_66, L_74, /*hidden argument*/NULL);
		goto IL_021c;
	}

IL_0201:
	{
		Transform_t1659122786 * L_75 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_76 = __this->get_camFollow_10();
		NullCheck(L_76);
		Transform_t1659122786 * L_77 = GameObject_get_transform_m1278640159(L_76, /*hidden argument*/NULL);
		NullCheck(L_77);
		Vector3_t4282066566  L_78 = Transform_get_position_m2211398607(L_77, /*hidden argument*/NULL);
		NullCheck(L_75);
		Transform_set_position_m3111394108(L_75, L_78, /*hidden argument*/NULL);
	}

IL_021c:
	{
		goto IL_02b0;
	}

IL_0221:
	{
		float L_79 = V_1;
		if ((((float)L_79) <= ((float)(((float)((float)1))))))
		{
			goto IL_027d;
		}
	}
	{
		Transform_t1659122786 * L_80 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_81 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_81);
		Vector3_t4282066566  L_82 = Transform_get_position_m2211398607(L_81, /*hidden argument*/NULL);
		RaycastHit_t4003175726 * L_83 = __this->get_address_of_hit_9();
		Vector3_t4282066566  L_84 = RaycastHit_get_point_m4165497838(L_83, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_85 = __this->get_camFollow_10();
		NullCheck(L_85);
		Transform_t1659122786 * L_86 = GameObject_get_transform_m1278640159(L_85, /*hidden argument*/NULL);
		NullCheck(L_86);
		Vector3_t4282066566  L_87 = Transform_get_forward_m877665793(L_86, /*hidden argument*/NULL);
		Vector3_t4282066566  L_88 = Vector3_op_UnaryNegation_m3293197314(NULL /*static, unused*/, L_87, /*hidden argument*/NULL);
		Vector3_t4282066566  L_89 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, (((float)((float)1))), L_88, /*hidden argument*/NULL);
		Vector3_t4282066566  L_90 = Vector3_op_Addition_m695438225(NULL /*static, unused*/, L_84, L_89, /*hidden argument*/NULL);
		float L_91 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_92 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_82, L_90, ((float)((float)(((float)((float)5)))*(float)L_91)), /*hidden argument*/NULL);
		NullCheck(L_80);
		Transform_set_position_m3111394108(L_80, L_92, /*hidden argument*/NULL);
		goto IL_02b0;
	}

IL_027d:
	{
		Transform_t1659122786 * L_93 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_94 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_94);
		Vector3_t4282066566  L_95 = Transform_get_position_m2211398607(L_94, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_96 = __this->get_camFollow_10();
		NullCheck(L_96);
		Transform_t1659122786 * L_97 = GameObject_get_transform_m1278640159(L_96, /*hidden argument*/NULL);
		NullCheck(L_97);
		Vector3_t4282066566  L_98 = Transform_get_position_m2211398607(L_97, /*hidden argument*/NULL);
		float L_99 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_100 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_95, L_98, ((float)((float)(((float)((float)5)))*(float)L_99)), /*hidden argument*/NULL);
		NullCheck(L_93);
		Transform_set_position_m3111394108(L_93, L_100, /*hidden argument*/NULL);
	}

IL_02b0:
	{
		goto IL_02e8;
	}

IL_02b5:
	{
		Transform_t1659122786 * L_101 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_102 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_102);
		Vector3_t4282066566  L_103 = Transform_get_position_m2211398607(L_102, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_104 = __this->get_camSpot_11();
		NullCheck(L_104);
		Transform_t1659122786 * L_105 = GameObject_get_transform_m1278640159(L_104, /*hidden argument*/NULL);
		NullCheck(L_105);
		Vector3_t4282066566  L_106 = Transform_get_position_m2211398607(L_105, /*hidden argument*/NULL);
		float L_107 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_108 = Vector3_MoveTowards_m2405650085(NULL /*static, unused*/, L_103, L_106, ((float)((float)(((float)((float)5)))*(float)L_107)), /*hidden argument*/NULL);
		NullCheck(L_101);
		Transform_set_position_m3111394108(L_101, L_108, /*hidden argument*/NULL);
	}

IL_02e8:
	{
		return;
	}
}
// System.Void CameraCollision::Main()
extern "C"  void CameraCollision_Main_m2040166164 (CameraCollision_t1979127149 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void cameraSpin::.ctor()
extern "C"  void cameraSpin__ctor_m884782249 (cameraSpin_t2282820871 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void cameraSpin::Update()
extern "C"  void cameraSpin_Update_m3385308036 (cameraSpin_t2282820871 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m569346304(L_0, (((float)((float)0))), (((float)((float)2))), (((float)((float)0))), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void cameraSpin::Main()
extern "C"  void cameraSpin_Main_m3546266356 (cameraSpin_t2282820871 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CharacterMovement::.ctor()
extern "C"  void CharacterMovement__ctor_m788163262 (CharacterMovement_t2528585432 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_speed_2((((float)((float)7))));
		__this->set_gravity_3((((float)((float)((int32_t)10)))));
		Vector3_t4282066566  L_0 = Vector3_get_zero_m2017759730(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->set_moveDirection_5(L_0);
		return;
	}
}
// System.Void CharacterMovement::Start()
extern const Il2CppType* CharacterController_t1618060635_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterController_t1618060635_il2cpp_TypeInfo_var;
extern const uint32_t CharacterMovement_Start_m4030268350_MetadataUsageId;
extern "C"  void CharacterMovement_Start_m4030268350 (CharacterMovement_t2528585432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_Start_m4030268350_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(CharacterController_t1618060635_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3501516275 * L_2 = Component_GetComponent_m936021879(L_0, L_1, /*hidden argument*/NULL);
		__this->set_controller_4(((CharacterController_t1618060635 *)CastclassSealed(L_2, CharacterController_t1618060635_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void CharacterMovement::Update()
extern Il2CppClass* Input_t4200062272_il2cpp_TypeInfo_var;
extern Il2CppClass* Vector3_t4282066566_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral119;
extern Il2CppCodeGenString* _stringLiteral115;
extern Il2CppCodeGenString* _stringLiteral97;
extern Il2CppCodeGenString* _stringLiteral100;
extern const uint32_t CharacterMovement_Update_m390119439_MetadataUsageId;
extern "C"  void CharacterMovement_Update_m390119439 (CharacterMovement_t2528585432 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_Update_m390119439_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t4282066566  V_0;
	memset(&V_0, 0, sizeof(V_0));
	Vector3_t4282066566  V_1;
	memset(&V_1, 0, sizeof(V_1));
	{
		Vector3_t4282066566 * L_0 = __this->get_address_of_moveDirection_5();
		float L_1 = L_0->get_y_2();
		float L_2 = __this->get_gravity_3();
		if ((((float)L_1) <= ((float)((float)((float)L_2*(float)(((float)((float)(-1)))))))))
		{
			goto IL_003c;
		}
	}
	{
		Vector3_t4282066566 * L_3 = __this->get_address_of_moveDirection_5();
		Vector3_t4282066566 * L_4 = __this->get_address_of_moveDirection_5();
		float L_5 = L_4->get_y_2();
		float L_6 = __this->get_gravity_3();
		float L_7 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		L_3->set_y_2(((float)((float)L_5-(float)((float)((float)L_6*(float)L_7)))));
	}

IL_003c:
	{
		CharacterController_t1618060635 * L_8 = __this->get_controller_4();
		Vector3_t4282066566  L_9 = __this->get_moveDirection_5();
		float L_10 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_11 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_8);
		CharacterController_Move_m3043020731(L_8, L_11, /*hidden argument*/NULL);
		Transform_t1659122786 * L_12 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector3_t4282066566  L_13 = Vector3_get_left_m1616598929(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t4282066566  L_14 = Transform_TransformDirection_m83001769(L_12, L_13, /*hidden argument*/NULL);
		V_0 = L_14;
		CharacterController_t1618060635 * L_15 = __this->get_controller_4();
		NullCheck(L_15);
		bool L_16 = CharacterController_get_isGrounded_m1739295843(L_15, /*hidden argument*/NULL);
		if (!L_16)
		{
			goto IL_0161;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_17 = Input_GetKeyDown_m2928824675(NULL /*static, unused*/, ((int32_t)32), /*hidden argument*/NULL);
		if (!L_17)
		{
			goto IL_009b;
		}
	}
	{
		Vector3_t4282066566 * L_18 = __this->get_address_of_moveDirection_5();
		float L_19 = __this->get_speed_2();
		L_18->set_y_2(L_19);
		goto IL_015c;
	}

IL_009b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_20 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral119, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_00d1;
		}
	}
	{
		CharacterController_t1618060635 * L_21 = __this->get_controller_4();
		Transform_t1659122786 * L_22 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t4282066566  L_23 = Transform_get_forward_m877665793(L_22, /*hidden argument*/NULL);
		float L_24 = __this->get_speed_2();
		Vector3_t4282066566  L_25 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		CharacterController_SimpleMove_m3593592780(L_21, L_25, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_00d1:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_26 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral115, /*hidden argument*/NULL);
		if (!L_26)
		{
			goto IL_0108;
		}
	}
	{
		CharacterController_t1618060635 * L_27 = __this->get_controller_4();
		Transform_t1659122786 * L_28 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_28);
		Vector3_t4282066566  L_29 = Transform_get_forward_m877665793(L_28, /*hidden argument*/NULL);
		float L_30 = __this->get_speed_2();
		Vector3_t4282066566  L_31 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_29, ((-L_30)), /*hidden argument*/NULL);
		NullCheck(L_27);
		CharacterController_SimpleMove_m3593592780(L_27, L_31, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_0108:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_32 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral97, /*hidden argument*/NULL);
		if (!L_32)
		{
			goto IL_0134;
		}
	}
	{
		CharacterController_t1618060635 * L_33 = __this->get_controller_4();
		Vector3_t4282066566  L_34 = V_0;
		float L_35 = __this->get_speed_2();
		Vector3_t4282066566  L_36 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_34, L_35, /*hidden argument*/NULL);
		NullCheck(L_33);
		CharacterController_SimpleMove_m3593592780(L_33, L_36, /*hidden argument*/NULL);
		goto IL_015c;
	}

IL_0134:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_37 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral100, /*hidden argument*/NULL);
		if (!L_37)
		{
			goto IL_015c;
		}
	}
	{
		CharacterController_t1618060635 * L_38 = __this->get_controller_4();
		Vector3_t4282066566  L_39 = V_0;
		float L_40 = __this->get_speed_2();
		Vector3_t4282066566  L_41 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_39, ((-L_40)), /*hidden argument*/NULL);
		NullCheck(L_38);
		CharacterController_SimpleMove_m3593592780(L_38, L_41, /*hidden argument*/NULL);
	}

IL_015c:
	{
		goto IL_01b6;
	}

IL_0161:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t4200062272_il2cpp_TypeInfo_var);
		bool L_42 = Input_GetKey_m3403617450(NULL /*static, unused*/, _stringLiteral119, /*hidden argument*/NULL);
		if (!L_42)
		{
			goto IL_01b6;
		}
	}
	{
		Initobj (Vector3_t4282066566_il2cpp_TypeInfo_var, (&V_1));
		Transform_t1659122786 * L_43 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_43);
		Vector3_t4282066566  L_44 = Transform_TransformDirection_m3326859781(L_43, (((float)((float)0))), (((float)((float)0))), (((float)((float)1))), /*hidden argument*/NULL);
		V_1 = L_44;
		CharacterController_t1618060635 * L_45 = __this->get_controller_4();
		Vector3_t4282066566  L_46 = V_1;
		float L_47 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t4282066566  L_48 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_46, L_47, /*hidden argument*/NULL);
		float L_49 = __this->get_speed_2();
		Vector3_t4282066566  L_50 = Vector3_op_Multiply_m973638459(NULL /*static, unused*/, L_48, L_49, /*hidden argument*/NULL);
		Vector3_t4282066566  L_51 = Vector3_op_Division_m4277988370(NULL /*static, unused*/, L_50, (1.5f), /*hidden argument*/NULL);
		NullCheck(L_45);
		CharacterController_Move_m3043020731(L_45, L_51, /*hidden argument*/NULL);
	}

IL_01b6:
	{
		return;
	}
}
// System.Void CharacterMovement::OnTriggerEnter(UnityEngine.Collider)
extern const Il2CppType* LoadNewScene_t2813835762_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LoadNewScene_t2813835762_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2813835762;
extern const uint32_t CharacterMovement_OnTriggerEnter_m1343824250_MetadataUsageId;
extern "C"  void CharacterMovement_OnTriggerEnter_m1343824250 (CharacterMovement_t2528585432 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_OnTriggerEnter_m1343824250_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Component_get_tag_m217485006(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2813835762, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		Collider_t2939674232 * L_4 = ___hit0;
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(LoadNewScene_t2813835762_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3501516275 * L_7 = Component_GetComponent_m936021879(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(((LoadNewScene_t2813835762 *)CastclassClass(L_7, LoadNewScene_t2813835762_il2cpp_TypeInfo_var)));
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(5 /* System.Collections.IEnumerator LoadNewScene::DisplayScene() */, ((LoadNewScene_t2813835762 *)CastclassClass(L_7, LoadNewScene_t2813835762_il2cpp_TypeInfo_var)));
		MonoBehaviour_StartCoroutine_Auto_m1831125106(__this, L_8, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void CharacterMovement::OnTriggerExit(UnityEngine.Collider)
extern const Il2CppType* LoadNewScene_t2813835762_0_0_0_var;
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* LoadNewScene_t2813835762_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2813835762;
extern const uint32_t CharacterMovement_OnTriggerExit_m2048576744_MetadataUsageId;
extern "C"  void CharacterMovement_OnTriggerExit_m2048576744 (CharacterMovement_t2528585432 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_OnTriggerExit_m2048576744_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = Component_get_transform_m4257140443(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Component_get_tag_m217485006(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2813835762, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0040;
		}
	}
	{
		Collider_t2939674232 * L_4 = ___hit0;
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = Component_get_transform_m4257140443(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_6 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(LoadNewScene_t2813835762_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_5);
		Component_t3501516275 * L_7 = Component_GetComponent_m936021879(L_5, L_6, /*hidden argument*/NULL);
		NullCheck(((LoadNewScene_t2813835762 *)CastclassClass(L_7, LoadNewScene_t2813835762_il2cpp_TypeInfo_var)));
		Il2CppObject * L_8 = VirtFuncInvoker0< Il2CppObject * >::Invoke(6 /* System.Collections.IEnumerator LoadNewScene::HideScene() */, ((LoadNewScene_t2813835762 *)CastclassClass(L_7, LoadNewScene_t2813835762_il2cpp_TypeInfo_var)));
		MonoBehaviour_StartCoroutine_Auto_m1831125106(__this, L_8, /*hidden argument*/NULL);
	}

IL_0040:
	{
		return;
	}
}
// System.Void CharacterMovement::OnControllerColliderHit(UnityEngine.ControllerColliderHit)
extern const MethodInfo* Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var;
extern const uint32_t CharacterMovement_OnControllerColliderHit_m3640053894_MetadataUsageId;
extern "C"  void CharacterMovement_OnControllerColliderHit_m3640053894 (CharacterMovement_t2528585432 * __this, ControllerColliderHit_t2416790841 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (CharacterMovement_OnControllerColliderHit_m3640053894_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		ControllerColliderHit_t2416790841 * L_0 = ___hit0;
		NullCheck(L_0);
		Transform_t1659122786 * L_1 = ControllerColliderHit_get_transform_m1182304469(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Rigidbody_t3346577219 * L_2 = Component_GetComponent_TisRigidbody_t3346577219_m2174365699(L_1, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var);
		bool L_3 = Object_op_Implicit_m2106766291(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0038;
		}
	}
	{
		ControllerColliderHit_t2416790841 * L_4 = ___hit0;
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = ControllerColliderHit_get_transform_m1182304469(L_4, /*hidden argument*/NULL);
		NullCheck(L_5);
		Rigidbody_t3346577219 * L_6 = Component_GetComponent_TisRigidbody_t3346577219_m2174365699(L_5, /*hidden argument*/Component_GetComponent_TisRigidbody_t3346577219_m2174365699_MethodInfo_var);
		Transform_t1659122786 * L_7 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Vector3_t4282066566  L_8 = Transform_get_forward_m877665793(L_7, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9 = Vector3_op_Multiply_m3809076219(NULL /*static, unused*/, (((float)((float)((int32_t)10)))), L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		Rigidbody_AddForce_m3682301239(L_6, L_9, /*hidden argument*/NULL);
	}

IL_0038:
	{
		return;
	}
}
// System.Void CharacterMovement::Main()
extern "C"  void CharacterMovement_Main_m4097338943 (CharacterMovement_t2528585432 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void coin taken::.ctor()
extern "C"  void coinU20taken__ctor_m746433112 (coinU20taken_t1434922616 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void coin taken::Start()
extern "C"  void coinU20taken_Start_m3988538200 (coinU20taken_t1434922616 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void coin taken::Update()
extern "C"  void coinU20taken_Update_m3391452085 (coinU20taken_t1434922616 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void coin taken::Main()
extern "C"  void coinU20taken_Main_m3957445477 (coinU20taken_t1434922616 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void CoinSpin::.ctor()
extern "C"  void CoinSpin__ctor_m460253149 (CoinSpin_t3643068051 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CoinSpin::Update()
extern "C"  void CoinSpin_Update_m3109807824 (CoinSpin_t3643068051 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_Rotate_m569346304(L_0, (((float)((float)0))), (((float)((float)4))), (((float)((float)0))), 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CoinSpin::Main()
extern "C"  void CoinSpin_Main_m2562740544 (CoinSpin_t3643068051 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void enemy::.ctor()
extern "C"  void enemy__ctor_m3282291854 (enemy_t96653192 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_enemyhealth_2(((int32_t)50));
		return;
	}
}
// System.Void enemy::DeductPoints(System.Int32)
extern "C"  void enemy_DeductPoints_m1056746317 (enemy_t96653192 * __this, int32_t ___hitpoints0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_enemyhealth_2();
		int32_t L_1 = ___hitpoints0;
		__this->set_enemyhealth_2(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		return;
	}
}
// System.Void enemy::Update()
extern "C"  void enemy_Update_m398694463 (enemy_t96653192 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_enemyhealth_2();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void enemy::Main()
extern "C"  void enemy_Main_m3069416047 (enemy_t96653192 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void gobacktofirstbar::.ctor()
extern "C"  void gobacktofirstbar__ctor_m1362364611 (gobacktofirstbar_t2648077805 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void gobacktofirstbar::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t gobacktofirstbar_OnTriggerEnter_m969645461_MetadataUsageId;
extern "C"  void gobacktofirstbar_OnTriggerEnter_m969645461 (gobacktofirstbar_t2648077805 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (gobacktofirstbar_OnTriggerEnter_m969645461_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void gobacktofirstbar::Main()
extern "C"  void gobacktofirstbar_Main_m2868935578 (gobacktofirstbar_t2648077805 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GoToBar::.ctor()
extern "C"  void GoToBar__ctor_m1856216294 (GoToBar_t1847036784 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GoToBar::Main()
extern Il2CppClass* U24MainU2437_t2773556266_il2cpp_TypeInfo_var;
extern const uint32_t GoToBar_Main_m1668053573_MetadataUsageId;
extern "C"  Il2CppObject * GoToBar_Main_m1668053573 (GoToBar_t1847036784 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoToBar_Main_m1668053573_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2437_t2773556266 * L_0 = (U24MainU2437_t2773556266 *)il2cpp_codegen_object_new(U24MainU2437_t2773556266_il2cpp_TypeInfo_var);
		U24MainU2437__ctor_m3889436454(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2437_GetEnumerator_m1256932934(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void GoToBar/$Main$37::.ctor()
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2437__ctor_m3889436454_MetadataUsageId;
extern "C"  void U24MainU2437__ctor_m3889436454 (U24MainU2437_t2773556266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2437__ctor_m3889436454_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> GoToBar/$Main$37::GetEnumerator()
extern Il2CppClass* U24_t2507849599_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2437_GetEnumerator_m1256932934_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2437_GetEnumerator_m1256932934 (U24MainU2437_t2773556266 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2437_GetEnumerator_m1256932934_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24_t2507849599 * L_0 = (U24_t2507849599 *)il2cpp_codegen_object_new(U24_t2507849599_il2cpp_TypeInfo_var);
		U24__ctor_m2751181873(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void GoToBar/$Main$37/$::.ctor()
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m2751181873_MetadataUsageId;
extern "C"  void U24__ctor_m2751181873 (U24_t2507849599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2751181873_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		return;
	}
}
// System.Boolean GoToBar/$Main$37/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m732796885_MetadataUsageId;
extern "C"  bool U24_MoveNext_m732796885 (U24_t2507849599 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m732796885_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0039;
		}
		if (L_0 == 2)
		{
			goto IL_002b;
		}
	}

IL_0017:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)((int32_t)15)))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_003a;
	}

IL_002b:
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0039:
	{
		G_B4_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B4_0;
	}
}
// System.Void GoToBar002::.ctor()
extern "C"  void GoToBar002__ctor_m425628494 (GoToBar002_t2246850754 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoToBar002::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t GoToBar002_OnTriggerEnter_m203850474_MetadataUsageId;
extern "C"  void GoToBar002_OnTriggerEnter_m203850474 (GoToBar002_t2246850754 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (GoToBar002_OnTriggerEnter_m203850474_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0020;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 4, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void GoToBar002::Main()
extern "C"  void GoToBar002_Main_m1868886959 (GoToBar002_t2246850754 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GoToBar1::.ctor()
extern "C"  void GoToBar1__ctor_m2640488303 (GoToBar1_t1423565505 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GoToBar1::Main()
extern "C"  void GoToBar1_Main_m2355976046 (GoToBar1_t1423565505 * __this, const MethodInfo* method)
{
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void health::.ctor()
extern "C"  void health__ctor_m3602949492 (health_t3073704540 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void health::Main()
extern "C"  void health_Main_m2248475849 (health_t3073704540 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void HitVars::.ctor()
extern "C"  void HitVars__ctor_m3959088375 (HitVars_t2591603775 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void HitVars::.cctor()
extern Il2CppClass* HitVars_t2591603775_il2cpp_TypeInfo_var;
extern const uint32_t HitVars__cctor_m1990559126_MetadataUsageId;
extern "C"  void HitVars__cctor_m1990559126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (HitVars__cctor_m1990559126_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((HitVars_t2591603775_StaticFields*)HitVars_t2591603775_il2cpp_TypeInfo_var->static_fields)->set_hitpoints_2(((int32_t)10));
		((HitVars_t2591603775_StaticFields*)HitVars_t2591603775_il2cpp_TypeInfo_var->static_fields)->set_range_4((((float)((float)5))));
		return;
	}
}
// System.Void HitVars::Main()
extern "C"  void HitVars_Main_m43206886 (HitVars_t2591603775 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Jaagub::.ctor()
extern "C"  void Jaagub__ctor_m2085018758 (Jaagub_t2211131146 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_enemyhealth_2(((int32_t)50));
		return;
	}
}
// System.Void Jaagub::DeductPoints(System.Int32)
extern "C"  void Jaagub_DeductPoints_m3855673429 (Jaagub_t2211131146 * __this, int32_t ___hitpoints0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_enemyhealth_2();
		int32_t L_1 = ___hitpoints0;
		__this->set_enemyhealth_2(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		return;
	}
}
// System.Void Jaagub::Update()
extern "C"  void Jaagub_Update_m1937934151 (Jaagub_t2211131146 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_enemyhealth_2();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_0017;
		}
	}
	{
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void Jaagub::Main()
extern "C"  void Jaagub_Main_m3862078327 (Jaagub_t2211131146 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Jordberprat::.ctor()
extern "C"  void Jordberprat__ctor_m237308649 (Jordberprat_t2203708557 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Jordberprat::Main()
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var;
extern const uint32_t Jordberprat_Main_m477338804_MetadataUsageId;
extern "C"  void Jordberprat_Main_m477338804 (Jordberprat_t2203708557 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Jordberprat_Main_m477338804_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		int32_t L_0 = ((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->get_myScore_2();
		if ((((int32_t)L_0) <= ((int32_t)0)))
		{
			goto IL_0016;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 5, /*hidden argument*/NULL);
		goto IL_0021;
	}

IL_0016:
	{
		AudioSource_t1740077639 * L_1 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_1);
		AudioSource_Play_m1360558992(L_1, /*hidden argument*/NULL);
	}

IL_0021:
	{
		return;
	}
}
// System.Void LoadNewScene::.ctor()
extern "C"  void LoadNewScene__ctor_m2205960414 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadNewScene::Start()
extern Il2CppClass* LoadNewScene_t2813835762_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1207976373;
extern Il2CppCodeGenString* _stringLiteral80818744;
extern const uint32_t LoadNewScene_Start_m1153098206_MetadataUsageId;
extern "C"  void LoadNewScene_Start_m1153098206 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadNewScene_Start_m1153098206_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral1207976373, /*hidden argument*/NULL);
		((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->set_title_Background_3(L_0);
		GameObject_t3674682005 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral80818744, /*hidden argument*/NULL);
		((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->set_title_4(L_1);
		return;
	}
}
// System.Collections.IEnumerator LoadNewScene::DisplayScene()
extern Il2CppClass* U24DisplaySceneU2417_t4064840153_il2cpp_TypeInfo_var;
extern const uint32_t LoadNewScene_DisplayScene_m360774050_MetadataUsageId;
extern "C"  Il2CppObject * LoadNewScene_DisplayScene_m360774050 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadNewScene_DisplayScene_m360774050_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24DisplaySceneU2417_t4064840153 * L_0 = (U24DisplaySceneU2417_t4064840153 *)il2cpp_codegen_object_new(U24DisplaySceneU2417_t4064840153_il2cpp_TypeInfo_var);
		U24DisplaySceneU2417__ctor_m4290325163(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24DisplaySceneU2417_GetEnumerator_m720241883(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Collections.IEnumerator LoadNewScene::HideScene()
extern Il2CppClass* U24HideSceneU2428_t3689129383_il2cpp_TypeInfo_var;
extern const uint32_t LoadNewScene_HideScene_m1348930580_MetadataUsageId;
extern "C"  Il2CppObject * LoadNewScene_HideScene_m1348930580 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadNewScene_HideScene_m1348930580_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24HideSceneU2428_t3689129383 * L_0 = (U24HideSceneU2428_t3689129383 *)il2cpp_codegen_object_new(U24HideSceneU2428_t3689129383_il2cpp_TypeInfo_var);
		U24HideSceneU2428__ctor_m597204489(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24HideSceneU2428_GetEnumerator_m3464629243(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LoadNewScene::LoadScene()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral32;
extern const uint32_t LoadNewScene_LoadScene_m3608904258_MetadataUsageId;
extern "C"  void LoadNewScene_LoadScene_m3608904258 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (LoadNewScene_LoadScene_m3608904258_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = NULL;
	{
		String_t* L_0 = __this->get_newScene_2();
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->get_Empty_2();
		NullCheck(L_0);
		String_t* L_2 = String_Replace_m2915759397(L_0, _stringLiteral32, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void LoadNewScene::Main()
extern "C"  void LoadNewScene_Main_m4143074335 (LoadNewScene_t2813835762 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void LoadNewScene/$DisplayScene$17::.ctor(LoadNewScene)
extern const MethodInfo* GenericGenerator_1__ctor_m1943341621_MethodInfo_var;
extern const uint32_t U24DisplaySceneU2417__ctor_m4290325163_MetadataUsageId;
extern "C"  void U24DisplaySceneU2417__ctor_m4290325163 (U24DisplaySceneU2417_t4064840153 * __this, LoadNewScene_t2813835762 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24DisplaySceneU2417__ctor_m4290325163_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1943341621(__this, /*hidden argument*/GenericGenerator_1__ctor_m1943341621_MethodInfo_var);
		LoadNewScene_t2813835762 * L_0 = ___self_0;
		__this->set_U24self_U2427_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> LoadNewScene/$DisplayScene$17::GetEnumerator()
extern Il2CppClass* U24_t2186116462_il2cpp_TypeInfo_var;
extern const uint32_t U24DisplaySceneU2417_GetEnumerator_m720241883_MetadataUsageId;
extern "C"  Il2CppObject* U24DisplaySceneU2417_GetEnumerator_m720241883 (U24DisplaySceneU2417_t4064840153 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24DisplaySceneU2417_GetEnumerator_m720241883_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		LoadNewScene_t2813835762 * L_0 = __this->get_U24self_U2427_0();
		U24_t2186116462 * L_1 = (U24_t2186116462 *)il2cpp_codegen_object_new(U24_t2186116462_il2cpp_TypeInfo_var);
		U24__ctor_m2453716342(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void LoadNewScene/$DisplayScene$17/$::.ctor(LoadNewScene)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m1672667353_MethodInfo_var;
extern const uint32_t U24__ctor_m2453716342_MetadataUsageId;
extern "C"  void U24__ctor_m2453716342 (U24_t2186116462 * __this, LoadNewScene_t2813835762 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2453716342_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m1672667353(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m1672667353_MethodInfo_var);
		LoadNewScene_t2813835762 * L_0 = ___self_0;
		__this->set_U24self_U2426_10(L_0);
		return;
	}
}
// System.Boolean LoadNewScene/$DisplayScene$17/$::MoveNext()
extern Il2CppClass* LoadNewScene_t2813835762_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var;
extern const uint32_t U24_MoveNext_m509452482_MetadataUsageId;
extern "C"  bool U24_MoveNext_m509452482 (U24_t2186116462 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m509452482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Color_t4194546905  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Color_t4194546905  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Color_t4194546905  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Color_t4194546905  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Color_t4194546905  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Color_t4194546905  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float V_17 = 0.0f;
	Color_t4194546905  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t67576739 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_021b;
		}
		if (L_0 == 2)
		{
			goto IL_0135;
		}
	}

IL_0017:
	{
		GameObject_t3674682005 * L_1 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_1);
		Text_t9039225 * L_2 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		LoadNewScene_t2813835762 * L_3 = __this->get_U24self_U2426_10();
		NullCheck(L_3);
		String_t* L_4 = L_3->get_newScene_2();
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_4);
		goto IL_0135;
	}

IL_0036:
	{
		GameObject_t3674682005 * L_5 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_5);
		Image_t538875265 * L_6 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_5, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_6);
		Color_t4194546905  L_7 = Graphic_get_color_m2048831972(L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		float L_8 = (&V_0)->get_a_3();
		float L_9 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_10 = ((float)((float)L_8+(float)((float)((float)(2.0f)*(float)L_9))));
		V_1 = L_10;
		__this->set_U24U241U2418_2(L_10);
		float L_11 = V_1;
		GameObject_t3674682005 * L_12 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_12);
		Image_t538875265 * L_13 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_12, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_13);
		Color_t4194546905  L_14 = Graphic_get_color_m2048831972(L_13, /*hidden argument*/NULL);
		Color_t4194546905  L_15 = L_14;
		V_2 = L_15;
		__this->set_U24U242U2419_3(L_15);
		Color_t4194546905  L_16 = V_2;
		Color_t4194546905 * L_17 = __this->get_address_of_U24U242U2419_3();
		float L_18 = __this->get_U24U241U2418_2();
		float L_19 = L_18;
		V_3 = L_19;
		L_17->set_a_3(L_19);
		float L_20 = V_3;
		GameObject_t3674682005 * L_21 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_21);
		Image_t538875265 * L_22 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_21, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		Color_t4194546905  L_23 = __this->get_U24U242U2419_3();
		Color_t4194546905  L_24 = L_23;
		V_4 = L_24;
		NullCheck(L_22);
		Graphic_set_color_m1311501487(L_22, L_24, /*hidden argument*/NULL);
		Color_t4194546905  L_25 = V_4;
		GameObject_t3674682005 * L_26 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_26);
		Text_t9039225 * L_27 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_26, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_27);
		Color_t4194546905  L_28 = Graphic_get_color_m2048831972(L_27, /*hidden argument*/NULL);
		V_5 = L_28;
		float L_29 = (&V_5)->get_a_3();
		float L_30 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_31 = ((float)((float)L_29+(float)((float)((float)(2.0f)*(float)L_30))));
		V_6 = L_31;
		__this->set_U24U243U2420_4(L_31);
		float L_32 = V_6;
		GameObject_t3674682005 * L_33 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_33);
		Text_t9039225 * L_34 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_33, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_34);
		Color_t4194546905  L_35 = Graphic_get_color_m2048831972(L_34, /*hidden argument*/NULL);
		Color_t4194546905  L_36 = L_35;
		V_7 = L_36;
		__this->set_U24U244U2421_5(L_36);
		Color_t4194546905  L_37 = V_7;
		Color_t4194546905 * L_38 = __this->get_address_of_U24U244U2421_5();
		float L_39 = __this->get_U24U243U2420_4();
		float L_40 = L_39;
		V_8 = L_40;
		L_38->set_a_3(L_40);
		float L_41 = V_8;
		GameObject_t3674682005 * L_42 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_42);
		Text_t9039225 * L_43 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_42, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		Color_t4194546905  L_44 = __this->get_U24U244U2421_5();
		Color_t4194546905  L_45 = L_44;
		V_9 = L_45;
		NullCheck(L_43);
		Graphic_set_color_m1311501487(L_43, L_45, /*hidden argument*/NULL);
		Color_t4194546905  L_46 = V_9;
		bool L_47 = GenericGeneratorEnumerator_1_YieldDefault_m3387285518(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var);
		G_B6_0 = ((int32_t)(L_47));
		goto IL_021c;
	}

IL_0135:
	{
		GameObject_t3674682005 * L_48 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_48);
		Image_t538875265 * L_49 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_48, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_49);
		Color_t4194546905  L_50 = Graphic_get_color_m2048831972(L_49, /*hidden argument*/NULL);
		V_10 = L_50;
		float L_51 = (&V_10)->get_a_3();
		if ((((float)L_51) < ((float)(0.9f))))
		{
			goto IL_0036;
		}
	}
	{
		float L_52 = (1.0f);
		V_11 = L_52;
		__this->set_U24U245U2422_6(L_52);
		float L_53 = V_11;
		GameObject_t3674682005 * L_54 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_54);
		Image_t538875265 * L_55 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_54, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_55);
		Color_t4194546905  L_56 = Graphic_get_color_m2048831972(L_55, /*hidden argument*/NULL);
		Color_t4194546905  L_57 = L_56;
		V_12 = L_57;
		__this->set_U24U246U2423_7(L_57);
		Color_t4194546905  L_58 = V_12;
		Color_t4194546905 * L_59 = __this->get_address_of_U24U246U2423_7();
		float L_60 = __this->get_U24U245U2422_6();
		float L_61 = L_60;
		V_13 = L_61;
		L_59->set_a_3(L_61);
		float L_62 = V_13;
		GameObject_t3674682005 * L_63 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_63);
		Image_t538875265 * L_64 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_63, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		Color_t4194546905  L_65 = __this->get_U24U246U2423_7();
		Color_t4194546905  L_66 = L_65;
		V_14 = L_66;
		NullCheck(L_64);
		Graphic_set_color_m1311501487(L_64, L_66, /*hidden argument*/NULL);
		Color_t4194546905  L_67 = V_14;
		float L_68 = (1.0f);
		V_15 = L_68;
		__this->set_U24U247U2424_8(L_68);
		float L_69 = V_15;
		GameObject_t3674682005 * L_70 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_70);
		Text_t9039225 * L_71 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_70, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_71);
		Color_t4194546905  L_72 = Graphic_get_color_m2048831972(L_71, /*hidden argument*/NULL);
		Color_t4194546905  L_73 = L_72;
		V_16 = L_73;
		__this->set_U24U248U2425_9(L_73);
		Color_t4194546905  L_74 = V_16;
		Color_t4194546905 * L_75 = __this->get_address_of_U24U248U2425_9();
		float L_76 = __this->get_U24U247U2424_8();
		float L_77 = L_76;
		V_17 = L_77;
		L_75->set_a_3(L_77);
		float L_78 = V_17;
		GameObject_t3674682005 * L_79 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_79);
		Text_t9039225 * L_80 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_79, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		Color_t4194546905  L_81 = __this->get_U24U248U2425_9();
		Color_t4194546905  L_82 = L_81;
		V_18 = L_82;
		NullCheck(L_80);
		Graphic_set_color_m1311501487(L_80, L_82, /*hidden argument*/NULL);
		Color_t4194546905  L_83 = V_18;
		GenericGeneratorEnumerator_1_YieldDefault_m3387285518(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var);
	}

IL_021b:
	{
		G_B6_0 = 0;
	}

IL_021c:
	{
		return (bool)G_B6_0;
	}
}
// System.Void LoadNewScene/$HideScene$28::.ctor()
extern const MethodInfo* GenericGenerator_1__ctor_m1943341621_MethodInfo_var;
extern const uint32_t U24HideSceneU2428__ctor_m597204489_MetadataUsageId;
extern "C"  void U24HideSceneU2428__ctor_m597204489 (U24HideSceneU2428_t3689129383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24HideSceneU2428__ctor_m597204489_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1943341621(__this, /*hidden argument*/GenericGenerator_1__ctor_m1943341621_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Object> LoadNewScene/$HideScene$28::GetEnumerator()
extern Il2CppClass* U24_t1905319356_il2cpp_TypeInfo_var;
extern const uint32_t U24HideSceneU2428_GetEnumerator_m3464629243_MetadataUsageId;
extern "C"  Il2CppObject* U24HideSceneU2428_GetEnumerator_m3464629243 (U24HideSceneU2428_t3689129383 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24HideSceneU2428_GetEnumerator_m3464629243_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24_t1905319356 * L_0 = (U24_t1905319356 *)il2cpp_codegen_object_new(U24_t1905319356_il2cpp_TypeInfo_var);
		U24__ctor_m12193364(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void LoadNewScene/$HideScene$28/$::.ctor()
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m1672667353_MethodInfo_var;
extern const uint32_t U24__ctor_m12193364_MetadataUsageId;
extern "C"  void U24__ctor_m12193364 (U24_t1905319356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m12193364_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m1672667353(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m1672667353_MethodInfo_var);
		return;
	}
}
// System.Boolean LoadNewScene/$HideScene$28/$::MoveNext()
extern Il2CppClass* LoadNewScene_t2813835762_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var;
extern const uint32_t U24_MoveNext_m4017414482_MetadataUsageId;
extern "C"  bool U24_MoveNext_m4017414482 (U24_t1905319356 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m4017414482_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	Color_t4194546905  V_0;
	memset(&V_0, 0, sizeof(V_0));
	float V_1 = 0.0f;
	Color_t4194546905  V_2;
	memset(&V_2, 0, sizeof(V_2));
	float V_3 = 0.0f;
	Color_t4194546905  V_4;
	memset(&V_4, 0, sizeof(V_4));
	Color_t4194546905  V_5;
	memset(&V_5, 0, sizeof(V_5));
	float V_6 = 0.0f;
	Color_t4194546905  V_7;
	memset(&V_7, 0, sizeof(V_7));
	float V_8 = 0.0f;
	Color_t4194546905  V_9;
	memset(&V_9, 0, sizeof(V_9));
	Color_t4194546905  V_10;
	memset(&V_10, 0, sizeof(V_10));
	float V_11 = 0.0f;
	Color_t4194546905  V_12;
	memset(&V_12, 0, sizeof(V_12));
	float V_13 = 0.0f;
	Color_t4194546905  V_14;
	memset(&V_14, 0, sizeof(V_14));
	float V_15 = 0.0f;
	Color_t4194546905  V_16;
	memset(&V_16, 0, sizeof(V_16));
	float V_17 = 0.0f;
	Color_t4194546905  V_18;
	memset(&V_18, 0, sizeof(V_18));
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t67576739 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_01fb;
		}
		if (L_0 == 2)
		{
			goto IL_011b;
		}
	}

IL_0017:
	{
		goto IL_011b;
	}

IL_001c:
	{
		GameObject_t3674682005 * L_1 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_1);
		Image_t538875265 * L_2 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_1, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_2);
		Color_t4194546905  L_3 = Graphic_get_color_m2048831972(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		float L_4 = (&V_0)->get_a_3();
		float L_5 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_6 = ((float)((float)L_4-(float)((float)((float)(2.0f)*(float)L_5))));
		V_1 = L_6;
		__this->set_U24U249U2429_2(L_6);
		float L_7 = V_1;
		GameObject_t3674682005 * L_8 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_8);
		Image_t538875265 * L_9 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_8, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_9);
		Color_t4194546905  L_10 = Graphic_get_color_m2048831972(L_9, /*hidden argument*/NULL);
		Color_t4194546905  L_11 = L_10;
		V_2 = L_11;
		__this->set_U24U2410U2430_3(L_11);
		Color_t4194546905  L_12 = V_2;
		Color_t4194546905 * L_13 = __this->get_address_of_U24U2410U2430_3();
		float L_14 = __this->get_U24U249U2429_2();
		float L_15 = L_14;
		V_3 = L_15;
		L_13->set_a_3(L_15);
		float L_16 = V_3;
		GameObject_t3674682005 * L_17 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_17);
		Image_t538875265 * L_18 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_17, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		Color_t4194546905  L_19 = __this->get_U24U2410U2430_3();
		Color_t4194546905  L_20 = L_19;
		V_4 = L_20;
		NullCheck(L_18);
		Graphic_set_color_m1311501487(L_18, L_20, /*hidden argument*/NULL);
		Color_t4194546905  L_21 = V_4;
		GameObject_t3674682005 * L_22 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_22);
		Text_t9039225 * L_23 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_22, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_23);
		Color_t4194546905  L_24 = Graphic_get_color_m2048831972(L_23, /*hidden argument*/NULL);
		V_5 = L_24;
		float L_25 = (&V_5)->get_a_3();
		float L_26 = Time_get_deltaTime_m2741110510(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_27 = ((float)((float)L_25-(float)((float)((float)(2.0f)*(float)L_26))));
		V_6 = L_27;
		__this->set_U24U2411U2431_4(L_27);
		float L_28 = V_6;
		GameObject_t3674682005 * L_29 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_29);
		Text_t9039225 * L_30 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_29, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_30);
		Color_t4194546905  L_31 = Graphic_get_color_m2048831972(L_30, /*hidden argument*/NULL);
		Color_t4194546905  L_32 = L_31;
		V_7 = L_32;
		__this->set_U24U2412U2432_5(L_32);
		Color_t4194546905  L_33 = V_7;
		Color_t4194546905 * L_34 = __this->get_address_of_U24U2412U2432_5();
		float L_35 = __this->get_U24U2411U2431_4();
		float L_36 = L_35;
		V_8 = L_36;
		L_34->set_a_3(L_36);
		float L_37 = V_8;
		GameObject_t3674682005 * L_38 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_38);
		Text_t9039225 * L_39 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_38, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		Color_t4194546905  L_40 = __this->get_U24U2412U2432_5();
		Color_t4194546905  L_41 = L_40;
		V_9 = L_41;
		NullCheck(L_39);
		Graphic_set_color_m1311501487(L_39, L_41, /*hidden argument*/NULL);
		Color_t4194546905  L_42 = V_9;
		bool L_43 = GenericGeneratorEnumerator_1_YieldDefault_m3387285518(__this, 2, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var);
		G_B6_0 = ((int32_t)(L_43));
		goto IL_01fc;
	}

IL_011b:
	{
		GameObject_t3674682005 * L_44 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_44);
		Image_t538875265 * L_45 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_44, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_45);
		Color_t4194546905  L_46 = Graphic_get_color_m2048831972(L_45, /*hidden argument*/NULL);
		V_10 = L_46;
		float L_47 = (&V_10)->get_a_3();
		if ((((float)L_47) > ((float)(0.01f))))
		{
			goto IL_001c;
		}
	}
	{
		float L_48 = (((float)((float)0)));
		V_11 = L_48;
		__this->set_U24U2413U2433_6(L_48);
		float L_49 = V_11;
		GameObject_t3674682005 * L_50 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_50);
		Image_t538875265 * L_51 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_50, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		NullCheck(L_51);
		Color_t4194546905  L_52 = Graphic_get_color_m2048831972(L_51, /*hidden argument*/NULL);
		Color_t4194546905  L_53 = L_52;
		V_12 = L_53;
		__this->set_U24U2414U2434_7(L_53);
		Color_t4194546905  L_54 = V_12;
		Color_t4194546905 * L_55 = __this->get_address_of_U24U2414U2434_7();
		float L_56 = __this->get_U24U2413U2433_6();
		float L_57 = L_56;
		V_13 = L_57;
		L_55->set_a_3(L_57);
		float L_58 = V_13;
		GameObject_t3674682005 * L_59 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_Background_3();
		NullCheck(L_59);
		Image_t538875265 * L_60 = GameObject_GetComponent_TisImage_t538875265_m3560905424(L_59, /*hidden argument*/GameObject_GetComponent_TisImage_t538875265_m3560905424_MethodInfo_var);
		Color_t4194546905  L_61 = __this->get_U24U2414U2434_7();
		Color_t4194546905  L_62 = L_61;
		V_14 = L_62;
		NullCheck(L_60);
		Graphic_set_color_m1311501487(L_60, L_62, /*hidden argument*/NULL);
		Color_t4194546905  L_63 = V_14;
		float L_64 = (((float)((float)0)));
		V_15 = L_64;
		__this->set_U24U2415U2435_8(L_64);
		float L_65 = V_15;
		GameObject_t3674682005 * L_66 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_66);
		Text_t9039225 * L_67 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_66, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		NullCheck(L_67);
		Color_t4194546905  L_68 = Graphic_get_color_m2048831972(L_67, /*hidden argument*/NULL);
		Color_t4194546905  L_69 = L_68;
		V_16 = L_69;
		__this->set_U24U2416U2436_9(L_69);
		Color_t4194546905  L_70 = V_16;
		Color_t4194546905 * L_71 = __this->get_address_of_U24U2416U2436_9();
		float L_72 = __this->get_U24U2415U2435_8();
		float L_73 = L_72;
		V_17 = L_73;
		L_71->set_a_3(L_73);
		float L_74 = V_17;
		GameObject_t3674682005 * L_75 = ((LoadNewScene_t2813835762_StaticFields*)LoadNewScene_t2813835762_il2cpp_TypeInfo_var->static_fields)->get_title_4();
		NullCheck(L_75);
		Text_t9039225 * L_76 = GameObject_GetComponent_TisText_t9039225_m2604051366(L_75, /*hidden argument*/GameObject_GetComponent_TisText_t9039225_m2604051366_MethodInfo_var);
		Color_t4194546905  L_77 = __this->get_U24U2416U2436_9();
		Color_t4194546905  L_78 = L_77;
		V_18 = L_78;
		NullCheck(L_76);
		Graphic_set_color_m1311501487(L_76, L_78, /*hidden argument*/NULL);
		Color_t4194546905  L_79 = V_18;
		GenericGeneratorEnumerator_1_YieldDefault_m3387285518(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m3387285518_MethodInfo_var);
	}

IL_01fb:
	{
		G_B6_0 = 0;
	}

IL_01fc:
	{
		return (bool)G_B6_0;
	}
}
// System.Void map::.ctor()
extern "C"  void map__ctor_m2201079290 (map_t107868 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void map::Start()
extern "C"  void map_Start_m1148217082 (map_t107868 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void map::Update()
extern "C"  void map_Update_m1240843347 (map_t107868 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void map::Main()
extern "C"  void map_Main_m3727274883 (map_t107868 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Peptalk001::.ctor()
extern "C"  void Peptalk001__ctor_m4249990246 (Peptalk001_t3966958762 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Peptalk001::Main()
extern Il2CppClass* U24MainU2438_t3773328433_il2cpp_TypeInfo_var;
extern const uint32_t Peptalk001_Main_m1310406057_MetadataUsageId;
extern "C"  Il2CppObject * Peptalk001_Main_m1310406057 (Peptalk001_t3966958762 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Peptalk001_Main_m1310406057_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2438_t3773328433 * L_0 = (U24MainU2438_t3773328433 *)il2cpp_codegen_object_new(U24MainU2438_t3773328433_il2cpp_TypeInfo_var);
		U24MainU2438__ctor_m1199793755(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2438_GetEnumerator_m3742278741(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Peptalk001/$Main$38::.ctor(Peptalk001)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2438__ctor_m1199793755_MetadataUsageId;
extern "C"  void U24MainU2438__ctor_m1199793755 (U24MainU2438_t3773328433 * __this, Peptalk001_t3966958762 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2438__ctor_m1199793755_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		Peptalk001_t3966958762 * L_0 = ___self_0;
		__this->set_U24self_U2440_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Peptalk001/$Main$38::GetEnumerator()
extern Il2CppClass* U24_t1216227782_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2438_GetEnumerator_m3742278741_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2438_GetEnumerator_m3742278741 (U24MainU2438_t3773328433 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2438_GetEnumerator_m3742278741_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Peptalk001_t3966958762 * L_0 = __this->get_U24self_U2440_0();
		U24_t1216227782 * L_1 = (U24_t1216227782 *)il2cpp_codegen_object_new(U24_t1216227782_il2cpp_TypeInfo_var);
		U24__ctor_m339097062(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Peptalk001/$Main$38/$::.ctor(Peptalk001)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m339097062_MetadataUsageId;
extern "C"  void U24__ctor_m339097062 (U24_t1216227782 * __this, Peptalk001_t3966958762 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m339097062_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		Peptalk001_t3966958762 * L_0 = ___self_0;
		__this->set_U24self_U2439_2(L_0);
		return;
	}
}
// System.Boolean Peptalk001/$Main$38/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m3131154394_MetadataUsageId;
extern "C"  bool U24_MoveNext_m3131154394 (U24_t1216227782 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m3131154394_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_0079;
		}
		if (L_0 == 2)
		{
			goto IL_003e;
		}
		if (L_0 == 3)
		{
			goto IL_0061;
		}
	}

IL_001b:
	{
		Peptalk001_t3966958762 * L_1 = __this->get_U24self_U2439_2();
		NullCheck(L_1);
		AudioSource_t1740077639 * L_2 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_1, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_2);
		AudioSource_Play_m1360558992(L_2, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_3 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_4));
		goto IL_007a;
	}

IL_003e:
	{
		Peptalk001_t3966958762 * L_5 = __this->get_U24self_U2439_2();
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_5, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_6);
		AudioSource_Play_m1360558992(L_6, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_7 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_7, (((float)((float)2))), /*hidden argument*/NULL);
		bool L_8 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 3, L_7, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_8));
		goto IL_007a;
	}

IL_0061:
	{
		Peptalk001_t3966958762 * L_9 = __this->get_U24self_U2439_2();
		NullCheck(L_9);
		AudioSource_t1740077639 * L_10 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_9, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_10);
		AudioSource_Play_m1360558992(L_10, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0079:
	{
		G_B5_0 = 0;
	}

IL_007a:
	{
		return (bool)G_B5_0;
	}
}
// System.Void Peptalk002::.ctor()
extern "C"  void Peptalk002__ctor_m4053476741 (Peptalk002_t3966958763 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Peptalk002::Main()
extern Il2CppClass* U24MainU2441_t3576814952_il2cpp_TypeInfo_var;
extern const uint32_t Peptalk002_Main_m3797918890_MetadataUsageId;
extern "C"  Il2CppObject * Peptalk002_Main_m3797918890 (Peptalk002_t3966958763 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Peptalk002_Main_m3797918890_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2441_t3576814952 * L_0 = (U24MainU2441_t3576814952 *)il2cpp_codegen_object_new(U24MainU2441_t3576814952_il2cpp_TypeInfo_var);
		U24MainU2441__ctor_m671048675(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2441_GetEnumerator_m2248992766(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Peptalk002/$Main$41::.ctor(Peptalk002)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2441__ctor_m671048675_MetadataUsageId;
extern "C"  void U24MainU2441__ctor_m671048675 (U24MainU2441_t3576814952 * __this, Peptalk002_t3966958763 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2441__ctor_m671048675_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		Peptalk002_t3966958763 * L_0 = ___self_0;
		__this->set_U24self_U2443_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Peptalk002/$Main$41::GetEnumerator()
extern Il2CppClass* U24_t1345333565_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2441_GetEnumerator_m2248992766_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2441_GetEnumerator_m2248992766 (U24MainU2441_t3576814952 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2441_GetEnumerator_m2248992766_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Peptalk002_t3966958763 * L_0 = __this->get_U24self_U2443_0();
		U24_t1345333565 * L_1 = (U24_t1345333565 *)il2cpp_codegen_object_new(U24_t1345333565_il2cpp_TypeInfo_var);
		U24__ctor_m3316153646(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Peptalk002/$Main$41/$::.ctor(Peptalk002)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m3316153646_MetadataUsageId;
extern "C"  void U24__ctor_m3316153646 (U24_t1345333565 * __this, Peptalk002_t3966958763 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3316153646_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		Peptalk002_t3966958763 * L_0 = ___self_0;
		__this->set_U24self_U2442_2(L_0);
		return;
	}
}
// System.Boolean Peptalk002/$Main$41/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m590685393_MetadataUsageId;
extern "C"  bool U24_MoveNext_m590685393 (U24_t1345333565 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m590685393_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0064;
		}
		if (L_0 == 2)
		{
			goto IL_003b;
		}
	}

IL_0017:
	{
		Peptalk002_t3966958763 * L_1 = __this->get_U24self_U2442_2();
		NullCheck(L_1);
		AudioSource_t1740077639 * L_2 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_1, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_2);
		Behaviour_set_enabled_m2046806933(L_2, (bool)1, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_3 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_4));
		goto IL_0065;
	}

IL_003b:
	{
		Peptalk002_t3966958763 * L_5 = __this->get_U24self_U2442_2();
		NullCheck(L_5);
		AudioSource_t1740077639 * L_6 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_5, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_6);
		AudioSource_Play_m1360558992(L_6, /*hidden argument*/NULL);
		Peptalk002_t3966958763 * L_7 = __this->get_U24self_U2442_2();
		NullCheck(L_7);
		AudioSource_t1740077639 * L_8 = Component_GetComponent_TisAudioSource_t1740077639_m2453081547(L_7, /*hidden argument*/Component_GetComponent_TisAudioSource_t1740077639_m2453081547_MethodInfo_var);
		NullCheck(L_8);
		Behaviour_set_enabled_m2046806933(L_8, (bool)0, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0064:
	{
		G_B4_0 = 0;
	}

IL_0065:
	{
		return (bool)G_B4_0;
	}
}
// System.Void pickup::.ctor()
extern "C"  void pickup__ctor_m966246740 (pickup_t3306490492 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pickup::Update()
extern "C"  void pickup_Update_m1615739961 (pickup_t3306490492 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void pickup::OnMouseDown()
extern Il2CppCodeGenString* _stringLiteral2186127685;
extern Il2CppCodeGenString* _stringLiteral3827208868;
extern const uint32_t pickup_OnMouseDown_m3411407738_MetadataUsageId;
extern "C"  void pickup_OnMouseDown_m3411407738 (pickup_t3306490492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (pickup_OnMouseDown_m3411407738_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_1 = __this->get_target_2();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_4 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2186127685, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t1659122786 * L_5 = GameObject_get_transform_m1278640159(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_parent_m3231272063(L_3, L_5, /*hidden argument*/NULL);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_7 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral3827208868, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = GameObject_get_transform_m1278640159(L_7, /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_parent_m3231272063(L_6, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pickup::OnMouseUp()
extern Il2CppCodeGenString* _stringLiteral2186127685;
extern const uint32_t pickup_OnMouseUp_m1992879347_MetadataUsageId;
extern "C"  void pickup_OnMouseUp_m1992879347 (pickup_t3306490492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (pickup_OnMouseUp_m1992879347_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_1 = GameObject_Find_m332785498(NULL /*static, unused*/, _stringLiteral2186127685, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_t1659122786 * L_2 = GameObject_get_transform_m1278640159(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_parent_m3231272063(L_0, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_parent_m3231272063(L_3, (Transform_t1659122786 *)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void pickup::Main()
extern "C"  void pickup_Main_m1609231593 (pickup_t3306490492 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Portal::.ctor()
extern "C"  void Portal__ctor_m356886852 (Portal_t2396353676 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Portal::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* U24OnTriggerEnterU2448_t458821736_il2cpp_TypeInfo_var;
extern const uint32_t Portal_OnTriggerEnter_m3753774150_MetadataUsageId;
extern "C"  Il2CppObject * Portal_OnTriggerEnter_m3753774150 (Portal_t2396353676 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (Portal_OnTriggerEnter_m3753774150_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		U24OnTriggerEnterU2448_t458821736 * L_1 = (U24OnTriggerEnterU2448_t458821736 *)il2cpp_codegen_object_new(U24OnTriggerEnterU2448_t458821736_il2cpp_TypeInfo_var);
		U24OnTriggerEnterU2448__ctor_m1882592995(L_1, L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Il2CppObject* L_2 = U24OnTriggerEnterU2448_GetEnumerator_m427524286(L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void Portal::Main()
extern "C"  void Portal_Main_m65554169 (Portal_t2396353676 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Portal/$OnTriggerEnter$48::.ctor(UnityEngine.Collider)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24OnTriggerEnterU2448__ctor_m1882592995_MetadataUsageId;
extern "C"  void U24OnTriggerEnterU2448__ctor_m1882592995 (U24OnTriggerEnterU2448_t458821736 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24OnTriggerEnterU2448__ctor_m1882592995_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		Collider_t2939674232 * L_0 = ___hit0;
		__this->set_U24hitU2451_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Portal/$OnTriggerEnter$48::GetEnumerator()
extern Il2CppClass* U24_t2841025597_il2cpp_TypeInfo_var;
extern const uint32_t U24OnTriggerEnterU2448_GetEnumerator_m427524286_MetadataUsageId;
extern "C"  Il2CppObject* U24OnTriggerEnterU2448_GetEnumerator_m427524286 (U24OnTriggerEnterU2448_t458821736 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24OnTriggerEnterU2448_GetEnumerator_m427524286_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = __this->get_U24hitU2451_0();
		U24_t2841025597 * L_1 = (U24_t2841025597 *)il2cpp_codegen_object_new(U24_t2841025597_il2cpp_TypeInfo_var);
		U24__ctor_m3790511726(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void Portal/$OnTriggerEnter$48/$::.ctor(UnityEngine.Collider)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m3790511726_MetadataUsageId;
extern "C"  void U24__ctor_m3790511726 (U24_t2841025597 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3790511726_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		Collider_t2939674232 * L_0 = ___hit0;
		__this->set_U24hitU2450_3(L_0);
		return;
	}
}
// System.Boolean Portal/$OnTriggerEnter$48/$::MoveNext()
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern Il2CppCodeGenString* _stringLiteral2654077975;
extern const uint32_t U24_MoveNext_m114794641_MetadataUsageId;
extern "C"  bool U24_MoveNext_m114794641 (U24_t2841025597 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m114794641_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B9_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0081;
		}
		if (L_0 == 2)
		{
			goto IL_0072;
		}
	}

IL_0017:
	{
		Collider_t2939674232 * L_1 = __this->get_U24hitU2450_3();
		NullCheck(L_1);
		GameObject_t3674682005 * L_2 = Component_get_gameObject_m1170635899(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = GameObject_get_tag_m211612200(L_2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_4 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_3, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0058;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		int32_t L_5 = ((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->get_myScore_2();
		if ((((int32_t)L_5) <= ((int32_t)((int32_t)9))))
		{
			goto IL_0053;
		}
	}
	{
		__this->set_U24morecashU2449_2(0);
		Application_LoadLevel_m2722573885(NULL /*static, unused*/, _stringLiteral2654077975, /*hidden argument*/NULL);
	}

IL_0053:
	{
		goto IL_0079;
	}

IL_0058:
	{
		__this->set_U24morecashU2449_2(1);
		WaitForSeconds_t1615819279 * L_6 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_6, (((float)((float)3))), /*hidden argument*/NULL);
		bool L_7 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_6, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B9_0 = ((int32_t)(L_7));
		goto IL_0082;
	}

IL_0072:
	{
		__this->set_U24morecashU2449_2(0);
	}

IL_0079:
	{
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0081:
	{
		G_B9_0 = 0;
	}

IL_0082:
	{
		return (bool)G_B9_0;
	}
}
// System.Void poschange::.ctor()
extern "C"  void poschange__ctor_m1267797778 (poschange_t1524020996 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void poschange::Main()
extern "C"  void poschange_Main_m2865885035 (poschange_t1524020996 * __this, const MethodInfo* method)
{
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Vector2_t4282066565  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Vector2__ctor_m1517109030(&L_1, (((float)((float)((int32_t)-20)))), (((float)((float)3))), /*hidden argument*/NULL);
		Vector3_t4282066566  L_2 = Vector2_op_Implicit_m482286037(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RespawnMenuV2::.ctor()
extern "C"  void RespawnMenuV2__ctor_m2327531539 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RespawnMenuV2::.cctor()
extern "C"  void RespawnMenuV2__cctor_m2951904762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void RespawnMenuV2::Start()
extern const Il2CppType* CharacterController_t1618060635_0_0_0_var;
extern Il2CppClass* Type_t_il2cpp_TypeInfo_var;
extern Il2CppClass* CharacterController_t1618060635_il2cpp_TypeInfo_var;
extern const uint32_t RespawnMenuV2_Start_m1274669331_MetadataUsageId;
extern "C"  void RespawnMenuV2_Start_m1274669331 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RespawnMenuV2_Start_m1274669331_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t3674682005 * L_0 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m3806905434(NULL /*static, unused*/, LoadTypeToken(CharacterController_t1618060635_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		Component_t3501516275 * L_2 = GameObject_GetComponent_m1004814461(L_0, L_1, /*hidden argument*/NULL);
		__this->set_charController_2(((CharacterController_t1618060635 *)CastclassSealed(L_2, CharacterController_t1618060635_il2cpp_TypeInfo_var)));
		return;
	}
}
// System.Void RespawnMenuV2::Update()
extern Il2CppClass* RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var;
extern const uint32_t RespawnMenuV2_Update_m865895770_MetadataUsageId;
extern "C"  void RespawnMenuV2_Update_m865895770 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RespawnMenuV2_Update_m865895770_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var);
		bool L_0 = ((RespawnMenuV2_t3986403171_StaticFields*)RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var->static_fields)->get_playerIsDead_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_0017;
		}
	}
	{
		CharacterController_t1618060635 * L_1 = __this->get_charController_2();
		NullCheck(L_1);
		Collider_set_enabled_m2575670866(L_1, (bool)0, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void RespawnMenuV2::OnGUI()
extern Il2CppClass* RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2762040584;
extern Il2CppCodeGenString* _stringLiteral2394495;
extern Il2CppCodeGenString* _stringLiteral765900084;
extern const uint32_t RespawnMenuV2_OnGUI_m1822930189_MetadataUsageId;
extern "C"  void RespawnMenuV2_OnGUI_m1822930189 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RespawnMenuV2_OnGUI_m1822930189_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var);
		bool L_0 = ((RespawnMenuV2_t3986403171_StaticFields*)RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var->static_fields)->get_playerIsDead_4();
		if ((!(((uint32_t)L_0) == ((uint32_t)1))))
		{
			goto IL_007b;
		}
	}
	{
		int32_t L_1 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, ((float)((float)((float)((float)(((float)((float)L_1)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)50)))))), (((float)((float)((int32_t)180)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)40)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_3 = GUI_Button_m885093907(NULL /*static, unused*/, L_2, _stringLiteral2762040584, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0041;
		}
	}
	{
		VirtActionInvoker0::Invoke(7 /* System.Void RespawnMenuV2::RespawnPlayer() */, __this);
	}

IL_0041:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_5;
		memset(&L_5, 0, sizeof(L_5));
		Rect__ctor_m3291325233(&L_5, ((float)((float)((float)((float)(((float)((float)L_4)))*(float)(0.5f)))-(float)(((float)((float)((int32_t)50)))))), (((float)((float)((int32_t)240)))), (((float)((float)((int32_t)100)))), (((float)((float)((int32_t)40)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_6 = GUI_Button_m885093907(NULL /*static, unused*/, L_5, _stringLiteral2394495, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_007b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral765900084, /*hidden argument*/NULL);
	}

IL_007b:
	{
		return;
	}
}
// System.Void RespawnMenuV2::RespawnPlayer()
extern Il2CppClass* RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var;
extern Il2CppClass* Debug_t4195163081_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2212779127;
extern Il2CppCodeGenString* _stringLiteral3319714530;
extern const uint32_t RespawnMenuV2_RespawnPlayer_m1829664026_MetadataUsageId;
extern "C"  void RespawnMenuV2_RespawnPlayer_m1829664026 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (RespawnMenuV2_RespawnPlayer_m1829664026_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t1659122786 * L_0 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_1 = __this->get_respawnTransform_3();
		NullCheck(L_1);
		Vector3_t4282066566  L_2 = Transform_get_position_m2211398607(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m3111394108(L_0, L_2, /*hidden argument*/NULL);
		Transform_t1659122786 * L_3 = Component_get_transform_m4257140443(__this, /*hidden argument*/NULL);
		Transform_t1659122786 * L_4 = __this->get_respawnTransform_3();
		NullCheck(L_4);
		Quaternion_t1553702882  L_5 = Transform_get_rotation_m11483428(L_4, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_rotation_m1525803229(L_3, L_5, /*hidden argument*/NULL);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		GameObject_SendMessage_m2244324713(L_6, _stringLiteral2212779127, /*hidden argument*/NULL);
		CharacterController_t1618060635 * L_7 = __this->get_charController_2();
		NullCheck(L_7);
		Collider_set_enabled_m2575670866(L_7, (bool)1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var);
		((RespawnMenuV2_t3986403171_StaticFields*)RespawnMenuV2_t3986403171_il2cpp_TypeInfo_var->static_fields)->set_playerIsDead_4((bool)0);
		IL2CPP_RUNTIME_CLASS_INIT(Debug_t4195163081_il2cpp_TypeInfo_var);
		Debug_Log_m1731103628(NULL /*static, unused*/, _stringLiteral3319714530, /*hidden argument*/NULL);
		return;
	}
}
// System.Void RespawnMenuV2::Main()
extern "C"  void RespawnMenuV2_Main_m1791691338 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void sandcastleLifes::.ctor()
extern "C"  void sandcastleLifes__ctor_m1835958747 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_enemyhealth_3(((int32_t)100));
		return;
	}
}
// System.Void sandcastleLifes::.cctor()
extern Il2CppClass* sandcastleLifes_t1127312347_il2cpp_TypeInfo_var;
extern const uint32_t sandcastleLifes__cctor_m598050098_MetadataUsageId;
extern "C"  void sandcastleLifes__cctor_m598050098 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (sandcastleLifes__cctor_m598050098_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		((sandcastleLifes_t1127312347_StaticFields*)sandcastleLifes_t1127312347_il2cpp_TypeInfo_var->static_fields)->set_trigger1_2(1);
		return;
	}
}
// System.Void sandcastleLifes::DeductPoints(System.Int32)
extern "C"  void sandcastleLifes_DeductPoints_m892424416 (sandcastleLifes_t1127312347 * __this, int32_t ___hitpoints0, const MethodInfo* method)
{
	{
		int32_t L_0 = __this->get_enemyhealth_3();
		int32_t L_1 = ___hitpoints0;
		__this->set_enemyhealth_3(((int32_t)((int32_t)L_0-(int32_t)L_1)));
		return;
	}
}
// System.Void sandcastleLifes::Update()
extern Il2CppClass* sandcastleLifes_t1127312347_il2cpp_TypeInfo_var;
extern const uint32_t sandcastleLifes_Update_m2807008402_MetadataUsageId;
extern "C"  void sandcastleLifes_Update_m2807008402 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (sandcastleLifes_Update_m2807008402_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = __this->get_enemyhealth_3();
		if ((((int32_t)L_0) > ((int32_t)0)))
		{
			goto IL_001d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(sandcastleLifes_t1127312347_il2cpp_TypeInfo_var);
		((sandcastleLifes_t1127312347_StaticFields*)sandcastleLifes_t1127312347_il2cpp_TypeInfo_var->static_fields)->set_trigger1_2(1);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
	}

IL_001d:
	{
		return;
	}
}
// System.Void sandcastleLifes::Main()
extern "C"  void sandcastleLifes_Main_m528908162 (sandcastleLifes_t1127312347 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ScoreSystem::.ctor()
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern const uint32_t ScoreSystem__ctor_m2873307829_MetadataUsageId;
extern "C"  void ScoreSystem__ctor_m2873307829 (ScoreSystem_t3184889665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreSystem__ctor_m2873307829_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		int32_t L_0 = ((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->get_myScore_2();
		__this->set_money_2(L_0);
		return;
	}
}
// System.Void ScoreSystem::OnTriggerEnter(UnityEngine.Collider)
extern Il2CppClass* String_t_il2cpp_TypeInfo_var;
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2393081601;
extern const uint32_t ScoreSystem_OnTriggerEnter_m2027114723_MetadataUsageId;
extern "C"  void ScoreSystem_OnTriggerEnter_m2027114723 (ScoreSystem_t3184889665 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreSystem_OnTriggerEnter_m2027114723_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		Collider_t2939674232 * L_0 = ___hit0;
		NullCheck(L_0);
		GameObject_t3674682005 * L_1 = Component_get_gameObject_m1170635899(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m211612200(L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m1260523650(NULL /*static, unused*/, L_2, _stringLiteral2393081601, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0033;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		int32_t L_4 = ((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->get_myScore_2();
		((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->set_myScore_2(((int32_t)((int32_t)L_4+(int32_t)1)));
		GameObject_t3674682005 * L_5 = Component_get_gameObject_m1170635899(__this, /*hidden argument*/NULL);
		Object_Destroy_m2260435093(NULL /*static, unused*/, L_5, (((float)((float)0))), /*hidden argument*/NULL);
	}

IL_0033:
	{
		return;
	}
}
// System.Void ScoreSystem::OnGUI()
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern Il2CppClass* Int32_t1153838500_il2cpp_TypeInfo_var;
extern Il2CppClass* RuntimeServices_t3947355960_il2cpp_TypeInfo_var;
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2011262393;
extern const uint32_t ScoreSystem_OnGUI_m2368706479_MetadataUsageId;
extern "C"  void ScoreSystem_OnGUI_m2368706479 (ScoreSystem_t3184889665 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ScoreSystem_OnGUI_m2368706479_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_1;
		memset(&L_1, 0, sizeof(L_1));
		Rect__ctor_m3291325233(&L_1, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))+(int32_t)((int32_t)20)))))), (((float)((float)((int32_t)10)))), (((float)((float)((int32_t)200)))), (((float)((float)((int32_t)30)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		int32_t L_2 = ((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->get_myScore_2();
		int32_t L_3 = L_2;
		Il2CppObject * L_4 = Box(Int32_t1153838500_il2cpp_TypeInfo_var, &L_3);
		IL2CPP_RUNTIME_CLASS_INIT(RuntimeServices_t3947355960_il2cpp_TypeInfo_var);
		String_t* L_5 = RuntimeServices_op_Addition_m3613207904(NULL /*static, unused*/, _stringLiteral2011262393, L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		GUI_Label_m1483857617(NULL /*static, unused*/, L_1, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScoreSystem::Main()
extern "C"  void ScoreSystem_Main_m978013032 (ScoreSystem_t3184889665 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void scorevalue::.ctor()
extern "C"  void scorevalue__ctor_m4124501649 (scorevalue_t1604558367 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		__this->set_DelayTime_3(((int32_t)10));
		return;
	}
}
// System.Void scorevalue::.cctor()
extern "C"  void scorevalue__cctor_m2823403324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void scorevalue::Start()
extern Il2CppClass* scorevalue_t1604558367_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1841789348;
extern const uint32_t scorevalue_Start_m3071639441_MetadataUsageId;
extern "C"  void scorevalue_Start_m3071639441 (scorevalue_t1604558367 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (scorevalue_Start_m3071639441_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(scorevalue_t1604558367_il2cpp_TypeInfo_var);
		((scorevalue_t1604558367_StaticFields*)scorevalue_t1604558367_il2cpp_TypeInfo_var->static_fields)->set_myScore_2(0);
		int32_t L_0 = __this->get_DelayTime_3();
		MonoBehaviour_Invoke_m2825545578(__this, _stringLiteral1841789348, (((float)((float)L_0))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void scorevalue::Main()
extern "C"  void scorevalue_Main_m2126752780 (scorevalue_t1604558367 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void text::.ctor()
extern "C"  void text__ctor_m1431604483 (text_t3556653 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator text::Main()
extern Il2CppClass* U24MainU2458_t32180044_il2cpp_TypeInfo_var;
extern const uint32_t text_Main_m1742654188_MetadataUsageId;
extern "C"  Il2CppObject * text_Main_m1742654188 (text_t3556653 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (text_Main_m1742654188_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2458_t32180044 * L_0 = (U24MainU2458_t32180044 *)il2cpp_codegen_object_new(U24MainU2458_t32180044_il2cpp_TypeInfo_var);
		U24MainU2458__ctor_m3093444157(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2458_GetEnumerator_m864224986(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text(1)::.ctor()
extern "C"  void textU281U29__ctor_m2727829443 (textU281U29_t2877074419 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator text(1)::Main()
extern Il2CppClass* U24MainU2452_t3645169472_il2cpp_TypeInfo_var;
extern const uint32_t textU281U29_Main_m3635832776_MetadataUsageId;
extern "C"  Il2CppObject * textU281U29_Main_m3635832776 (textU281U29_t2877074419 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (textU281U29_Main_m3635832776_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2452_t3645169472 * L_0 = (U24MainU2452_t3645169472 *)il2cpp_codegen_object_new(U24MainU2452_t3645169472_il2cpp_TypeInfo_var);
		U24MainU2452__ctor_m1775250447(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2452_GetEnumerator_m522146288(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text(1)/$Main$52::.ctor(text(1))
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2452__ctor_m1775250447_MetadataUsageId;
extern "C"  void U24MainU2452__ctor_m1775250447 (U24MainU2452_t3645169472 * __this, textU281U29_t2877074419 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2452__ctor_m1775250447_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		textU281U29_t2877074419 * L_0 = ___self_0;
		__this->set_U24self_U2454_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text(1)/$Main$52::GetEnumerator()
extern Il2CppClass* U24_t2609517845_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2452_GetEnumerator_m522146288_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2452_GetEnumerator_m522146288 (U24MainU2452_t3645169472 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2452_GetEnumerator_m522146288_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		textU281U29_t2877074419 * L_0 = __this->get_U24self_U2454_0();
		U24_t2609517845 * L_1 = (U24_t2609517845 *)il2cpp_codegen_object_new(U24_t2609517845_il2cpp_TypeInfo_var);
		U24__ctor_m77688612(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text(1)/$Main$52/$::.ctor(text(1))
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m77688612_MetadataUsageId;
extern "C"  void U24__ctor_m77688612 (U24_t2609517845 * __this, textU281U29_t2877074419 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m77688612_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		textU281U29_t2877074419 * L_0 = ___self_0;
		__this->set_U24self_U2453_2(L_0);
		return;
	}
}
// System.Boolean text(1)/$Main$52/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m3576380523_MetadataUsageId;
extern "C"  bool U24_MoveNext_m3576380523 (U24_t2609517845 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m3576380523_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B6_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001f;
		}
		if (L_0 == 1)
		{
			goto IL_0090;
		}
		if (L_0 == 2)
		{
			goto IL_0032;
		}
		if (L_0 == 3)
		{
			goto IL_0045;
		}
		if (L_0 == 4)
		{
			goto IL_0078;
		}
	}

IL_001f:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)3))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B6_0 = ((int32_t)(L_2));
		goto IL_0091;
	}

IL_0032:
	{
		WaitForSeconds_t1615819279 * L_3 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 3, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B6_0 = ((int32_t)(L_4));
		goto IL_0091;
	}

IL_0045:
	{
		textU281U29_t2877074419 * L_5 = __this->get_U24self_U2453_2();
		NullCheck(L_5);
		Transform_t1659122786 * L_6 = Component_get_transform_m4257140443(L_5, /*hidden argument*/NULL);
		Vector3_t4282066566  L_7;
		memset(&L_7, 0, sizeof(L_7));
		Vector3__ctor_m2926210380(&L_7, (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)120)))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_6);
		Transform_set_position_m3111394108(L_6, L_7, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_8 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_8, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_9 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 4, L_8, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B6_0 = ((int32_t)(L_9));
		goto IL_0091;
	}

IL_0078:
	{
		textU281U29_t2877074419 * L_10 = __this->get_U24self_U2453_2();
		NullCheck(L_10);
		GameObject_t3674682005 * L_11 = Component_get_gameObject_m1170635899(L_10, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0090:
	{
		G_B6_0 = 0;
	}

IL_0091:
	{
		return (bool)G_B6_0;
	}
}
// System.Void text(2)::.ctor()
extern "C"  void textU282U29__ctor_m930878084 (textU282U29_t2877074450 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator text(2)::Main()
extern Il2CppClass* U24MainU2455_t1848218116_il2cpp_TypeInfo_var;
extern const uint32_t textU282U29_Main_m3439319271_MetadataUsageId;
extern "C"  Il2CppObject * textU282U29_Main_m3439319271 (textU282U29_t2877074450 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (textU282U29_Main_m3439319271_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2455_t1848218116 * L_0 = (U24MainU2455_t1848218116 *)il2cpp_codegen_object_new(U24MainU2455_t1848218116_il2cpp_TypeInfo_var);
		U24MainU2455__ctor_m2501827220(L_0, __this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2455_GetEnumerator_m1571159980(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text(2)/$Main$55::.ctor(text(2))
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2455__ctor_m2501827220_MetadataUsageId;
extern "C"  void U24MainU2455__ctor_m2501827220 (U24MainU2455_t1848218116 * __this, textU282U29_t2877074450 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2455__ctor_m2501827220_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		textU282U29_t2877074450 * L_0 = ___self_0;
		__this->set_U24self_U2457_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text(2)/$Main$55::GetEnumerator()
extern Il2CppClass* U24_t2316117721_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2455_GetEnumerator_m1571159980_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2455_GetEnumerator_m1571159980 (U24MainU2455_t1848218116 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2455_GetEnumerator_m1571159980_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		textU282U29_t2877074450 * L_0 = __this->get_U24self_U2457_0();
		U24_t2316117721 * L_1 = (U24_t2316117721 *)il2cpp_codegen_object_new(U24_t2316117721_il2cpp_TypeInfo_var);
		U24__ctor_m2532342953(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text(2)/$Main$55/$::.ctor(text(2))
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m2532342953_MetadataUsageId;
extern "C"  void U24__ctor_m2532342953 (U24_t2316117721 * __this, textU282U29_t2877074450 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2532342953_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		textU282U29_t2877074450 * L_0 = ___self_0;
		__this->set_U24self_U2456_2(L_0);
		return;
	}
}
// System.Boolean text(2)/$Main$55/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m1726859311_MetadataUsageId;
extern "C"  bool U24_MoveNext_m1726859311 (U24_t2316117721 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m1726859311_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B7_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0023;
		}
		if (L_0 == 1)
		{
			goto IL_00aa;
		}
		if (L_0 == 2)
		{
			goto IL_0036;
		}
		if (L_0 == 3)
		{
			goto IL_0049;
		}
		if (L_0 == 4)
		{
			goto IL_005f;
		}
		if (L_0 == 5)
		{
			goto IL_0092;
		}
	}

IL_0023:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)3))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_2));
		goto IL_00ab;
	}

IL_0036:
	{
		WaitForSeconds_t1615819279 * L_3 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 3, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_4));
		goto IL_00ab;
	}

IL_0049:
	{
		WaitForSeconds_t1615819279 * L_5 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_5, (6.3f), /*hidden argument*/NULL);
		bool L_6 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 4, L_5, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_6));
		goto IL_00ab;
	}

IL_005f:
	{
		textU282U29_t2877074450 * L_7 = __this->get_U24self_U2456_2();
		NullCheck(L_7);
		Transform_t1659122786 * L_8 = Component_get_transform_m4257140443(L_7, /*hidden argument*/NULL);
		Vector3_t4282066566  L_9;
		memset(&L_9, 0, sizeof(L_9));
		Vector3__ctor_m2926210380(&L_9, (((float)((float)((int32_t)400)))), (((float)((float)((int32_t)120)))), (((float)((float)0))), /*hidden argument*/NULL);
		NullCheck(L_8);
		Transform_set_position_m3111394108(L_8, L_9, /*hidden argument*/NULL);
		WaitForSeconds_t1615819279 * L_10 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_10, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_11 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 5, L_10, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B7_0 = ((int32_t)(L_11));
		goto IL_00ab;
	}

IL_0092:
	{
		textU282U29_t2877074450 * L_12 = __this->get_U24self_U2456_2();
		NullCheck(L_12);
		GameObject_t3674682005 * L_13 = Component_get_gameObject_m1170635899(L_12, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_00aa:
	{
		G_B7_0 = 0;
	}

IL_00ab:
	{
		return (bool)G_B7_0;
	}
}
// System.Void text/$Main$58::.ctor(text)
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2458__ctor_m3093444157_MetadataUsageId;
extern "C"  void U24MainU2458__ctor_m3093444157 (U24MainU2458_t32180044 * __this, text_t3556653 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2458__ctor_m3093444157_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		text_t3556653 * L_0 = ___self_0;
		__this->set_U24self_U2460_0(L_0);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text/$Main$58::GetEnumerator()
extern Il2CppClass* U24_t860252705_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2458_GetEnumerator_m864224986_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2458_GetEnumerator_m864224986 (U24MainU2458_t32180044 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2458_GetEnumerator_m864224986_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		text_t3556653 * L_0 = __this->get_U24self_U2460_0();
		U24_t860252705 * L_1 = (U24_t860252705 *)il2cpp_codegen_object_new(U24_t860252705_il2cpp_TypeInfo_var);
		U24__ctor_m3519126728(L_1, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void text/$Main$58/$::.ctor(text)
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m3519126728_MetadataUsageId;
extern "C"  void U24__ctor_m3519126728 (U24_t860252705 * __this, text_t3556653 * ___self_0, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m3519126728_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		text_t3556653 * L_0 = ___self_0;
		__this->set_U24self_U2459_2(L_0);
		return;
	}
}
// System.Boolean text/$Main$58/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m365369077_MetadataUsageId;
extern "C"  bool U24_MoveNext_m365369077 (U24_t860252705 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m365369077_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B5_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_001b;
		}
		if (L_0 == 1)
		{
			goto IL_0059;
		}
		if (L_0 == 2)
		{
			goto IL_002e;
		}
		if (L_0 == 3)
		{
			goto IL_0041;
		}
	}

IL_001b:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)3))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_2));
		goto IL_005a;
	}

IL_002e:
	{
		WaitForSeconds_t1615819279 * L_3 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_3, (((float)((float)6))), /*hidden argument*/NULL);
		bool L_4 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 3, L_3, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B5_0 = ((int32_t)(L_4));
		goto IL_005a;
	}

IL_0041:
	{
		text_t3556653 * L_5 = __this->get_U24self_U2459_2();
		NullCheck(L_5);
		GameObject_t3674682005 * L_6 = Component_get_gameObject_m1170635899(L_5, /*hidden argument*/NULL);
		Object_Destroy_m176400816(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0059:
	{
		G_B5_0 = 0;
	}

IL_005a:
	{
		return (bool)G_B5_0;
	}
}
// System.Void til hovudlevel::.ctor()
extern "C"  void tilU20hovudlevel__ctor_m1682576865 (tilU20hovudlevel_t1326528079 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void til hovudlevel::Main()
extern "C"  void tilU20hovudlevel_Main_m1216697020 (tilU20hovudlevel_t1326528079 * __this, const MethodInfo* method)
{
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void title buttons::.ctor()
extern "C"  void titleU20buttons__ctor_m215423613 (titleU20buttons_t1264625081 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void title buttons::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral66921822;
extern Il2CppCodeGenString* _stringLiteral1090026127;
extern const uint32_t titleU20buttons_OnGUI_m4005789559_MetadataUsageId;
extern "C"  void titleU20buttons_OnGUI_m4005789559 (titleU20buttons_t1264625081 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (titleU20buttons_OnGUI_m4005789559_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))-(int32_t)((int32_t)50)))))), (((float)((float)((int32_t)((int32_t)L_1/(int32_t)2))))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)60)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_3 = GUI_Button_m885093907(NULL /*static, unused*/, L_2, _stringLiteral66921822, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0036:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)50)))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_5/(int32_t)2))+(int32_t)((int32_t)65)))))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)62)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Button_m885093907(NULL /*static, unused*/, L_6, _stringLiteral1090026127, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006e;
		}
	}
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void title buttons::Main()
extern "C"  void titleU20buttons_Main_m1446464160 (titleU20buttons_t1264625081 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void TitleScreenButtons::.ctor()
extern "C"  void TitleScreenButtons__ctor_m1889281875 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void TitleScreenButtons::OnGUI()
extern Il2CppClass* GUI_t3134605553_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral80089277;
extern Il2CppCodeGenString* _stringLiteral1653741144;
extern const uint32_t TitleScreenButtons_OnGUI_m1384680525_MetadataUsageId;
extern "C"  void TitleScreenButtons_OnGUI_m1384680525 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (TitleScreenButtons_OnGUI_m1384680525_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_1 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_2;
		memset(&L_2, 0, sizeof(L_2));
		Rect__ctor_m3291325233(&L_2, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_0/(int32_t)2))-(int32_t)((int32_t)50)))))), (((float)((float)((int32_t)((int32_t)L_1/(int32_t)2))))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)60)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_3 = GUI_Button_m885093907(NULL /*static, unused*/, L_2, _stringLiteral80089277, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0036;
		}
	}
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 1, /*hidden argument*/NULL);
	}

IL_0036:
	{
		int32_t L_4 = Screen_get_width_m3080333084(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_5 = Screen_get_height_m1504859443(NULL /*static, unused*/, /*hidden argument*/NULL);
		Rect_t4241904616  L_6;
		memset(&L_6, 0, sizeof(L_6));
		Rect__ctor_m3291325233(&L_6, (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_4/(int32_t)2))-(int32_t)((int32_t)50)))))), (((float)((float)((int32_t)((int32_t)((int32_t)((int32_t)L_5/(int32_t)2))+(int32_t)((int32_t)65)))))), (((float)((float)((int32_t)150)))), (((float)((float)((int32_t)62)))), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t3134605553_il2cpp_TypeInfo_var);
		bool L_7 = GUI_Button_m885093907(NULL /*static, unused*/, L_6, _stringLiteral1653741144, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_006e;
		}
	}
	{
		Application_Quit_m1187862186(NULL /*static, unused*/, /*hidden argument*/NULL);
	}

IL_006e:
	{
		return;
	}
}
// System.Void TitleScreenButtons::Main()
extern "C"  void TitleScreenButtons_Main_m3855764234 (TitleScreenButtons_t1628429853 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void ToPoteland::.ctor()
extern "C"  void ToPoteland__ctor_m72997946 (ToPoteland_t2745890390 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m2022291967(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator ToPoteland::Main()
extern Il2CppClass* U24MainU2444_t3891303456_il2cpp_TypeInfo_var;
extern const uint32_t ToPoteland_Main_m205833045_MetadataUsageId;
extern "C"  Il2CppObject * ToPoteland_Main_m205833045 (ToPoteland_t2745890390 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (ToPoteland_Main_m205833045_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24MainU2444_t3891303456 * L_0 = (U24MainU2444_t3891303456 *)il2cpp_codegen_object_new(U24MainU2444_t3891303456_il2cpp_TypeInfo_var);
		U24MainU2444__ctor_m1372992950(L_0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Il2CppObject* L_1 = U24MainU2444_GetEnumerator_m2259384390(L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void ToPoteland/$Main$44::.ctor()
extern const MethodInfo* GenericGenerator_1__ctor_m1086344373_MethodInfo_var;
extern const uint32_t U24MainU2444__ctor_m1372992950_MetadataUsageId;
extern "C"  void U24MainU2444__ctor_m1372992950 (U24MainU2444_t3891303456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2444__ctor_m1372992950_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGenerator_1__ctor_m1086344373(__this, /*hidden argument*/GenericGenerator_1__ctor_m1086344373_MethodInfo_var);
		return;
	}
}
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> ToPoteland/$Main$44::GetEnumerator()
extern Il2CppClass* U24_t2921075189_il2cpp_TypeInfo_var;
extern const uint32_t U24MainU2444_GetEnumerator_m2259384390_MetadataUsageId;
extern "C"  Il2CppObject* U24MainU2444_GetEnumerator_m2259384390 (U24MainU2444_t3891303456 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24MainU2444_GetEnumerator_m2259384390_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		U24_t2921075189 * L_0 = (U24_t2921075189 *)il2cpp_codegen_object_new(U24_t2921075189_il2cpp_TypeInfo_var);
		U24__ctor_m2515562177(L_0, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void ToPoteland/$Main$44/$::.ctor()
extern const MethodInfo* GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var;
extern const uint32_t U24__ctor_m2515562177_MetadataUsageId;
extern "C"  void U24__ctor_m2515562177 (U24_t2921075189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24__ctor_m2515562177_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	{
		GenericGeneratorEnumerator_1__ctor_m3634585745(__this, /*hidden argument*/GenericGeneratorEnumerator_1__ctor_m3634585745_MethodInfo_var);
		return;
	}
}
// System.Boolean ToPoteland/$Main$44/$::MoveNext()
extern Il2CppClass* WaitForSeconds_t1615819279_il2cpp_TypeInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var;
extern const MethodInfo* GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var;
extern const uint32_t U24_MoveNext_m615627145_MetadataUsageId;
extern "C"  bool U24_MoveNext_m615627145 (U24_t2921075189 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		il2cpp_codegen_initialize_method (U24_MoveNext_m615627145_MetadataUsageId);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B4_0 = 0;
	{
		int32_t L_0 = ((GenericGeneratorEnumerator_1_t1807546943 *)__this)->get__state_1();
		if (L_0 == 0)
		{
			goto IL_0017;
		}
		if (L_0 == 1)
		{
			goto IL_0039;
		}
		if (L_0 == 2)
		{
			goto IL_002b;
		}
	}

IL_0017:
	{
		WaitForSeconds_t1615819279 * L_1 = (WaitForSeconds_t1615819279 *)il2cpp_codegen_object_new(WaitForSeconds_t1615819279_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m3184996201(L_1, (((float)((float)((int32_t)11)))), /*hidden argument*/NULL);
		bool L_2 = GenericGeneratorEnumerator_1_Yield_m4040456012(__this, 2, L_1, /*hidden argument*/GenericGeneratorEnumerator_1_Yield_m4040456012_MethodInfo_var);
		G_B4_0 = ((int32_t)(L_2));
		goto IL_003a;
	}

IL_002b:
	{
		Application_LoadLevel_m1181471414(NULL /*static, unused*/, 7, /*hidden argument*/NULL);
		GenericGeneratorEnumerator_1_YieldDefault_m289716250(__this, 1, /*hidden argument*/GenericGeneratorEnumerator_1_YieldDefault_m289716250_MethodInfo_var);
	}

IL_0039:
	{
		G_B4_0 = 0;
	}

IL_003a:
	{
		return (bool)G_B4_0;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
