﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Peptalk002
struct Peptalk002_t3966958763;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Peptalk002/$Main$41/$
struct  U24_t1345333565  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// Peptalk002 Peptalk002/$Main$41/$::$self_$42
	Peptalk002_t3966958763 * ___U24self_U2442_2;

public:
	inline static int32_t get_offset_of_U24self_U2442_2() { return static_cast<int32_t>(offsetof(U24_t1345333565, ___U24self_U2442_2)); }
	inline Peptalk002_t3966958763 * get_U24self_U2442_2() const { return ___U24self_U2442_2; }
	inline Peptalk002_t3966958763 ** get_address_of_U24self_U2442_2() { return &___U24self_U2442_2; }
	inline void set_U24self_U2442_2(Peptalk002_t3966958763 * value)
	{
		___U24self_U2442_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2442_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
