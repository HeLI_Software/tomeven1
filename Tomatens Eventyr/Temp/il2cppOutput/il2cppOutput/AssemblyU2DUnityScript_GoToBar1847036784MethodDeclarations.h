﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToBar
struct GoToBar_t1847036784;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToBar::.ctor()
extern "C"  void GoToBar__ctor_m1856216294 (GoToBar_t1847036784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GoToBar::Main()
extern "C"  Il2CppObject * GoToBar_Main_m1668053573 (GoToBar_t1847036784 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
