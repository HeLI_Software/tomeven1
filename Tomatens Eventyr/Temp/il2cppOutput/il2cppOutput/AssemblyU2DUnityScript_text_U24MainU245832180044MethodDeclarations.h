﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text/$Main$58
struct U24MainU2458_t32180044;
// text
struct text_t3556653;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_text3556653.h"

// System.Void text/$Main$58::.ctor(text)
extern "C"  void U24MainU2458__ctor_m3093444157 (U24MainU2458_t32180044 * __this, text_t3556653 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text/$Main$58::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2458_GetEnumerator_m864224986 (U24MainU2458_t32180044 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
