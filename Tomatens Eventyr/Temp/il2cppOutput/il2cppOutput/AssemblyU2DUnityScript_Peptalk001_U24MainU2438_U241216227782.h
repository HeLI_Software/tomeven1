﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Peptalk001
struct Peptalk001_t3966958762;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen1807546943.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Peptalk001/$Main$38/$
struct  U24_t1216227782  : public GenericGeneratorEnumerator_1_t1807546943
{
public:
	// Peptalk001 Peptalk001/$Main$38/$::$self_$39
	Peptalk001_t3966958762 * ___U24self_U2439_2;

public:
	inline static int32_t get_offset_of_U24self_U2439_2() { return static_cast<int32_t>(offsetof(U24_t1216227782, ___U24self_U2439_2)); }
	inline Peptalk001_t3966958762 * get_U24self_U2439_2() const { return ___U24self_U2439_2; }
	inline Peptalk001_t3966958762 ** get_address_of_U24self_U2439_2() { return &___U24self_U2439_2; }
	inline void set_U24self_U2439_2(Peptalk001_t3966958762 * value)
	{
		___U24self_U2439_2 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2439_2, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
