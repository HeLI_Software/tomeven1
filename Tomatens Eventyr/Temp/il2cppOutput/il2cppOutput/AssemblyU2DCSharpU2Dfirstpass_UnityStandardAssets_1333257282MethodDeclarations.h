﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Water.WaterBase
struct WaterBase_t1333257282;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.Camera
struct Camera_t2727095145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Transform1659122786.h"
#include "UnityEngine_UnityEngine_Camera2727095145.h"

// System.Void UnityStandardAssets.Water.WaterBase::.ctor()
extern "C"  void WaterBase__ctor_m3514809247 (WaterBase_t1333257282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::UpdateShader()
extern "C"  void WaterBase_UpdateShader_m2829669459 (WaterBase_t1333257282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::WaterTileBeingRendered(UnityEngine.Transform,UnityEngine.Camera)
extern "C"  void WaterBase_WaterTileBeingRendered_m4064990337 (WaterBase_t1333257282 * __this, Transform_t1659122786 * ___tr0, Camera_t2727095145 * ___currentCam1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.WaterBase::Update()
extern "C"  void WaterBase_Update_m3311766350 (WaterBase_t1333257282 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
