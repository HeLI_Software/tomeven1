﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t3792884695;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.MeshRenderer
struct MeshRenderer_t2804666580;
// UnityEngine.NavMeshAgent
struct NavMeshAgent_t588466745;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// testai
struct  testai_t3417797690  : public MonoBehaviour_t667441552
{
public:
	// System.Single testai::searchingTurnSpeed
	float ___searchingTurnSpeed_2;
	// System.Single testai::searchingDuration
	float ___searchingDuration_3;
	// System.Single testai::sightRange
	float ___sightRange_4;
	// UnityEngine.Transform[] testai::wayPoints
	TransformU5BU5D_t3792884695* ___wayPoints_5;
	// UnityEngine.Transform testai::eyes
	Transform_t1659122786 * ___eyes_6;
	// UnityEngine.Vector3 testai::offset
	Vector3_t4282066566  ___offset_7;
	// UnityEngine.MeshRenderer testai::meshRendererFlag
	MeshRenderer_t2804666580 * ___meshRendererFlag_8;
	// UnityEngine.Transform testai::chaseTarget
	Transform_t1659122786 * ___chaseTarget_9;
	// UnityEngine.NavMeshAgent testai::navMeshAgent
	NavMeshAgent_t588466745 * ___navMeshAgent_10;

public:
	inline static int32_t get_offset_of_searchingTurnSpeed_2() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___searchingTurnSpeed_2)); }
	inline float get_searchingTurnSpeed_2() const { return ___searchingTurnSpeed_2; }
	inline float* get_address_of_searchingTurnSpeed_2() { return &___searchingTurnSpeed_2; }
	inline void set_searchingTurnSpeed_2(float value)
	{
		___searchingTurnSpeed_2 = value;
	}

	inline static int32_t get_offset_of_searchingDuration_3() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___searchingDuration_3)); }
	inline float get_searchingDuration_3() const { return ___searchingDuration_3; }
	inline float* get_address_of_searchingDuration_3() { return &___searchingDuration_3; }
	inline void set_searchingDuration_3(float value)
	{
		___searchingDuration_3 = value;
	}

	inline static int32_t get_offset_of_sightRange_4() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___sightRange_4)); }
	inline float get_sightRange_4() const { return ___sightRange_4; }
	inline float* get_address_of_sightRange_4() { return &___sightRange_4; }
	inline void set_sightRange_4(float value)
	{
		___sightRange_4 = value;
	}

	inline static int32_t get_offset_of_wayPoints_5() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___wayPoints_5)); }
	inline TransformU5BU5D_t3792884695* get_wayPoints_5() const { return ___wayPoints_5; }
	inline TransformU5BU5D_t3792884695** get_address_of_wayPoints_5() { return &___wayPoints_5; }
	inline void set_wayPoints_5(TransformU5BU5D_t3792884695* value)
	{
		___wayPoints_5 = value;
		Il2CppCodeGenWriteBarrier(&___wayPoints_5, value);
	}

	inline static int32_t get_offset_of_eyes_6() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___eyes_6)); }
	inline Transform_t1659122786 * get_eyes_6() const { return ___eyes_6; }
	inline Transform_t1659122786 ** get_address_of_eyes_6() { return &___eyes_6; }
	inline void set_eyes_6(Transform_t1659122786 * value)
	{
		___eyes_6 = value;
		Il2CppCodeGenWriteBarrier(&___eyes_6, value);
	}

	inline static int32_t get_offset_of_offset_7() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___offset_7)); }
	inline Vector3_t4282066566  get_offset_7() const { return ___offset_7; }
	inline Vector3_t4282066566 * get_address_of_offset_7() { return &___offset_7; }
	inline void set_offset_7(Vector3_t4282066566  value)
	{
		___offset_7 = value;
	}

	inline static int32_t get_offset_of_meshRendererFlag_8() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___meshRendererFlag_8)); }
	inline MeshRenderer_t2804666580 * get_meshRendererFlag_8() const { return ___meshRendererFlag_8; }
	inline MeshRenderer_t2804666580 ** get_address_of_meshRendererFlag_8() { return &___meshRendererFlag_8; }
	inline void set_meshRendererFlag_8(MeshRenderer_t2804666580 * value)
	{
		___meshRendererFlag_8 = value;
		Il2CppCodeGenWriteBarrier(&___meshRendererFlag_8, value);
	}

	inline static int32_t get_offset_of_chaseTarget_9() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___chaseTarget_9)); }
	inline Transform_t1659122786 * get_chaseTarget_9() const { return ___chaseTarget_9; }
	inline Transform_t1659122786 ** get_address_of_chaseTarget_9() { return &___chaseTarget_9; }
	inline void set_chaseTarget_9(Transform_t1659122786 * value)
	{
		___chaseTarget_9 = value;
		Il2CppCodeGenWriteBarrier(&___chaseTarget_9, value);
	}

	inline static int32_t get_offset_of_navMeshAgent_10() { return static_cast<int32_t>(offsetof(testai_t3417797690, ___navMeshAgent_10)); }
	inline NavMeshAgent_t588466745 * get_navMeshAgent_10() const { return ___navMeshAgent_10; }
	inline NavMeshAgent_t588466745 ** get_address_of_navMeshAgent_10() { return &___navMeshAgent_10; }
	inline void set_navMeshAgent_10(NavMeshAgent_t588466745 * value)
	{
		___navMeshAgent_10 = value;
		Il2CppCodeGenWriteBarrier(&___navMeshAgent_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
