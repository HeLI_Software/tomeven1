﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PixelMapTerrain
struct PixelMapTerrain_t2288975167;
// UnityEngine.Texture2D
struct Texture2D_t3884108195;
// Dimensions
struct Dimensions_t2407799789;
// UnityEngine.Material[]
struct MaterialU5BU5D_t170856778;
// System.Single[]
struct SingleU5BU5D_t2316563989;
// System.Int32[]
struct Int32U5BU5D_t3230847821;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Texture2D3884108195.h"
#include "AssemblyU2DCSharp_Dimensions2407799789.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void PixelMapTerrain::.ctor()
extern "C"  void PixelMapTerrain__ctor_m2555920652 (PixelMapTerrain_t2288975167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PixelMapTerrain::GenerateTerrain(UnityEngine.Texture2D,Dimensions,UnityEngine.Material[])
extern "C"  void PixelMapTerrain_GenerateTerrain_m2171995231 (PixelMapTerrain_t2288975167 * __this, Texture2D_t3884108195 * ___heightmap0, Dimensions_t2407799789 * ___dimensions1, MaterialU5BU5D_t170856778* ___mat2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single PixelMapTerrain::ColorToFloat(UnityEngine.Color)
extern "C"  float PixelMapTerrain_ColorToFloat_m1149907388 (PixelMapTerrain_t2288975167 * __this, Color_t4194546905  ___color0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] PixelMapTerrain::HeightFromImage(UnityEngine.Texture2D,System.Single)
extern "C"  SingleU5BU5D_t2316563989* PixelMapTerrain_HeightFromImage_m1847910979 (PixelMapTerrain_t2288975167 * __this, Texture2D_t3884108195 * ___img0, float ___heightModifier1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PixelMapTerrain::OnDestroy()
extern "C"  void PixelMapTerrain_OnDestroy_m3502944261 (PixelMapTerrain_t2288975167 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32[] PixelMapTerrain::BlockifyHeights(System.Single[],System.Int32,System.Int32)
extern "C"  Int32U5BU5D_t3230847821* PixelMapTerrain_BlockifyHeights_m613137302 (PixelMapTerrain_t2288975167 * __this, SingleU5BU5D_t2316563989* ___heights0, int32_t ___blockSize1, int32_t ___mapHeight2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single[] PixelMapTerrain::CubeHeights(System.Int32[])
extern "C"  SingleU5BU5D_t2316563989* PixelMapTerrain_CubeHeights_m2893317114 (PixelMapTerrain_t2288975167 * __this, Int32U5BU5D_t3230847821* ___heights0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PixelMapTerrain::PlaneNormal(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  PixelMapTerrain_PlaneNormal_m3954937112 (PixelMapTerrain_t2288975167 * __this, Vector3_t4282066566  ___p00, Vector3_t4282066566  ___p11, Vector3_t4282066566  ___p22, const MethodInfo* method) IL2CPP_METHOD_ATTR;
