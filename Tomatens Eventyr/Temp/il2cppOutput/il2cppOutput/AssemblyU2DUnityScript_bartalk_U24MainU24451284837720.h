﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// bartalk
struct bartalk_t3961876287;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// bartalk/$Main$45
struct  U24MainU2445_t1284837720  : public GenericGenerator_1_t207610107
{
public:
	// bartalk bartalk/$Main$45::$self_$47
	bartalk_t3961876287 * ___U24self_U2447_0;

public:
	inline static int32_t get_offset_of_U24self_U2447_0() { return static_cast<int32_t>(offsetof(U24MainU2445_t1284837720, ___U24self_U2447_0)); }
	inline bartalk_t3961876287 * get_U24self_U2447_0() const { return ___U24self_U2447_0; }
	inline bartalk_t3961876287 ** get_address_of_U24self_U2447_0() { return &___U24self_U2447_0; }
	inline void set_U24self_U2447_0(bartalk_t3961876287 * value)
	{
		___U24self_U2447_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2447_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
