﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToBar1
struct GoToBar1_t1423565505;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToBar1::.ctor()
extern "C"  void GoToBar1__ctor_m2640488303 (GoToBar1_t1423565505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GoToBar1::Main()
extern "C"  void GoToBar1_Main_m2355976046 (GoToBar1_t1423565505 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
