﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GoToBar/$Main$37/$
struct U24_t2507849599;

#include "codegen/il2cpp-codegen.h"

// System.Void GoToBar/$Main$37/$::.ctor()
extern "C"  void U24__ctor_m2751181873 (U24_t2507849599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GoToBar/$Main$37/$::MoveNext()
extern "C"  bool U24_MoveNext_m732796885 (U24_t2507849599 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
