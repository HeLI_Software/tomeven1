﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Dimensions
struct Dimensions_t2407799789;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Dimensions::.ctor()
extern "C"  void Dimensions__ctor_m666081102 (Dimensions_t2407799789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String Dimensions::ToString()
extern "C"  String_t* Dimensions_ToString_m481552133 (Dimensions_t2407799789 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
