﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Portal/$OnTriggerEnter$48
struct U24OnTriggerEnterU2448_t458821736;
// UnityEngine.Collider
struct Collider_t2939674232;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Portal/$OnTriggerEnter$48::.ctor(UnityEngine.Collider)
extern "C"  void U24OnTriggerEnterU2448__ctor_m1882592995 (U24OnTriggerEnterU2448_t458821736 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Portal/$OnTriggerEnter$48::GetEnumerator()
extern "C"  Il2CppObject* U24OnTriggerEnterU2448_GetEnumerator_m427524286 (U24OnTriggerEnterU2448_t458821736 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
