﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk002/$Main$41
struct U24MainU2441_t3576814952;
// Peptalk002
struct Peptalk002_t3966958763;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Peptalk0023966958763.h"

// System.Void Peptalk002/$Main$41::.ctor(Peptalk002)
extern "C"  void U24MainU2441__ctor_m671048675 (U24MainU2441_t3576814952 * __this, Peptalk002_t3966958763 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Peptalk002/$Main$41::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2441_GetEnumerator_m2248992766 (U24MainU2441_t3576814952 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
