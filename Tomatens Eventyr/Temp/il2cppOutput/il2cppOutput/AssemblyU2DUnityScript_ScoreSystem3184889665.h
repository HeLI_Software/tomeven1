﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// ScoreSystem
struct  ScoreSystem_t3184889665  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 ScoreSystem::money
	int32_t ___money_2;

public:
	inline static int32_t get_offset_of_money_2() { return static_cast<int32_t>(offsetof(ScoreSystem_t3184889665, ___money_2)); }
	inline int32_t get_money_2() const { return ___money_2; }
	inline int32_t* get_address_of_money_2() { return &___money_2; }
	inline void set_money_2(int32_t value)
	{
		___money_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
