﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.CharacterController
struct CharacterController_t1618060635;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// RespawnMenuV2
struct  RespawnMenuV2_t3986403171  : public MonoBehaviour_t667441552
{
public:
	// UnityEngine.CharacterController RespawnMenuV2::charController
	CharacterController_t1618060635 * ___charController_2;
	// UnityEngine.Transform RespawnMenuV2::respawnTransform
	Transform_t1659122786 * ___respawnTransform_3;

public:
	inline static int32_t get_offset_of_charController_2() { return static_cast<int32_t>(offsetof(RespawnMenuV2_t3986403171, ___charController_2)); }
	inline CharacterController_t1618060635 * get_charController_2() const { return ___charController_2; }
	inline CharacterController_t1618060635 ** get_address_of_charController_2() { return &___charController_2; }
	inline void set_charController_2(CharacterController_t1618060635 * value)
	{
		___charController_2 = value;
		Il2CppCodeGenWriteBarrier(&___charController_2, value);
	}

	inline static int32_t get_offset_of_respawnTransform_3() { return static_cast<int32_t>(offsetof(RespawnMenuV2_t3986403171, ___respawnTransform_3)); }
	inline Transform_t1659122786 * get_respawnTransform_3() const { return ___respawnTransform_3; }
	inline Transform_t1659122786 ** get_address_of_respawnTransform_3() { return &___respawnTransform_3; }
	inline void set_respawnTransform_3(Transform_t1659122786 * value)
	{
		___respawnTransform_3 = value;
		Il2CppCodeGenWriteBarrier(&___respawnTransform_3, value);
	}
};

struct RespawnMenuV2_t3986403171_StaticFields
{
public:
	// System.Boolean RespawnMenuV2::playerIsDead
	bool ___playerIsDead_4;

public:
	inline static int32_t get_offset_of_playerIsDead_4() { return static_cast<int32_t>(offsetof(RespawnMenuV2_t3986403171_StaticFields, ___playerIsDead_4)); }
	inline bool get_playerIsDead_4() const { return ___playerIsDead_4; }
	inline bool* get_address_of_playerIsDead_4() { return &___playerIsDead_4; }
	inline void set_playerIsDead_4(bool value)
	{
		___playerIsDead_4 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
