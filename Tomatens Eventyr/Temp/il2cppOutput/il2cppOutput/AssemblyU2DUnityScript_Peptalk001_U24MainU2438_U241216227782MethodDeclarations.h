﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk001/$Main$38/$
struct U24_t1216227782;
// Peptalk001
struct Peptalk001_t3966958762;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Peptalk0013966958762.h"

// System.Void Peptalk001/$Main$38/$::.ctor(Peptalk001)
extern "C"  void U24__ctor_m339097062 (U24_t1216227782 * __this, Peptalk001_t3966958762 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Peptalk001/$Main$38/$::MoveNext()
extern "C"  bool U24_MoveNext_m3131154394 (U24_t1216227782 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
