﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Material
struct Material_t3870600107;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_FogMode3255732919.h"
#include "UnityEngine_UnityEngine_Material3870600107.h"

// UnityEngine.FogMode UnityEngine.RenderSettings::get_fogMode()
extern "C"  int32_t RenderSettings_get_fogMode_m1271225857 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogDensity()
extern "C"  float RenderSettings_get_fogDensity_m2444985551 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogStartDistance()
extern "C"  float RenderSettings_get_fogStartDistance_m3752408606 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.RenderSettings::get_fogEndDistance()
extern "C"  float RenderSettings_get_fogEndDistance_m167008471 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Material UnityEngine.RenderSettings::get_skybox()
extern "C"  Material_t3870600107 * RenderSettings_get_skybox_m505912468 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.RenderSettings::set_skybox(UnityEngine.Material)
extern "C"  void RenderSettings_set_skybox_m3777670233 (Il2CppObject * __this /* static, unused */, Material_t3870600107 * ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
