﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// text(1)
struct textU281U29_t2877074419;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// text(1)/$Main$52
struct  U24MainU2452_t3645169472  : public GenericGenerator_1_t207610107
{
public:
	// text(1) text(1)/$Main$52::$self_$54
	textU281U29_t2877074419 * ___U24self_U2454_0;

public:
	inline static int32_t get_offset_of_U24self_U2454_0() { return static_cast<int32_t>(offsetof(U24MainU2452_t3645169472, ___U24self_U2454_0)); }
	inline textU281U29_t2877074419 * get_U24self_U2454_0() const { return ___U24self_U2454_0; }
	inline textU281U29_t2877074419 ** get_address_of_U24self_U2454_0() { return &___U24self_U2454_0; }
	inline void set_U24self_U2454_0(textU281U29_t2877074419 * value)
	{
		___U24self_U2454_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2454_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
