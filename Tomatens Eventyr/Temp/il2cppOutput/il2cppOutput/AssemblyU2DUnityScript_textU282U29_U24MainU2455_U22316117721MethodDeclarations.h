﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(2)/$Main$55/$
struct U24_t2316117721;
// text(2)
struct textU282U29_t2877074450;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_textU282U292877074450.h"

// System.Void text(2)/$Main$55/$::.ctor(text(2))
extern "C"  void U24__ctor_m2532342953 (U24_t2316117721 * __this, textU282U29_t2877074450 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean text(2)/$Main$55/$::MoveNext()
extern "C"  bool U24_MoveNext_m1726859311 (U24_t2316117721 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
