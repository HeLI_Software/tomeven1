﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// RespawnMenuV2
struct RespawnMenuV2_t3986403171;

#include "codegen/il2cpp-codegen.h"

// System.Void RespawnMenuV2::.ctor()
extern "C"  void RespawnMenuV2__ctor_m2327531539 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::.cctor()
extern "C"  void RespawnMenuV2__cctor_m2951904762 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::Start()
extern "C"  void RespawnMenuV2_Start_m1274669331 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::Update()
extern "C"  void RespawnMenuV2_Update_m865895770 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::OnGUI()
extern "C"  void RespawnMenuV2_OnGUI_m1822930189 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::RespawnPlayer()
extern "C"  void RespawnMenuV2_RespawnPlayer_m1829664026 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void RespawnMenuV2::Main()
extern "C"  void RespawnMenuV2_Main_m1791691338 (RespawnMenuV2_t3986403171 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
