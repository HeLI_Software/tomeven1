﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(2)/$Main$55
struct U24MainU2455_t1848218116;
// text(2)
struct textU282U29_t2877074450;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_textU282U292877074450.h"

// System.Void text(2)/$Main$55::.ctor(text(2))
extern "C"  void U24MainU2455__ctor_m2501827220 (U24MainU2455_t1848218116 * __this, textU282U29_t2877074450 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> text(2)/$Main$55::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2455_GetEnumerator_m1571159980 (U24MainU2455_t1848218116 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
