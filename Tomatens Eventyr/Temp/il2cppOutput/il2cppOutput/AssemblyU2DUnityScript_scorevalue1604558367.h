﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// scorevalue
struct  scorevalue_t1604558367  : public MonoBehaviour_t667441552
{
public:
	// System.Int32 scorevalue::DelayTime
	int32_t ___DelayTime_3;

public:
	inline static int32_t get_offset_of_DelayTime_3() { return static_cast<int32_t>(offsetof(scorevalue_t1604558367, ___DelayTime_3)); }
	inline int32_t get_DelayTime_3() const { return ___DelayTime_3; }
	inline int32_t* get_address_of_DelayTime_3() { return &___DelayTime_3; }
	inline void set_DelayTime_3(int32_t value)
	{
		___DelayTime_3 = value;
	}
};

struct scorevalue_t1604558367_StaticFields
{
public:
	// System.Int32 scorevalue::myScore
	int32_t ___myScore_2;

public:
	inline static int32_t get_offset_of_myScore_2() { return static_cast<int32_t>(offsetof(scorevalue_t1604558367_StaticFields, ___myScore_2)); }
	inline int32_t get_myScore_2() const { return ___myScore_2; }
	inline int32_t* get_address_of_myScore_2() { return &___myScore_2; }
	inline void set_myScore_2(int32_t value)
	{
		___myScore_2 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
