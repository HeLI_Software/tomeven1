﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadNewScene/$HideScene$28/$
struct  U24_t1905319356  : public GenericGeneratorEnumerator_1_t67576739
{
public:
	// System.Single LoadNewScene/$HideScene$28/$::$$9$29
	float ___U24U249U2429_2;
	// UnityEngine.Color LoadNewScene/$HideScene$28/$::$$10$30
	Color_t4194546905  ___U24U2410U2430_3;
	// System.Single LoadNewScene/$HideScene$28/$::$$11$31
	float ___U24U2411U2431_4;
	// UnityEngine.Color LoadNewScene/$HideScene$28/$::$$12$32
	Color_t4194546905  ___U24U2412U2432_5;
	// System.Single LoadNewScene/$HideScene$28/$::$$13$33
	float ___U24U2413U2433_6;
	// UnityEngine.Color LoadNewScene/$HideScene$28/$::$$14$34
	Color_t4194546905  ___U24U2414U2434_7;
	// System.Single LoadNewScene/$HideScene$28/$::$$15$35
	float ___U24U2415U2435_8;
	// UnityEngine.Color LoadNewScene/$HideScene$28/$::$$16$36
	Color_t4194546905  ___U24U2416U2436_9;

public:
	inline static int32_t get_offset_of_U24U249U2429_2() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U249U2429_2)); }
	inline float get_U24U249U2429_2() const { return ___U24U249U2429_2; }
	inline float* get_address_of_U24U249U2429_2() { return &___U24U249U2429_2; }
	inline void set_U24U249U2429_2(float value)
	{
		___U24U249U2429_2 = value;
	}

	inline static int32_t get_offset_of_U24U2410U2430_3() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2410U2430_3)); }
	inline Color_t4194546905  get_U24U2410U2430_3() const { return ___U24U2410U2430_3; }
	inline Color_t4194546905 * get_address_of_U24U2410U2430_3() { return &___U24U2410U2430_3; }
	inline void set_U24U2410U2430_3(Color_t4194546905  value)
	{
		___U24U2410U2430_3 = value;
	}

	inline static int32_t get_offset_of_U24U2411U2431_4() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2411U2431_4)); }
	inline float get_U24U2411U2431_4() const { return ___U24U2411U2431_4; }
	inline float* get_address_of_U24U2411U2431_4() { return &___U24U2411U2431_4; }
	inline void set_U24U2411U2431_4(float value)
	{
		___U24U2411U2431_4 = value;
	}

	inline static int32_t get_offset_of_U24U2412U2432_5() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2412U2432_5)); }
	inline Color_t4194546905  get_U24U2412U2432_5() const { return ___U24U2412U2432_5; }
	inline Color_t4194546905 * get_address_of_U24U2412U2432_5() { return &___U24U2412U2432_5; }
	inline void set_U24U2412U2432_5(Color_t4194546905  value)
	{
		___U24U2412U2432_5 = value;
	}

	inline static int32_t get_offset_of_U24U2413U2433_6() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2413U2433_6)); }
	inline float get_U24U2413U2433_6() const { return ___U24U2413U2433_6; }
	inline float* get_address_of_U24U2413U2433_6() { return &___U24U2413U2433_6; }
	inline void set_U24U2413U2433_6(float value)
	{
		___U24U2413U2433_6 = value;
	}

	inline static int32_t get_offset_of_U24U2414U2434_7() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2414U2434_7)); }
	inline Color_t4194546905  get_U24U2414U2434_7() const { return ___U24U2414U2434_7; }
	inline Color_t4194546905 * get_address_of_U24U2414U2434_7() { return &___U24U2414U2434_7; }
	inline void set_U24U2414U2434_7(Color_t4194546905  value)
	{
		___U24U2414U2434_7 = value;
	}

	inline static int32_t get_offset_of_U24U2415U2435_8() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2415U2435_8)); }
	inline float get_U24U2415U2435_8() const { return ___U24U2415U2435_8; }
	inline float* get_address_of_U24U2415U2435_8() { return &___U24U2415U2435_8; }
	inline void set_U24U2415U2435_8(float value)
	{
		___U24U2415U2435_8 = value;
	}

	inline static int32_t get_offset_of_U24U2416U2436_9() { return static_cast<int32_t>(offsetof(U24_t1905319356, ___U24U2416U2436_9)); }
	inline Color_t4194546905  get_U24U2416U2436_9() const { return ___U24U2416U2436_9; }
	inline Color_t4194546905 * get_address_of_U24U2416U2436_9() { return &___U24U2416U2436_9; }
	inline void set_U24U2416U2436_9(Color_t4194546905  value)
	{
		___U24U2416U2436_9 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
