﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t1659122786;
// UnityEngine.CharacterController
struct CharacterController_t1618060635;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AdvancedAIV2
struct  AdvancedAIV2_t2935784454  : public MonoBehaviour_t667441552
{
public:
	// System.Object AdvancedAIV2::Distance
	Il2CppObject * ___Distance_2;
	// UnityEngine.Transform AdvancedAIV2::Target
	Transform_t1659122786 * ___Target_3;
	// System.Single AdvancedAIV2::lookAtDistance
	float ___lookAtDistance_4;
	// System.Single AdvancedAIV2::chaseRange
	float ___chaseRange_5;
	// System.Single AdvancedAIV2::attackRange
	float ___attackRange_6;
	// System.Single AdvancedAIV2::moveSpeed
	float ___moveSpeed_7;
	// System.Single AdvancedAIV2::Damping
	float ___Damping_8;
	// System.Int32 AdvancedAIV2::attackRepeatTime
	int32_t ___attackRepeatTime_9;
	// System.Int32 AdvancedAIV2::TheDammage
	int32_t ___TheDammage_10;
	// System.Single AdvancedAIV2::attackTime
	float ___attackTime_11;
	// UnityEngine.CharacterController AdvancedAIV2::controller
	CharacterController_t1618060635 * ___controller_12;
	// System.Single AdvancedAIV2::gravity
	float ___gravity_13;
	// UnityEngine.Vector3 AdvancedAIV2::MoveDirection
	Vector3_t4282066566  ___MoveDirection_14;

public:
	inline static int32_t get_offset_of_Distance_2() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___Distance_2)); }
	inline Il2CppObject * get_Distance_2() const { return ___Distance_2; }
	inline Il2CppObject ** get_address_of_Distance_2() { return &___Distance_2; }
	inline void set_Distance_2(Il2CppObject * value)
	{
		___Distance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Distance_2, value);
	}

	inline static int32_t get_offset_of_Target_3() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___Target_3)); }
	inline Transform_t1659122786 * get_Target_3() const { return ___Target_3; }
	inline Transform_t1659122786 ** get_address_of_Target_3() { return &___Target_3; }
	inline void set_Target_3(Transform_t1659122786 * value)
	{
		___Target_3 = value;
		Il2CppCodeGenWriteBarrier(&___Target_3, value);
	}

	inline static int32_t get_offset_of_lookAtDistance_4() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___lookAtDistance_4)); }
	inline float get_lookAtDistance_4() const { return ___lookAtDistance_4; }
	inline float* get_address_of_lookAtDistance_4() { return &___lookAtDistance_4; }
	inline void set_lookAtDistance_4(float value)
	{
		___lookAtDistance_4 = value;
	}

	inline static int32_t get_offset_of_chaseRange_5() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___chaseRange_5)); }
	inline float get_chaseRange_5() const { return ___chaseRange_5; }
	inline float* get_address_of_chaseRange_5() { return &___chaseRange_5; }
	inline void set_chaseRange_5(float value)
	{
		___chaseRange_5 = value;
	}

	inline static int32_t get_offset_of_attackRange_6() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___attackRange_6)); }
	inline float get_attackRange_6() const { return ___attackRange_6; }
	inline float* get_address_of_attackRange_6() { return &___attackRange_6; }
	inline void set_attackRange_6(float value)
	{
		___attackRange_6 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_7() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___moveSpeed_7)); }
	inline float get_moveSpeed_7() const { return ___moveSpeed_7; }
	inline float* get_address_of_moveSpeed_7() { return &___moveSpeed_7; }
	inline void set_moveSpeed_7(float value)
	{
		___moveSpeed_7 = value;
	}

	inline static int32_t get_offset_of_Damping_8() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___Damping_8)); }
	inline float get_Damping_8() const { return ___Damping_8; }
	inline float* get_address_of_Damping_8() { return &___Damping_8; }
	inline void set_Damping_8(float value)
	{
		___Damping_8 = value;
	}

	inline static int32_t get_offset_of_attackRepeatTime_9() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___attackRepeatTime_9)); }
	inline int32_t get_attackRepeatTime_9() const { return ___attackRepeatTime_9; }
	inline int32_t* get_address_of_attackRepeatTime_9() { return &___attackRepeatTime_9; }
	inline void set_attackRepeatTime_9(int32_t value)
	{
		___attackRepeatTime_9 = value;
	}

	inline static int32_t get_offset_of_TheDammage_10() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___TheDammage_10)); }
	inline int32_t get_TheDammage_10() const { return ___TheDammage_10; }
	inline int32_t* get_address_of_TheDammage_10() { return &___TheDammage_10; }
	inline void set_TheDammage_10(int32_t value)
	{
		___TheDammage_10 = value;
	}

	inline static int32_t get_offset_of_attackTime_11() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___attackTime_11)); }
	inline float get_attackTime_11() const { return ___attackTime_11; }
	inline float* get_address_of_attackTime_11() { return &___attackTime_11; }
	inline void set_attackTime_11(float value)
	{
		___attackTime_11 = value;
	}

	inline static int32_t get_offset_of_controller_12() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___controller_12)); }
	inline CharacterController_t1618060635 * get_controller_12() const { return ___controller_12; }
	inline CharacterController_t1618060635 ** get_address_of_controller_12() { return &___controller_12; }
	inline void set_controller_12(CharacterController_t1618060635 * value)
	{
		___controller_12 = value;
		Il2CppCodeGenWriteBarrier(&___controller_12, value);
	}

	inline static int32_t get_offset_of_gravity_13() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___gravity_13)); }
	inline float get_gravity_13() const { return ___gravity_13; }
	inline float* get_address_of_gravity_13() { return &___gravity_13; }
	inline void set_gravity_13(float value)
	{
		___gravity_13 = value;
	}

	inline static int32_t get_offset_of_MoveDirection_14() { return static_cast<int32_t>(offsetof(AdvancedAIV2_t2935784454, ___MoveDirection_14)); }
	inline Vector3_t4282066566  get_MoveDirection_14() const { return ___MoveDirection_14; }
	inline Vector3_t4282066566 * get_address_of_MoveDirection_14() { return &___MoveDirection_14; }
	inline void set_MoveDirection_14(Vector3_t4282066566  value)
	{
		___MoveDirection_14 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
