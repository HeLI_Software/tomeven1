﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text(1)
struct textU281U29_t2877074419;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void text(1)::.ctor()
extern "C"  void textU281U29__ctor_m2727829443 (textU281U29_t2877074419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator text(1)::Main()
extern "C"  Il2CppObject * textU281U29_Main_m3635832776 (textU281U29_t2877074419 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
