﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// testai
struct testai_t3417797690;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void testai::.ctor()
extern "C"  void testai__ctor_m1582698913 (testai_t3417797690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testai::Awake()
extern "C"  void testai_Awake_m1820304132 (testai_t3417797690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testai::Start()
extern "C"  void testai_Start_m529836705 (testai_t3417797690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testai::Update()
extern "C"  void testai_Update_m3545888140 (testai_t3417797690 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void testai::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void testai_OnTriggerEnter_m318990711 (testai_t3417797690 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
