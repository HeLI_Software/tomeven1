﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// coin taken
struct coinU20taken_t1434922616;

#include "codegen/il2cpp-codegen.h"

// System.Void coin taken::.ctor()
extern "C"  void coinU20taken__ctor_m746433112 (coinU20taken_t1434922616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void coin taken::Start()
extern "C"  void coinU20taken_Start_m3988538200 (coinU20taken_t1434922616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void coin taken::Update()
extern "C"  void coinU20taken_Update_m3391452085 (coinU20taken_t1434922616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void coin taken::Main()
extern "C"  void coinU20taken_Main_m3957445477 (coinU20taken_t1434922616 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
