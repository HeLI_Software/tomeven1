﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Il2CppObject;
// UnityEngine.Transform
struct Transform_t1659122786;

#include "UnityEngine_UnityEngine_MonoBehaviour667441552.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// AISimple
struct  AISimple_t158361594  : public MonoBehaviour_t667441552
{
public:
	// System.Object AISimple::Distance
	Il2CppObject * ___Distance_2;
	// UnityEngine.Transform AISimple::Target
	Transform_t1659122786 * ___Target_3;
	// System.Single AISimple::lookAtDistance
	float ___lookAtDistance_4;
	// System.Single AISimple::attackRange
	float ___attackRange_5;
	// System.Single AISimple::moveSpeed
	float ___moveSpeed_6;
	// System.Single AISimple::Damping
	float ___Damping_7;

public:
	inline static int32_t get_offset_of_Distance_2() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___Distance_2)); }
	inline Il2CppObject * get_Distance_2() const { return ___Distance_2; }
	inline Il2CppObject ** get_address_of_Distance_2() { return &___Distance_2; }
	inline void set_Distance_2(Il2CppObject * value)
	{
		___Distance_2 = value;
		Il2CppCodeGenWriteBarrier(&___Distance_2, value);
	}

	inline static int32_t get_offset_of_Target_3() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___Target_3)); }
	inline Transform_t1659122786 * get_Target_3() const { return ___Target_3; }
	inline Transform_t1659122786 ** get_address_of_Target_3() { return &___Target_3; }
	inline void set_Target_3(Transform_t1659122786 * value)
	{
		___Target_3 = value;
		Il2CppCodeGenWriteBarrier(&___Target_3, value);
	}

	inline static int32_t get_offset_of_lookAtDistance_4() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___lookAtDistance_4)); }
	inline float get_lookAtDistance_4() const { return ___lookAtDistance_4; }
	inline float* get_address_of_lookAtDistance_4() { return &___lookAtDistance_4; }
	inline void set_lookAtDistance_4(float value)
	{
		___lookAtDistance_4 = value;
	}

	inline static int32_t get_offset_of_attackRange_5() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___attackRange_5)); }
	inline float get_attackRange_5() const { return ___attackRange_5; }
	inline float* get_address_of_attackRange_5() { return &___attackRange_5; }
	inline void set_attackRange_5(float value)
	{
		___attackRange_5 = value;
	}

	inline static int32_t get_offset_of_moveSpeed_6() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___moveSpeed_6)); }
	inline float get_moveSpeed_6() const { return ___moveSpeed_6; }
	inline float* get_address_of_moveSpeed_6() { return &___moveSpeed_6; }
	inline void set_moveSpeed_6(float value)
	{
		___moveSpeed_6 = value;
	}

	inline static int32_t get_offset_of_Damping_7() { return static_cast<int32_t>(offsetof(AISimple_t158361594, ___Damping_7)); }
	inline float get_Damping_7() const { return ___Damping_7; }
	inline float* get_address_of_Damping_7() { return &___Damping_7; }
	inline void set_Damping_7(float value)
	{
		___Damping_7 = value;
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
