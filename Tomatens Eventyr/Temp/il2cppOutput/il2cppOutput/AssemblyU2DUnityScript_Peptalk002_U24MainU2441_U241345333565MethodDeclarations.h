﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk002/$Main$41/$
struct U24_t1345333565;
// Peptalk002
struct Peptalk002_t3966958763;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Peptalk0023966958763.h"

// System.Void Peptalk002/$Main$41/$::.ctor(Peptalk002)
extern "C"  void U24__ctor_m3316153646 (U24_t1345333565 * __this, Peptalk002_t3966958763 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Peptalk002/$Main$41/$::MoveNext()
extern "C"  bool U24_MoveNext_m590685393 (U24_t1345333565 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
