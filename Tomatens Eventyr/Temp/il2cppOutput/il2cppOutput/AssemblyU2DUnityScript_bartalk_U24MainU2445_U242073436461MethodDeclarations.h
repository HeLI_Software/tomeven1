﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bartalk/$Main$45/$
struct U24_t2073436461;
// bartalk
struct bartalk_t3961876287;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_bartalk3961876287.h"

// System.Void bartalk/$Main$45/$::.ctor(bartalk)
extern "C"  void U24__ctor_m2809262448 (U24_t2073436461 * __this, bartalk_t3961876287 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean bartalk/$Main$45/$::MoveNext()
extern "C"  bool U24_MoveNext_m2211308675 (U24_t2073436461 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
