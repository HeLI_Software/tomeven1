﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AItest
struct AItest_t1931868346;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void AItest::.ctor()
extern "C"  void AItest__ctor_m901029153 (AItest_t1931868346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AItest::Awake()
extern "C"  void AItest_Awake_m1138634372 (AItest_t1931868346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AItest::Start()
extern "C"  void AItest_Start_m4143134241 (AItest_t1931868346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AItest::Update()
extern "C"  void AItest_Update_m3888962060 (AItest_t1931868346 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AItest::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void AItest_OnTriggerEnter_m3085602295 (AItest_t1931868346 * __this, Collider_t2939674232 * ___other0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
