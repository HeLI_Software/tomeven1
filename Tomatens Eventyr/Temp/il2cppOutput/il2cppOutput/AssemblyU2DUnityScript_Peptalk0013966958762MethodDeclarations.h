﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk001
struct Peptalk001_t3966958762;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void Peptalk001::.ctor()
extern "C"  void Peptalk001__ctor_m4249990246 (Peptalk001_t3966958762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Peptalk001::Main()
extern "C"  Il2CppObject * Peptalk001_Main_m1310406057 (Peptalk001_t3966958762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
