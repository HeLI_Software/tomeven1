﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Water.MeshContainer
struct MeshContainer_t1735241934;
// UnityEngine.Mesh
struct Mesh_t4241756145;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Mesh4241756145.h"

// System.Void UnityStandardAssets.Water.MeshContainer::.ctor(UnityEngine.Mesh)
extern "C"  void MeshContainer__ctor_m4091217775 (MeshContainer_t1735241934 * __this, Mesh_t4241756145 * ___m0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Water.MeshContainer::Update()
extern "C"  void MeshContainer_Update_m1922421466 (MeshContainer_t1735241934 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
