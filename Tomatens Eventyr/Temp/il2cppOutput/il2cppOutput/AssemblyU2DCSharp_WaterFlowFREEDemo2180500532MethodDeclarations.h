﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// WaterFlowFREEDemo
struct WaterFlowFREEDemo_t2180500532;

#include "codegen/il2cpp-codegen.h"

// System.Void WaterFlowFREEDemo::.ctor()
extern "C"  void WaterFlowFREEDemo__ctor_m2160876471 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::Start()
extern "C"  void WaterFlowFREEDemo_Start_m1108014263 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::Update()
extern "C"  void WaterFlowFREEDemo_Update_m4289523254 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnSlider_SimpleR(System.Single)
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleR_m2682694877 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnSlider_SimpleG(System.Single)
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleG_m4003603528 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnSlider_SimpleB(System.Single)
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleB_m2261307117 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnSlider_SimpleUSpeed(System.Single)
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleUSpeed_m794485011 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnSlider_SimpleVSpeed(System.Single)
extern "C"  void WaterFlowFREEDemo_OnSlider_SimpleVSpeed_m1005835924 (WaterFlowFREEDemo_t2180500532 * __this, float ___value0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::OnFindMoreInFullVersion()
extern "C"  void WaterFlowFREEDemo_OnFindMoreInFullVersion_m4067824716 (WaterFlowFREEDemo_t2180500532 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::<Start>m__0(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__0_m2914074683 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::<Start>m__1(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__1_m2403540506 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::<Start>m__2(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__2_m1893006329 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::<Start>m__3(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__3_m1382472152 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void WaterFlowFREEDemo::<Start>m__4(System.Single)
extern "C"  void WaterFlowFREEDemo_U3CStartU3Em__4_m871937975 (WaterFlowFREEDemo_t2180500532 * __this, float p0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
