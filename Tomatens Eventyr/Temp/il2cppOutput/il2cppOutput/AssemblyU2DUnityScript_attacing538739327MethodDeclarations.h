﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// attacing
struct attacing_t538739327;

#include "codegen/il2cpp-codegen.h"

// System.Void attacing::.ctor()
extern "C"  void attacing__ctor_m1210375537 (attacing_t538739327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void attacing::Update()
extern "C"  void attacing_Update_m593798076 (attacing_t538739327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void attacing::Main()
extern "C"  void attacing_Main_m2171296044 (attacing_t538739327 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
