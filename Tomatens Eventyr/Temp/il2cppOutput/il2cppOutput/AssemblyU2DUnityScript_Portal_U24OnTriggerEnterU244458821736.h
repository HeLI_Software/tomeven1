﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Collider
struct Collider_t2939674232;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Portal/$OnTriggerEnter$48
struct  U24OnTriggerEnterU2448_t458821736  : public GenericGenerator_1_t207610107
{
public:
	// UnityEngine.Collider Portal/$OnTriggerEnter$48::$hit$51
	Collider_t2939674232 * ___U24hitU2451_0;

public:
	inline static int32_t get_offset_of_U24hitU2451_0() { return static_cast<int32_t>(offsetof(U24OnTriggerEnterU2448_t458821736, ___U24hitU2451_0)); }
	inline Collider_t2939674232 * get_U24hitU2451_0() const { return ___U24hitU2451_0; }
	inline Collider_t2939674232 ** get_address_of_U24hitU2451_0() { return &___U24hitU2451_0; }
	inline void set_U24hitU2451_0(Collider_t2939674232 * value)
	{
		___U24hitU2451_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24hitU2451_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
