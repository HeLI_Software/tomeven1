﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// gobacktofirstbar
struct gobacktofirstbar_t2648077805;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void gobacktofirstbar::.ctor()
extern "C"  void gobacktofirstbar__ctor_m1362364611 (gobacktofirstbar_t2648077805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gobacktofirstbar::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void gobacktofirstbar_OnTriggerEnter_m969645461 (gobacktofirstbar_t2648077805 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void gobacktofirstbar::Main()
extern "C"  void gobacktofirstbar_Main_m2868935578 (gobacktofirstbar_t2648077805 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
