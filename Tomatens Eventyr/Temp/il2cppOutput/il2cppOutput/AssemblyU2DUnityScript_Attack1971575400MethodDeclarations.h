﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Attack
struct Attack_t1971575400;

#include "codegen/il2cpp-codegen.h"

// System.Void Attack::.ctor()
extern "C"  void Attack__ctor_m3138788072 (Attack_t1971575400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Attack::Update()
extern "C"  void Attack_Update_m245044517 (Attack_t1971575400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Attack::Main()
extern "C"  void Attack_Main_m3341881557 (Attack_t1971575400 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
