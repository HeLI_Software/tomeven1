﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// FPSInputController
struct FPSInputController_t3054943997;

#include "codegen/il2cpp-codegen.h"

// System.Void FPSInputController::.ctor()
extern "C"  void FPSInputController__ctor_m3174141031 (FPSInputController_t3054943997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSInputController::Awake()
extern "C"  void FPSInputController_Awake_m3411746250 (FPSInputController_t3054943997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSInputController::Update()
extern "C"  void FPSInputController_Update_m1340986246 (FPSInputController_t3054943997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void FPSInputController::Main()
extern "C"  void FPSInputController_Main_m2234643318 (FPSInputController_t3054943997 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
