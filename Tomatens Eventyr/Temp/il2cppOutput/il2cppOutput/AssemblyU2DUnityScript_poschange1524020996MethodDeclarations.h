﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// poschange
struct poschange_t1524020996;

#include "codegen/il2cpp-codegen.h"

// System.Void poschange::.ctor()
extern "C"  void poschange__ctor_m1267797778 (poschange_t1524020996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void poschange::Main()
extern "C"  void poschange_Main_m2865885035 (poschange_t1524020996 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
