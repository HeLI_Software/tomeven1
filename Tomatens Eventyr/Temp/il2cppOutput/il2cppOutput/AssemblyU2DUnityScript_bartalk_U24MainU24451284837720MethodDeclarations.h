﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// bartalk/$Main$45
struct U24MainU2445_t1284837720;
// bartalk
struct bartalk_t3961876287;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_bartalk3961876287.h"

// System.Void bartalk/$Main$45::.ctor(bartalk)
extern "C"  void U24MainU2445__ctor_m3470316635 (U24MainU2445_t1284837720 * __this, bartalk_t3961876287 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> bartalk/$Main$45::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2445_GetEnumerator_m2712048344 (U24MainU2445_t1284837720 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
