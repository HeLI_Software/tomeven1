﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// health
struct health_t3073704540;

#include "codegen/il2cpp-codegen.h"

// System.Void health::.ctor()
extern "C"  void health__ctor_m3602949492 (health_t3073704540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void health::Main()
extern "C"  void health_Main_m2248475849 (health_t3073704540 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
