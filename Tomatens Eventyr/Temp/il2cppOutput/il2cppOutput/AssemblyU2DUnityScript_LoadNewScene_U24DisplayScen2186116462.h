﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// LoadNewScene
struct LoadNewScene_t2813835762;

#include "Boo_Lang_Boo_Lang_GenericGeneratorEnumerator_1_gen67576739.h"
#include "UnityEngine_UnityEngine_Color4194546905.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// LoadNewScene/$DisplayScene$17/$
struct  U24_t2186116462  : public GenericGeneratorEnumerator_1_t67576739
{
public:
	// System.Single LoadNewScene/$DisplayScene$17/$::$$1$18
	float ___U24U241U2418_2;
	// UnityEngine.Color LoadNewScene/$DisplayScene$17/$::$$2$19
	Color_t4194546905  ___U24U242U2419_3;
	// System.Single LoadNewScene/$DisplayScene$17/$::$$3$20
	float ___U24U243U2420_4;
	// UnityEngine.Color LoadNewScene/$DisplayScene$17/$::$$4$21
	Color_t4194546905  ___U24U244U2421_5;
	// System.Single LoadNewScene/$DisplayScene$17/$::$$5$22
	float ___U24U245U2422_6;
	// UnityEngine.Color LoadNewScene/$DisplayScene$17/$::$$6$23
	Color_t4194546905  ___U24U246U2423_7;
	// System.Single LoadNewScene/$DisplayScene$17/$::$$7$24
	float ___U24U247U2424_8;
	// UnityEngine.Color LoadNewScene/$DisplayScene$17/$::$$8$25
	Color_t4194546905  ___U24U248U2425_9;
	// LoadNewScene LoadNewScene/$DisplayScene$17/$::$self_$26
	LoadNewScene_t2813835762 * ___U24self_U2426_10;

public:
	inline static int32_t get_offset_of_U24U241U2418_2() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U241U2418_2)); }
	inline float get_U24U241U2418_2() const { return ___U24U241U2418_2; }
	inline float* get_address_of_U24U241U2418_2() { return &___U24U241U2418_2; }
	inline void set_U24U241U2418_2(float value)
	{
		___U24U241U2418_2 = value;
	}

	inline static int32_t get_offset_of_U24U242U2419_3() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U242U2419_3)); }
	inline Color_t4194546905  get_U24U242U2419_3() const { return ___U24U242U2419_3; }
	inline Color_t4194546905 * get_address_of_U24U242U2419_3() { return &___U24U242U2419_3; }
	inline void set_U24U242U2419_3(Color_t4194546905  value)
	{
		___U24U242U2419_3 = value;
	}

	inline static int32_t get_offset_of_U24U243U2420_4() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U243U2420_4)); }
	inline float get_U24U243U2420_4() const { return ___U24U243U2420_4; }
	inline float* get_address_of_U24U243U2420_4() { return &___U24U243U2420_4; }
	inline void set_U24U243U2420_4(float value)
	{
		___U24U243U2420_4 = value;
	}

	inline static int32_t get_offset_of_U24U244U2421_5() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U244U2421_5)); }
	inline Color_t4194546905  get_U24U244U2421_5() const { return ___U24U244U2421_5; }
	inline Color_t4194546905 * get_address_of_U24U244U2421_5() { return &___U24U244U2421_5; }
	inline void set_U24U244U2421_5(Color_t4194546905  value)
	{
		___U24U244U2421_5 = value;
	}

	inline static int32_t get_offset_of_U24U245U2422_6() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U245U2422_6)); }
	inline float get_U24U245U2422_6() const { return ___U24U245U2422_6; }
	inline float* get_address_of_U24U245U2422_6() { return &___U24U245U2422_6; }
	inline void set_U24U245U2422_6(float value)
	{
		___U24U245U2422_6 = value;
	}

	inline static int32_t get_offset_of_U24U246U2423_7() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U246U2423_7)); }
	inline Color_t4194546905  get_U24U246U2423_7() const { return ___U24U246U2423_7; }
	inline Color_t4194546905 * get_address_of_U24U246U2423_7() { return &___U24U246U2423_7; }
	inline void set_U24U246U2423_7(Color_t4194546905  value)
	{
		___U24U246U2423_7 = value;
	}

	inline static int32_t get_offset_of_U24U247U2424_8() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U247U2424_8)); }
	inline float get_U24U247U2424_8() const { return ___U24U247U2424_8; }
	inline float* get_address_of_U24U247U2424_8() { return &___U24U247U2424_8; }
	inline void set_U24U247U2424_8(float value)
	{
		___U24U247U2424_8 = value;
	}

	inline static int32_t get_offset_of_U24U248U2425_9() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24U248U2425_9)); }
	inline Color_t4194546905  get_U24U248U2425_9() const { return ___U24U248U2425_9; }
	inline Color_t4194546905 * get_address_of_U24U248U2425_9() { return &___U24U248U2425_9; }
	inline void set_U24U248U2425_9(Color_t4194546905  value)
	{
		___U24U248U2425_9 = value;
	}

	inline static int32_t get_offset_of_U24self_U2426_10() { return static_cast<int32_t>(offsetof(U24_t2186116462, ___U24self_U2426_10)); }
	inline LoadNewScene_t2813835762 * get_U24self_U2426_10() const { return ___U24self_U2426_10; }
	inline LoadNewScene_t2813835762 ** get_address_of_U24self_U2426_10() { return &___U24self_U2426_10; }
	inline void set_U24self_U2426_10(LoadNewScene_t2813835762 * value)
	{
		___U24self_U2426_10 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2426_10, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
