﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// enemy
struct enemy_t96653192;

#include "codegen/il2cpp-codegen.h"

// System.Void enemy::.ctor()
extern "C"  void enemy__ctor_m3282291854 (enemy_t96653192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemy::DeductPoints(System.Int32)
extern "C"  void enemy_DeductPoints_m1056746317 (enemy_t96653192 * __this, int32_t ___hitpoints0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemy::Update()
extern "C"  void enemy_Update_m398694463 (enemy_t96653192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void enemy::Main()
extern "C"  void enemy_Main_m3069416047 (enemy_t96653192 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
