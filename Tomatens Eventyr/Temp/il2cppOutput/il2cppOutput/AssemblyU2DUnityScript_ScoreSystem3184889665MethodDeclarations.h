﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScoreSystem
struct ScoreSystem_t3184889665;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void ScoreSystem::.ctor()
extern "C"  void ScoreSystem__ctor_m2873307829 (ScoreSystem_t3184889665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreSystem::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void ScoreSystem_OnTriggerEnter_m2027114723 (ScoreSystem_t3184889665 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreSystem::OnGUI()
extern "C"  void ScoreSystem_OnGUI_m2368706479 (ScoreSystem_t3184889665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScoreSystem::Main()
extern "C"  void ScoreSystem_Main_m978013032 (ScoreSystem_t3184889665 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
