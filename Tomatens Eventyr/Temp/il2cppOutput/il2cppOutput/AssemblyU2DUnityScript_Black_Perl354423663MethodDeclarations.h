﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Black_Perl
struct Black_Perl_t354423663;
// UnityEngine.Collider
struct Collider_t2939674232;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Collider2939674232.h"

// System.Void Black_Perl::.ctor()
extern "C"  void Black_Perl__ctor_m2474732353 (Black_Perl_t354423663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Black_Perl::OnTriggerEnter(UnityEngine.Collider)
extern "C"  void Black_Perl_OnTriggerEnter_m1162736599 (Black_Perl_t354423663 * __this, Collider_t2939674232 * ___hit0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Black_Perl::Main()
extern "C"  void Black_Perl_Main_m2489176412 (Black_Perl_t354423663 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
