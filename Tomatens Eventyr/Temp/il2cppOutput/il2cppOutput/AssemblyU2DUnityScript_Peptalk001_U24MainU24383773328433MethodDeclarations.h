﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Peptalk001/$Main$38
struct U24MainU2438_t3773328433;
// Peptalk001
struct Peptalk001_t3966958762;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds>
struct IEnumerator_1_t3527684328;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_Peptalk0013966958762.h"

// System.Void Peptalk001/$Main$38::.ctor(Peptalk001)
extern "C"  void U24MainU2438__ctor_m1199793755 (U24MainU2438_t3773328433 * __this, Peptalk001_t3966958762 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<UnityEngine.WaitForSeconds> Peptalk001/$Main$38::GetEnumerator()
extern "C"  Il2CppObject* U24MainU2438_GetEnumerator_m3742278741 (U24MainU2438_t3773328433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
