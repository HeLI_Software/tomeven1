﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// cameraSpin
struct cameraSpin_t2282820871;

#include "codegen/il2cpp-codegen.h"

// System.Void cameraSpin::.ctor()
extern "C"  void cameraSpin__ctor_m884782249 (cameraSpin_t2282820871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraSpin::Update()
extern "C"  void cameraSpin_Update_m3385308036 (cameraSpin_t2282820871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void cameraSpin::Main()
extern "C"  void cameraSpin_Main_m3546266356 (cameraSpin_t2282820871 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
