﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadNewScene
struct LoadNewScene_t2813835762;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadNewScene::.ctor()
extern "C"  void LoadNewScene__ctor_m2205960414 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadNewScene::Start()
extern "C"  void LoadNewScene_Start_m1153098206 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadNewScene::DisplayScene()
extern "C"  Il2CppObject * LoadNewScene_DisplayScene_m360774050 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator LoadNewScene::HideScene()
extern "C"  Il2CppObject * LoadNewScene_HideScene_m1348930580 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadNewScene::LoadScene()
extern "C"  void LoadNewScene_LoadScene_m3608904258 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void LoadNewScene::Main()
extern "C"  void LoadNewScene_Main_m4143074335 (LoadNewScene_t2813835762 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
