﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadNewScene/$HideScene$28
struct U24HideSceneU2428_t3689129383;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"

// System.Void LoadNewScene/$HideScene$28::.ctor()
extern "C"  void U24HideSceneU2428__ctor_m597204489 (U24HideSceneU2428_t3689129383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> LoadNewScene/$HideScene$28::GetEnumerator()
extern "C"  Il2CppObject* U24HideSceneU2428_GetEnumerator_m3464629243 (U24HideSceneU2428_t3689129383 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
