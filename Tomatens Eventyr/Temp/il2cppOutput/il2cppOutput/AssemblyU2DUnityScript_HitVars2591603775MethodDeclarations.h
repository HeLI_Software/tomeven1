﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// HitVars
struct HitVars_t2591603775;

#include "codegen/il2cpp-codegen.h"

// System.Void HitVars::.ctor()
extern "C"  void HitVars__ctor_m3959088375 (HitVars_t2591603775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HitVars::.cctor()
extern "C"  void HitVars__cctor_m1990559126 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void HitVars::Main()
extern "C"  void HitVars_Main_m43206886 (HitVars_t2591603775 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
