﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SkyboxChanger
struct SkyboxChanger_t4201523768;

#include "codegen/il2cpp-codegen.h"

// System.Void SkyboxChanger::.ctor()
extern "C"  void SkyboxChanger__ctor_m1152136755 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxChanger::Awake()
extern "C"  void SkyboxChanger_Awake_m1389741974 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void SkyboxChanger::ChangeSkybox()
extern "C"  void SkyboxChanger_ChangeSkybox_m1815741195 (SkyboxChanger_t4201523768 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
