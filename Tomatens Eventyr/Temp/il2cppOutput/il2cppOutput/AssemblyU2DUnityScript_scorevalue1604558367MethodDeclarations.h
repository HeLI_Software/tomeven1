﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// scorevalue
struct scorevalue_t1604558367;

#include "codegen/il2cpp-codegen.h"

// System.Void scorevalue::.ctor()
extern "C"  void scorevalue__ctor_m4124501649 (scorevalue_t1604558367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void scorevalue::.cctor()
extern "C"  void scorevalue__cctor_m2823403324 (Il2CppObject * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void scorevalue::Start()
extern "C"  void scorevalue_Start_m3071639441 (scorevalue_t1604558367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void scorevalue::Main()
extern "C"  void scorevalue_Main_m2126752780 (scorevalue_t1604558367 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
