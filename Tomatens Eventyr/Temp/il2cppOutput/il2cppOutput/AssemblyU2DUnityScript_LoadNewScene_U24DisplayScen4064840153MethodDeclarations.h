﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// LoadNewScene/$DisplayScene$17
struct U24DisplaySceneU2417_t4064840153;
// LoadNewScene
struct LoadNewScene_t2813835762;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t1787714124;

#include "codegen/il2cpp-codegen.h"
#include "AssemblyU2DUnityScript_LoadNewScene2813835762.h"

// System.Void LoadNewScene/$DisplayScene$17::.ctor(LoadNewScene)
extern "C"  void U24DisplaySceneU2417__ctor_m4290325163 (U24DisplaySceneU2417_t4064840153 * __this, LoadNewScene_t2813835762 * ___self_0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.Generic.IEnumerator`1<System.Object> LoadNewScene/$DisplayScene$17::GetEnumerator()
extern "C"  Il2CppObject* U24DisplaySceneU2417_GetEnumerator_m720241883 (U24DisplaySceneU2417_t4064840153 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
