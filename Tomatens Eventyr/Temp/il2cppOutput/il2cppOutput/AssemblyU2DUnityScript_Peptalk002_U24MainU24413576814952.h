﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Peptalk002
struct Peptalk002_t3966958763;

#include "Boo_Lang_Boo_Lang_GenericGenerator_1_gen207610107.h"

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// Peptalk002/$Main$41
struct  U24MainU2441_t3576814952  : public GenericGenerator_1_t207610107
{
public:
	// Peptalk002 Peptalk002/$Main$41::$self_$43
	Peptalk002_t3966958763 * ___U24self_U2443_0;

public:
	inline static int32_t get_offset_of_U24self_U2443_0() { return static_cast<int32_t>(offsetof(U24MainU2441_t3576814952, ___U24self_U2443_0)); }
	inline Peptalk002_t3966958763 * get_U24self_U2443_0() const { return ___U24self_U2443_0; }
	inline Peptalk002_t3966958763 ** get_address_of_U24self_U2443_0() { return &___U24self_U2443_0; }
	inline void set_U24self_U2443_0(Peptalk002_t3966958763 * value)
	{
		___U24self_U2443_0 = value;
		Il2CppCodeGenWriteBarrier(&___U24self_U2443_0, value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif
