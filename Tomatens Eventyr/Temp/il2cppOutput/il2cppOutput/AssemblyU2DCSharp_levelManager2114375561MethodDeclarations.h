﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// levelManager
struct levelManager_t2114375561;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_String7231557.h"

// System.Void levelManager::.ctor()
extern "C"  void levelManager__ctor_m2387053874 (levelManager_t2114375561 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void levelManager::LoadLevel(System.String)
extern "C"  void levelManager_LoadLevel_m3956732980 (Il2CppObject * __this /* static, unused */, String_t* ___name0, const MethodInfo* method) IL2CPP_METHOD_ATTR;
