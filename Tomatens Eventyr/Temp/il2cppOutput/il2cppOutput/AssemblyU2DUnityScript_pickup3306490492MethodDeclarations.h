﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// pickup
struct pickup_t3306490492;

#include "codegen/il2cpp-codegen.h"

// System.Void pickup::.ctor()
extern "C"  void pickup__ctor_m966246740 (pickup_t3306490492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pickup::Update()
extern "C"  void pickup_Update_m1615739961 (pickup_t3306490492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pickup::OnMouseDown()
extern "C"  void pickup_OnMouseDown_m3411407738 (pickup_t3306490492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pickup::OnMouseUp()
extern "C"  void pickup_OnMouseUp_m1992879347 (pickup_t3306490492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void pickup::Main()
extern "C"  void pickup_Main_m1609231593 (pickup_t3306490492 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
