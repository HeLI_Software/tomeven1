﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// text
struct text_t3556653;
// System.Collections.IEnumerator
struct IEnumerator_t3464575207;

#include "codegen/il2cpp-codegen.h"

// System.Void text::.ctor()
extern "C"  void text__ctor_m1431604483 (text_t3556653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator text::Main()
extern "C"  Il2CppObject * text_Main_m1742654188 (text_t3556653 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
