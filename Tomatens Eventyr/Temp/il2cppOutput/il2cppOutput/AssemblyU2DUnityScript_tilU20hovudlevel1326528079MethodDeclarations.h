﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// til hovudlevel
struct tilU20hovudlevel_t1326528079;

#include "codegen/il2cpp-codegen.h"

// System.Void til hovudlevel::.ctor()
extern "C"  void tilU20hovudlevel__ctor_m1682576865 (tilU20hovudlevel_t1326528079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void til hovudlevel::Main()
extern "C"  void tilU20hovudlevel_Main_m1216697020 (tilU20hovudlevel_t1326528079 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
