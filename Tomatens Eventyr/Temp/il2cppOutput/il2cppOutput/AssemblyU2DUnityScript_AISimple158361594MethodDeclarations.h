﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AISimple
struct AISimple_t158361594;

#include "codegen/il2cpp-codegen.h"

// System.Void AISimple::.ctor()
extern "C"  void AISimple__ctor_m1034678870 (AISimple_t158361594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AISimple::Update()
extern "C"  void AISimple_Update_m3737135991 (AISimple_t158361594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AISimple::lookAt()
extern "C"  void AISimple_lookAt_m307248000 (AISimple_t158361594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AISimple::attack()
extern "C"  void AISimple_attack_m2888636694 (AISimple_t158361594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AISimple::Main()
extern "C"  void AISimple_Main_m3689649063 (AISimple_t158361594 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
