﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlatformInputController
struct PlatformInputController_t1844164083;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector34282066566.h"

// System.Void PlatformInputController::.ctor()
extern "C"  void PlatformInputController__ctor_m2826591823 (PlatformInputController_t1844164083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformInputController::Awake()
extern "C"  void PlatformInputController_Awake_m3064197042 (PlatformInputController_t1844164083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformInputController::Update()
extern "C"  void PlatformInputController_Update_m3451862686 (PlatformInputController_t1844164083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PlatformInputController::ProjectOntoPlane(UnityEngine.Vector3,UnityEngine.Vector3)
extern "C"  Vector3_t4282066566  PlatformInputController_ProjectOntoPlane_m2987832712 (PlatformInputController_t1844164083 * __this, Vector3_t4282066566  ___v0, Vector3_t4282066566  ___normal1, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector3 PlatformInputController::ConstantSlerp(UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern "C"  Vector3_t4282066566  PlatformInputController_ConstantSlerp_m2377028964 (PlatformInputController_t1844164083 * __this, Vector3_t4282066566  ___from0, Vector3_t4282066566  ___to1, float ___angle2, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlatformInputController::Main()
extern "C"  void PlatformInputController_Main_m3054716046 (PlatformInputController_t1844164083 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
