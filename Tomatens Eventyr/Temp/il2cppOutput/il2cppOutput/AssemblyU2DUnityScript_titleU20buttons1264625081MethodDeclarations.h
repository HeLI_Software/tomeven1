﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// title buttons
struct titleU20buttons_t1264625081;

#include "codegen/il2cpp-codegen.h"

// System.Void title buttons::.ctor()
extern "C"  void titleU20buttons__ctor_m215423613 (titleU20buttons_t1264625081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void title buttons::OnGUI()
extern "C"  void titleU20buttons_OnGUI_m4005789559 (titleU20buttons_t1264625081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void title buttons::Main()
extern "C"  void titleU20buttons_Main_m1446464160 (titleU20buttons_t1264625081 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
